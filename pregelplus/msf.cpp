#include "basic/pregel-dev.h"
#include "auxiliary.hpp"

using namespace std;

// Edge is also the message type!!!
struct Edge {
    int id, w, u, v;
    Edge() : id(0), w(0), u(0), v(0) {}
    Edge(int id, int w, int u, int v) :
            id(id), w(w), u(u), v(v) {} 
};

/* the comparison is besed on weight by default */
bool operator<(const Edge &a, const Edge &b) {
    return a.w != b.w ? a.w < b.w : a.id < b.id;
}

/* based on the target id */
bool cmp_id(const Edge &a, const Edge &b) {
    return a.id != b.id ? a.id < b.id : a.w < b.w;
}

ibinstream &operator<<(ibinstream &is, const Edge &a) {
    return (is << a.id << a.w << a.u << a.v);
}

obinstream &operator>>(obinstream &os, Edge &a) {
    return (os >> a.id >> a.w >> a.u >> a.v);
}

// merge the parallel edges
vector<Edge> merge_edges(vector<Edge> &vec) {
    vector<Edge> ret;
    if (vec.empty()) return ret;
    sort(vec.begin(), vec.end(), cmp_id);
    ret.push_back(vec.front());
    for (int i = 1; i < vec.size(); i++)
        if (vec[i].id != ret.back().id)
            ret.push_back(vec[i]);
    return ret;
}

struct XValue {
    int phase, P;
    bool finish, active;
    Edge store;
    vector<pair<int, int> > edges, msf;
    vector<Edge> nbr;
};

ibinstream &operator<<(ibinstream &m, const XValue &v) {
    m << v.phase << v.P << v.finish << v.active << v.store;
    m << v.edges << v.msf << v.nbr;
    return m;
}

obinstream &operator>>(obinstream &m, XValue &v) {
    m >> v.phase >> v.P >> v.finish >> v.active >> v.store;
    m >> v.edges >> v.msf >> v.nbr;
    return m;
}

class XVertex : public Vertex<int, XValue, Edge> {
public:
    void compute(MessageContainer &msgs) override {
        int phase = value().phase;
        if (phase == 1) {
            for (const auto &e : value().edges)
                value().nbr.push_back(Edge(e.first, e.second, id, e.first));
            value().edges.clear();
            value().active = true;
            if (value().nbr.empty()) {
                value().finish = true;
                value().active = false;
                value().phase = 7;
                vote_to_halt();
            } else {
                value().store = *min_element(
                    value().nbr.begin(), value().nbr.end());
                value().P = value().store.id;
                if (id > value().P)
                    send_message(value().P, Edge(id, 0, 0, 0));
                value().finish = false;
            }
            value().phase = 2;
            return;
        }
        if (phase == 2) {
            if ((*(bool*) getAgg())) {
                value().phase = 7;
                vote_to_halt();
            } else {
                bool sv = (id == value().P);
                for (const Edge &e : msgs)
                    sv = (sv || (e.id == value().P));
                if (sv) {
                    value().P = id;
                } else {
                    Edge &e = value().store;
                    send_message(e.u, Edge(e.v, e.w, 0, 0));
                    send_message(e.v, Edge(e.u, e.w, 0, 0));
                }
                value().finish = sv;
                value().phase = 7;
            }
            return;
        }
        if (phase == 7) {
            for (const auto &msg : msgs)
                value().msf.push_back(make_pair(msg.id, msg.w));
            if (!value().active) {
                vote_to_halt();
                return;
            }
            send_message(value().P, Edge(id, 0, 0, 0));
            value().phase = 3;
            return;
        }
        if (phase == 3) {
            if (!*(bool*) getAgg()) {
                for (const auto &msg : msgs)
                    send_message(msg.id, Edge(value().P, 0, 0, 0));
                value().phase = 4;
            } else {
                for (const auto &e : value().nbr)
                    send_message(e.id, Edge(value().P, e.w, e.u, e.v));
                value().nbr.clear();
                value().phase = 5;
            }
            return;
        }
        if (phase == 4) {
            value().finish = (msgs[0].id == value().P);
            value().P = msgs[0].id;
            send_message(value().P, Edge(id, 0, 0, 0));
            value().phase = 3;
            return;
        }
        if (phase == 5) {
            if (!msgs.empty()) {
                auto l = merge_edges(msgs);
                for (const auto &e : l)
                    if (e.id != value().P)
                        send_message(value().P, e);
                value().phase = 6;
            }
            return;
        }
        if (phase == 6) {
            if (msgs.empty()) {
                value().active = false;
                value().phase = 7;
                vote_to_halt();
            } else {
                value().finish = false;
                value().nbr = merge_edges(msgs);
                value().store = *min_element(
                    msgs.begin(), msgs.end());
                value().P = value().store.id;
                if (id > value().P)
                    send_message(value().P, Edge(id, 0, 0, 0));
                value().phase = 2;
            }
            return;
        }
    }
};

class XAgg : public Aggregator<XVertex, bool, bool> {
private:
    bool M;
public:
    void init() { M = true; }
    void stepPartial(XVertex *v) { M = M && v->value().finish; }
    void stepFinal(bool *part) { M = M && *part; }
    bool *finishPartial() { return &M; }
    bool *finishFinal() { return &M; }
};

class XWorker : public Worker<XVertex, XAgg> {
private:
    char buf[120];
public:
    XVertex *toVertex(char *line) override {
        XVertex *v = new XVertex();
        Tokenizer tok(line);
        parse(&tok, v->id);
        parse(&tok, v->value().edges);
        v->value().phase = 1;
        return v;
    }
    void toline(XVertex *v, BufferedWriter &writer) override {
        OutputBuffer ob(&writer, buf);
        print(&ob, v->id);
        print(&ob, v->value().msf);
        ob.flush();
    }
};

void pregel_msf(string in, string out) {
    WorkerParams param;
    param.input_path = in;
    param.output_path = out;
    param.force_write = true;
    param.native_dispatcher = false;
    XWorker worker;
    XAgg agg;
    worker.setAggregator(&agg);
    worker.run(param);
}

int main(int argc, char* argv[]) {
    if (argc < 3) {
        fprintf(stderr, "%s <input_folder> <output_folder>\n", argv[0]);
        return 0;
    }
    init_workers();
    pregel_msf(argv[1], argv[2]);
    worker_finalize();
    return 0;
}
