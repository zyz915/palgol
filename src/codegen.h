#ifndef CODEGEN_H
#define CODEGEN_H

#include <vector>
#include <map>
#include <set>

#include "intermediate.h"
#include "types.h"

struct MsgEnc {
	int index;
	MsgEnc() {}

	virtual void dump(FILE *f) const = 0;
};

struct IntValue : MsgEnc {
	int start, width;

	IntValue() : start(0), width(32) {}
	virtual void dump(FILE *f) const;
};

struct FloatValue : MsgEnc {
	FloatValue() {}
	virtual void dump(FILE *f) const;
};

struct MsgInfo {
	SendMsgIR *sm;
	int numInt, numFloat; // number of int/float variables

	MsgInfo() : sm(nullptr), numInt(0), numFloat(0) {}
};

class Doc {
public:
	virtual int print(int lvl, FILE *f) const { return lvl; }
};

class Text : public Doc {
protected:
	std::string cont_;
public:
	Text() {}
	Text(std::string s) : cont_(s) {}

	char back() { return cont_.back(); }
	void append(std::string s) { cont_ += s; }
	std::string cont() { return cont_; }
	int print(int lvl, FILE *f) const override;
};

class Increase : public Doc {
public:
	Increase() {}

	int print(int lvl, FILE *f) const override;
};

class Decrease : public Doc {
public:
	Decrease() {}

	int print(int lvl, FILE *f) const override;
};

struct AggInfo {
	Type *type;
	Operator *op;
	Conjunction *conj;
	ExprIR *expr;

	AggInfo(Type *type, Operator *op, Conjunction *conj, ExprIR *expr) :
			type(type), op(op), conj(conj), expr(expr) {}
};

class AggregatorManager {
private:
	int cnt_; // for generating names
	std::map<std::string, AggInfo*> aggs_;	

	const std::string default_ = std::string("iter");

public:
	AggregatorManager() : cnt_(0) {} 

	FieldName *getAggFieldName();
	FieldIR *getAggFieldIR();

	std::string add(AggInfo *agg);
	std::string getDefault(); // default aggregator
	AggInfo *get(std::string name);
	bool hasAgg() { return !aggs_.empty(); }
	bool isSimpleAgg();
	ExprIR *getAggIdentity(Type *type, Operator *op);

	void pregel_struct(CodeGen *cg);
	void pregel_serial(CodeGen *cg);
	void pregel_body(CodeGen *cg);
};

class MessageManager {
private:
	std::map<int, std::vector<SendMsgIR*> > ch2msg_;
	std::map<int, std::vector<int> > step2chs_;
	std::map<int, int> ch2step_;
	std::map<int, MsgInfo*> ch2info_;

	int maxInt, maxFloat;
	CombinerT combiner_;

	MsgInfo *compute(int flag_s, SendMsgIR *s);

public:
	MessageManager() : maxInt(0), maxFloat(0),
			combiner_(CombinerT::c_null) {}

	void addSM(SendMsgIR *sm);
	SendMsgIR *getFstSM(int ch);
	void addChannelToStep(int step, int ch);
	void analysize();
	bool useCombiner();
	// channel check
	bool isUnique(int ch);
	ExprIR *checkFlag(std::string t, int ch_);
	// type of message
	bool isSimpleType(); // for message type
	bool isSimpleType(int ch); // for each channel
	std::string getTypeStr();
	// for translating ReadMsgIR()
	void setCurVariable(const char *s);
	ExprIR *readMessage(int ch, int index, const std::string &name);
	ExprIR *encMessage(const SendMsgIR *sm);
	// generate data structure
	void pregel_struct(CodeGen *cg);
	void pregel_serial(CodeGen *cg);
	void pregel_combiner(CodeGen *cg);
};

class VertexManager {
private:
	std::set<std::string> used_;
	std::vector<FieldIR*> fields_;
public:
	VertexManager() {}

	void add(FieldIR *fn);
	bool uses(std::string fn);
	void pregel_struct(CodeGen *cg);
	void pregel_serial(CodeGen *cg);
};

class CodeGen {
private:
	bool ext_; // whether external function is used
	std::vector<Doc*> seq_; // the whole code
	AggregatorManager *am_;
	MessageManager *mm_;
	VertexManager *vm_;

	std::map<STM*, int> stm2step_;
	std::set<std::string> defs_;
	bool need_def_;    // add variable definition when printing IR
	int step_;         // for assigning step number
	int looplv_;

	std::vector<FieldName*> inps_, oups_;

public:
	CodeGen() : mm_(nullptr), ext_(false), step_(0),
			looplv_(0), need_def_(false) {}

	void add(Doc *f) { seq_.push_back(f); }
	void useExternal() { ext_ = true; }
	int  nextStep() { return ++step_; }
	// for translating loops
	void incLoopLv() { looplv_++; }
	void decLoopLv() { looplv_--; }
	int  loopLv() { return looplv_; }
	// state transition
	int getStepSTM(STM *stm);
	void setStepSTM(int step, STM *stm);
	void addJump(STM *stm, int curStep);
	// aggregator manager
	void setAM(AggregatorManager *am) { am_ = am; }
	AggregatorManager *getAM() { return am_; }
	// message manager
	void setMM(MessageManager *mm) { mm_ = mm; }
	MessageManager *getMM() { return mm_; }
	// vertex manager
	void setVM(VertexManager *vm) { vm_ = vm; }
	VertexManager *getVM() { return vm_; }
	// for translating variables
	bool needDef();
	bool hasDef(VarIR *var);
	void defVar(VarIR *var);
	void clearDefs();
	// for input and output
	void addInputField(FieldName *fn) { inps_.push_back(fn); }
	void addOutputField(FieldName *fn) { oups_.push_back(fn); };
	SeqSTM *checkInput(SeqSTM *prog);
	std::set<std::string> getInputFields();
	std::vector<FieldName*> getOutputFNs() { return oups_; }
	// generate code (pregelPlus)
	void pregel_header();
	void pregel_code(SeqSTM *prog);
	void pregel_worker();
	void pregel_run();
	void pregel_main();
	// void dump
	void dump(FILE *f = stderr);
};

CodeGen *getCodeGen();
 // for user-defined vars (add step number to avoid conflict)
std::string rename(std::string name, int step);
void genPregelPlus(SeqSTM *prog, FILE *f = stdout);

#endif
