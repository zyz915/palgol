#ifndef AST_H
#define AST_H

#include <cstring>
#include <memory>
#include <string>
#include <vector>
#include <set>

#include "errorinfo.h"
#include "intermediate.h"
#include "tokenizer.h"
#include "types.h"

class Identifier;

// type alias
using StringSet = std::set<std::string>;

class AST {
public:
	virtual ~AST() {}
	virtual void print(FILE *f) const {
		fail("AST::print()");
	}
};

class EvalStrategy {
public:
	bool loc_ok, loc_lp; //  locally evaluate or not
	bool rmt_ok, rmt_lp; // remotely evaluate or not

	EvalStrategy() :
			loc_ok(true), loc_lp(false), rmt_ok(false), rmt_lp(false) {}
	EvalStrategy(bool l1, bool l2, bool r1, bool r2) :
			loc_ok(l1), loc_lp(l2), rmt_ok(r1), rmt_lp(r2) {}

	EvalStrategy(const EvalStrategy &a) = default;
	EvalStrategy &operator=(const EvalStrategy &a) = default;

	virtual void calcStrategy(IREnv *env, Identifier *name) {}
	virtual void copyFrom(const EvalStrategy &a) { *this = a; }

	static EvalStrategy combine(const EvalStrategy &a, const EvalStrategy &b);
};

enum class EType {
	e_int, e_float, e_bool, e_inf,
	e_name, e_field, e_edge,
	e_pair, e_rec, e_list, e_listc,
	e_if, e_not, e_vid,
	e_neg, e_sel, e_size,
	e_toi, e_tof, e_gsize,
	e_acc, e_binop, e_agg,
	e_func, e_cont,
};

class ExprAST : public AST, public Pos, public EvalStrategy {
protected:
	Type *type_;
	EType etype_; // type of AST node

public:
	ExprAST() = delete;
	ExprAST(Pos pos, EType etype) :
			Pos(pos), etype_(etype), type_(nullptr), EvalStrategy() {}
	virtual ~ExprAST() {}
	virtual EType etype() const final { return etype_; }
	virtual bool equals(const ExprAST *other) const = 0;
	virtual void subst(TypeEnv *env) final { type_ = type_->subst(env); }
	virtual bool contains(const Identifier *name) const { return false; }
	virtual Type *produceType(TypeEnv *env) = 0;
	virtual void initCheck(const StringSet &in) {}
	virtual void confCheck(const StringSet &in) {}
	virtual Type *type() const final { return type_; }
	virtual ExprIR *tr_loc(IREnv *env) = 0;
	virtual ExprIR *tr_nbr(IREnv *env, RemoteData *rd) = 0;
	virtual ExprIR *tr_try(IREnv *env, RemoteData *rd) = 0;
	virtual void calcStrategy(IREnv *env, Identifier *name) = 0;
};

class StmtAST : public AST, public Pos, public EvalStrategy {
protected:
	StmtAST() = delete;
	StmtAST(Pos pos) : Pos(pos), EvalStrategy() {}
public:
	virtual ~StmtAST() {}
	virtual void collectConstraints(TypeEnv *env) = 0;
	virtual void initCheck(StringSet &in, std::vector<RUStruct*> &rus) {}
	virtual void confCheck(StringSet &in) {}
	virtual void tr_loc(IREnv *env) = 0;
	virtual void tr_nbr(IREnv *env, RemoteData *rd) = 0;
	virtual void tr_try(IREnv *env, RemoteData *rd) = 0;
	virtual void calcStrategy(IREnv *env, Identifier *name) = 0;
};

class ListAST : public ExprAST {
public:
	ListAST(Pos pos, EType etype) : ExprAST(pos, etype) {}
	~ListAST() {}
	// override
	void calcStrategy(IREnv *env, Identifier *name) override {}
};

struct Operator : Pos {
	int type;
	Operator(Pos pos, int type) :
			Pos(pos), type(type) {}

};

/* Identifier */
class Identifier : public ExprAST {
	std::string name_;
public:
	Identifier(Pos pos, const char *str) :
			ExprAST(pos, EType::e_name), name_(str) {}

	const std::string &getName() const;
	bool equals(const char *name) const;
	// override
	bool equals(const ExprAST *other) const override;
	void print(FILE *f) const override;
	Type *produceType(TypeEnv *env) override;
	ExprIR *tr_loc(IREnv *env) override;
	ExprIR *tr_nbr(IREnv *env, RemoteData *rd) override;
	ExprIR *tr_try(IREnv *env, RemoteData *rd) override;
	void calcStrategy(IREnv *env, Identifier *name) override;
};

class FieldName : public AST, public Pos {
	std::string field_;
public:
	FieldName(Pos pos, const char *str) :
			Pos(pos), field_(str) {}
	FieldName(const char *str) :
			Pos(), field_(str) {}

	const std::string &getName() const;
	bool equals(const char *name) const;
	bool equals(const FieldName *other) const;
	// override
	void print(FILE *f) const override;
};

class FieldList : public AST {
	std::vector<FieldName*> list_;
public:
	FieldList() {}

	void add(FieldName *fn);
	int size() const;
	FieldName *get(int index);
	bool equals(const FieldList *other) const;
	// override
	void print(FILE *f) const override;
};

/* ConstAST values */
class ConstAST : public ExprAST {
public:
	ConstAST(Pos pos, EType type) : ExprAST(pos, type) {}
	// override
	void calcStrategy(IREnv *env, Identifier *name) override;
	ExprIR *tr_nbr(IREnv *env, RemoteData *rd) override;
	ExprIR *tr_try(IREnv *env, RemoteData *rd) override;
};

class IntExpr : public ConstAST {
private:
	int val_;
public:
	IntExpr(Token tok) : ConstAST(tok, EType::e_int),
			val_(atoi(tok.val)) {}
	int getVal() const { return val_; }
	// override
	bool equals(const ExprAST *other) const override;
	void print(FILE *f) const override;
	Type *produceType(TypeEnv *env) override;
	ExprIR *tr_loc(IREnv *env) override;
};

class FloatExpr : public ConstAST {
private:
	double val_;
public:
	FloatExpr(Token tok) : ConstAST(tok, EType::e_float),
			val_(atof(tok.val)) {}
	// override
	bool equals(const ExprAST *other) const override;
	void print(FILE *f) const override;
	Type *produceType(TypeEnv *env) override;
	ExprIR *tr_loc(IREnv *env) override;
};

class BoolExpr : public ConstAST {
private:
	bool val_;
public:
	BoolExpr(Token tok) : ConstAST(tok, EType::e_bool),
			val_(tok.type == tok_true) {}
	// override
	bool equals(const ExprAST *other) const override;
	void print(FILE *f) const override;
	Type *produceType(TypeEnv *env) override;
	ExprIR *tr_loc(IREnv *env) override;
};

class InfiniteExpr : public ConstAST {
public:
	InfiniteExpr(Pos pos) : ConstAST(pos, EType::e_inf) {}
	// override
	bool equals(const ExprAST *other) const override;
	void print(FILE *f) const override;
	Type *produceType(TypeEnv *env) override;
	ExprIR *tr_loc(IREnv *env) override;
};

class GraphSize : public ConstAST {
public:
	GraphSize(Pos pos) : ConstAST(pos, EType::e_gsize) {}
	// override
	bool equals(const ExprAST *other) const override;
	void print(FILE *f) const override;
	Type *produceType(TypeEnv *env) override;
	ExprIR *tr_loc(IREnv *env) override;
};

/* Expressions */
class ExprList : public AST, public EvalStrategy {
protected:
	std::vector<ExprAST*> list_;
public:
	ExprList() {}

	void add(ExprAST *expr);
	int size() const;
	ExprAST *get(int index);
	ExprAST *operator[](int index);
	// override
	void print(FILE *f) const override;
	void calcStrategy(IREnv *env, Identifier *name) override;
};

class FieldExpr : public ExprAST {
protected:
	FieldName *name_;
	ExprAST *expr_;
public:
	FieldExpr(Pos pos, FieldName *name, ExprAST *expr) :
			ExprAST(pos, EType::e_field), name_(name), expr_(expr) {}

	FieldExpr *copyThis(ExprAST *expr); // keep type info
	FieldName *getFieldName();
	ExprAST *getExpr();
	std::string getName() const;
	FieldExpr *replace(ExprAST *expr, Identifier *name);
	bool isSubExpr(ExprAST *vertex);
	// override
	bool equals(const ExprAST *other) const override;
	bool contains(const Identifier *name) const override;
	void print(FILE *f) const override;
	Type *produceType(TypeEnv *env) override;
	void initCheck(const StringSet &in) override;
	void confCheck(const StringSet &in) override;
	void conflict(const StringSet &in);
	ExprIR *tr_loc(IREnv *env) override;
	ExprIR *tr_nbr(IREnv *env, RemoteData *rd) override;
	ExprIR *tr_try(IREnv *env, RemoteData *rd) override;
	void calcStrategy(IREnv *env, Identifier *name) override;
};

class ListComp : public ListAST {
protected:
	ExprAST *expr_;
	Identifier *name_;
	ExprAST *gen_;
	ExprList *exprs_;
public:
	ListComp(Pos pos, ExprAST *expr, Identifier *name,
		ExprAST *gen, ExprList *exprs) :
			ListAST(pos, EType::e_listc), expr_(expr), 
			name_(name), gen_(gen), exprs_(exprs) {}
	// override
	bool equals(const ExprAST *other) const override;
	bool contains(const Identifier *name) const override;
	void print(FILE *f) const override;
	Type *produceType(TypeEnv *env) override;
	void initCheck(const StringSet &in) override;
	void confCheck(const StringSet &in) override;
	ExprIR *tr_loc(IREnv *env) override;
	ExprIR *tr_nbr(IREnv *env, RemoteData *rd) override;
	ExprIR *tr_try(IREnv *env, RemoteData *rd) override;
	void calcStrategy(IREnv *env, Identifier *name) override;
};

class AggExpr : public ExprAST {
protected:
	Operator *op_;
	ExprAST *expr_;
	Identifier *name_;
	ExprList *exprs_;
public:
	AggExpr(Pos pos, Operator *op, ExprAST *expr,
		Identifier *name, ExprList *exprs) :
			ExprAST(pos, EType::e_agg), op_(op),
			expr_(expr), name_(name), exprs_(exprs) {}
	// override
	bool equals(const ExprAST *other) const override;
	bool contains(const Identifier *name) const override;
	void print(FILE *f) const override;
	Type *produceType(TypeEnv *env) override;
	void initCheck(const StringSet &in) override;
	void confCheck(const StringSet &in) override;
	ExprIR *tr_loc(IREnv *env) override;
	ExprIR *tr_nbr(IREnv *env, RemoteData *rd) override;
	ExprIR *tr_try(IREnv *env, RemoteData *rd) override;
	void calcStrategy(IREnv *env, Identifier *name) override;
};

class PairExpr : public ExprAST {
protected:
	ExprAST *fst_, *snd_;
public:
	PairExpr(Pos pos, ExprAST *fst, ExprAST *snd) :
			ExprAST(pos, EType::e_pair), fst_(fst), snd_(snd) {}

	ExprAST *getFirst() { return fst_; }
	ExprAST *getSecond() { return snd_; }
	// override
	bool equals(const ExprAST *other) const override;
	bool contains(const Identifier *name) const override;
	void print(FILE *f) const override;
	Type *produceType(TypeEnv *env) override;
	void initCheck(const StringSet &in) override;
	void confCheck(const StringSet &in) override;
	ExprIR *tr_loc(IREnv *env) override;
	ExprIR *tr_nbr(IREnv *env, RemoteData *rd) override;
	ExprIR *tr_try(IREnv *env, RemoteData *rd) override;
	void calcStrategy(IREnv *env, Identifier *name) override;
};

class RecordExpr : public ExprAST {
protected:
	ExprAST *id_, *w_;
public:
	RecordExpr(Pos pos, ExprAST *id, ExprAST *w) :
		ExprAST(pos, EType::e_rec), id_(id), w_(w) {}

	ExprAST *getId() { return id_; }
	ExprAST *getW() { return w_; }
	// override
	bool equals(const ExprAST *other) const override;
	bool contains(const Identifier *name) const override;
	void print(FILE *f) const override;
	Type *produceType(TypeEnv *env) override;
	void initCheck(const StringSet &in) override;
	void confCheck(const StringSet &in) override;
	ExprIR *tr_loc(IREnv *env) override;
	ExprIR *tr_nbr(IREnv *env, RemoteData *rd) override;
	ExprIR *tr_try(IREnv *env, RemoteData *rd) override;
	void calcStrategy(IREnv *env, Identifier *name) override;
};

class ListExpr : public ListAST {
protected:
	std::vector<ExprAST*> list_;
public:
	ListExpr(Pos pos) : ListAST(pos, EType::e_list) {}

	void add(ExprAST *expr);
	int size() const;
	ExprAST *get(int index);
	// override
	bool equals(const ExprAST *other) const override;
	bool contains(const Identifier *name) const override;
	void print(FILE *f) const override;
	Type *produceType(TypeEnv *env) override;
	void initCheck(const StringSet &in) override;
	void confCheck(const StringSet &in) override;
	ExprIR *tr_loc(IREnv *env) override;
	ExprIR *tr_nbr(IREnv *env, RemoteData *rd) override;
	ExprIR *tr_try(IREnv *env, RemoteData *rd) override;
	void calcStrategy(IREnv *env, Identifier *name) override;
};

class IfExpr : public ExprAST {
public:
	ExprAST *cond_, *e1_, *e2_;
public:
	IfExpr(Pos pos, ExprAST *cond, ExprAST *e1, ExprAST *e2) :
			ExprAST(pos, EType::e_if), cond_(cond), e1_(e1), e2_(e2) {}
	// override
	bool equals(const ExprAST *other) const override;
	bool contains(const Identifier *name) const override;
	void print(FILE *f) const override;
	Type *produceType(TypeEnv *env) override;
	void initCheck(const StringSet &in) override;
	void confCheck(const StringSet &in) override;
	ExprIR *tr_loc(IREnv *env) override;
	ExprIR *tr_nbr(IREnv *env, RemoteData *rd) override;
	ExprIR *tr_try(IREnv *env, RemoteData *rd) override;
	void calcStrategy(IREnv *env, Identifier *name) override;
};

class UnaryExpr : public ExprAST {
protected:
	ExprAST *expr_;

	UnaryExpr(Pos pos, ExprAST *expr, EType type) :
			ExprAST(pos, type), expr_(expr) {}
public:
	virtual ~UnaryExpr() {}
	// override
	bool contains(const Identifier *name) const override;
	void initCheck(const StringSet &in) override;
	void confCheck(const StringSet &in) override;
	void calcStrategy(IREnv *env, Identifier *name) override;
};

class BinaryExpr : public ExprAST {
protected:
	Operator *op_;
	ExprAST *e1_, *e2_;
public:
	BinaryExpr(Pos pos, Operator *op, ExprAST *e1, ExprAST *e2) :
			ExprAST(pos, EType::e_binop), op_(op), e1_(e1), e2_(e2) {}
	// override
	bool equals(const ExprAST *other) const override;
	bool contains(const Identifier *name) const override;
	void print(FILE *f) const override;
	Type *produceType(TypeEnv *env) override;
	void initCheck(const StringSet &in) override;
	void confCheck(const StringSet &in) override;
	ExprIR *tr_loc(IREnv *env) override;
	ExprIR *tr_nbr(IREnv *env, RemoteData *rd) override;
	ExprIR *tr_try(IREnv *env, RemoteData *rd) override;
	void calcStrategy(IREnv *env, Identifier *name) override;
};

class AccumExpr : public UnaryExpr {
protected:
	Operator *op_;
public:
	AccumExpr(Pos pos, Operator *op, ListAST *expr) :
			UnaryExpr(pos, expr, EType::e_acc), op_(op) {}
	// override
	bool equals(const ExprAST *other) const override;
	void print(FILE *f) const override;
	ExprIR *tr_loc(IREnv *env) override;
	ExprIR *tr_nbr(IREnv *env, RemoteData *rd) override;
	ExprIR *tr_try(IREnv *env, RemoteData *rd) override;
	Type *produceType(TypeEnv *env) override;
};

class NegativeExpr : public UnaryExpr {
public:
	NegativeExpr(Pos pos, ExprAST *expr) :
			UnaryExpr(pos, expr, EType::e_neg) {}
	// override
	bool equals(const ExprAST *other) const override;
	void print(FILE *f) const override;
	Type *produceType(TypeEnv *env) override;
	ExprIR *tr_loc(IREnv *env) override;
	ExprIR *tr_nbr(IREnv *env, RemoteData *rd) override;
	ExprIR *tr_try(IREnv *env, RemoteData *rd) override;
};

class NotExpr : public UnaryExpr {
public:
	NotExpr(Pos pos, ExprAST *expr) :
			UnaryExpr(pos, expr, EType::e_not) {}

	ExprAST *getExpr() { return expr_; }
	// override
	bool equals(const ExprAST *other) const override;
	void print(FILE *f) const override;
	Type *produceType(TypeEnv *env) override;
	ExprIR *tr_loc(IREnv *env) override;
	ExprIR *tr_nbr(IREnv *env, RemoteData *rd) override;
	ExprIR *tr_try(IREnv *env, RemoteData *rd) override;
};

class VidExpr : public UnaryExpr {
public:
	VidExpr(Pos pos, ExprAST *expr) :
			UnaryExpr(pos, expr, EType::e_vid) {}
	// override
	bool equals(const ExprAST *other) const override;
	void print(FILE *f) const override;
	Type *produceType(TypeEnv *env) override;
	void initCheck(const StringSet &in) override;
	ExprIR *tr_loc(IREnv *env) override;
	ExprIR *tr_nbr(IREnv *env, RemoteData *rd) override;
	ExprIR *tr_try(IREnv *env, RemoteData *rd) override;
};

class SelectExpr : public UnaryExpr {
protected:
	Operator *op_; 
public:
	SelectExpr(Pos pos, Operator *op, ExprAST *expr, EType type=EType::e_sel) :
			UnaryExpr(pos, expr, EType::e_sel), op_(op) {}
	// override
	bool equals(const ExprAST *other) const override;
	void print(FILE *f) const override;
	Type *produceType(TypeEnv *env) override;
	ExprIR *tr_loc(IREnv *env) override;
	ExprIR *tr_nbr(IREnv *env, RemoteData *rd) override;
	ExprIR *tr_try(IREnv *env, RemoteData *rd) override;
};

class SizeExpr : public UnaryExpr {
public:
	SizeExpr(Pos pos, ExprAST *expr) :
			UnaryExpr(pos, expr, EType::e_size) {}
	// override
	bool equals(const ExprAST *other) const override;
	void print(FILE *f) const override;
	Type *produceType(TypeEnv *env) override;
	ExprIR *tr_loc(IREnv *env) override;
	ExprIR *tr_nbr(IREnv *env, RemoteData *rd) override;
	ExprIR *tr_try(IREnv *env, RemoteData *rd) override;
};

class ToiExpr : public UnaryExpr {
public:
	ToiExpr(Pos pos, ExprAST *expr) :
			UnaryExpr(pos, expr, EType::e_toi) {}
	// override
	bool equals(const ExprAST *other) const override;
	void print(FILE *f) const override;
	Type *produceType(TypeEnv *env) override;
	ExprIR *tr_loc(IREnv *env) override;
	ExprIR *tr_nbr(IREnv *env, RemoteData *rd) override;
	ExprIR *tr_try(IREnv *env, RemoteData *rd) override;
};

class TofExpr : public UnaryExpr {
public:
	TofExpr(Pos pos, ExprAST *expr) :
			UnaryExpr(pos, expr, EType::e_tof) {}
	// override
	bool equals(const ExprAST *other) const override;
	void print(FILE *f) const override;
	Type *produceType(TypeEnv *env) override;
	ExprIR *tr_loc(IREnv *env) override;
	ExprIR *tr_nbr(IREnv *env, RemoteData *rd) override;
	ExprIR *tr_try(IREnv *env, RemoteData *rd) override;
};

class EdgeExpr : public SelectExpr {
public:
	EdgeExpr(Pos pos, Operator *op, ExprAST *expr) :
			SelectExpr(pos, op, expr, EType::e_edge) {}
	// override
	bool equals(const ExprAST *other) const override;
	void print(FILE *f) const override;
	Type *produceType(TypeEnv *env) override;
	ExprIR *tr_loc(IREnv *env) override;
	ExprIR *tr_nbr(IREnv *env, RemoteData *rd) override;
	ExprIR *tr_try(IREnv *env, RemoteData *rd) override;
	void calcStrategy(IREnv *env, Identifier *name) override;
};

class FuncExpr : public ExprAST {
protected:
	Identifier *name_;
	ExprList *exprs_;
public:
	FuncExpr(Pos pos, Identifier *name, ExprList *exprs) :
			ExprAST(pos, EType::e_func), name_(name), exprs_(exprs) {}
	// override
	bool equals(const ExprAST *other) const override;
	void print(FILE *f) const override;
	Type *produceType(TypeEnv *env) override;
	void initCheck(const StringSet &in) override;
	void confCheck(const StringSet &in) override;
	FunctionIR *tr_loc(IREnv *env) override;
	FunctionIR *tr_nbr(IREnv *env, RemoteData *rd) override;
	ExprIR *tr_try(IREnv *env, RemoteData *rd) override;
	void calcStrategy(IREnv *env, Identifier *name) override;
};

class ContainsExpr : public ExprAST {
protected:
	ExprAST *e1_, *e2_;
public:
	ContainsExpr(Pos pos, ExprAST *e1, ExprAST *e2) :
			ExprAST(pos, EType::e_cont), e1_(e1), e2_(e2) {}
	// override
	bool equals(const ExprAST *other) const override;
	bool contains(const Identifier *name) const override;
	void print(FILE *f) const override;
	Type *produceType(TypeEnv *env) override;
	void initCheck(const StringSet &in) override;
	void confCheck(const StringSet &in) override;
	ExprIR *tr_loc(IREnv *env) override;
	ExprIR *tr_nbr(IREnv *env, RemoteData *rd) override;
	ExprIR *tr_try(IREnv *env, RemoteData *rd) override;
	void calcStrategy(IREnv *env, Identifier *name) override;
};

/* Statements */
class StmtList : public StmtAST {
protected:
	std::vector<StmtAST*> list_;
public:
	StmtList() : StmtAST(Pos()) {}

	void add(StmtAST *stmt);
	int size() const;
	StmtAST *get(int index);
	StmtAST *operator[](int index);
	// override
	void print(FILE *f) const override;
	void collectConstraints(TypeEnv *env) override;
	void initCheck(StringSet &in, std::vector<RUStruct*> &rus) override;
	void confCheck(StringSet &in) override;
	void tr_loc(IREnv *env) override;
	void tr_nbr(IREnv *env, RemoteData *rd) override;
	void tr_try(IREnv *env, RemoteData *rd) override;
	void calcStrategy(IREnv *env, Identifier *name) override;
};

/* if statement (e.g., if (x > y) .. else .. end) */
class IfStmt : public StmtAST {
protected:
	ExprAST *cond_;
	StmtList *s1_, *s2_;
public:
	IfStmt(Pos pos, ExprAST *cond, StmtList *s1, StmtList *s2) :
			StmtAST(pos), cond_(cond), s1_(s1), s2_(s2) {}
	// override
	void print(FILE *f) const override;
	void collectConstraints(TypeEnv *env) override;
	void initCheck(StringSet &in, std::vector<RUStruct*> &rus) override;
	void confCheck(StringSet &in) override;
	void tr_loc(IREnv *env) override;
	void tr_nbr(IREnv *env, RemoteData *rd) override;
	void tr_try(IREnv *env, RemoteData *rd) override;
	void calcStrategy(IREnv *env, Identifier *name) override;
};

/* for statement (e.g., for (x <- Nbr[u]) ..) */
class ForStmt : public StmtAST {
protected:
	Identifier *name_;
	ExprAST *expr_;
	StmtList *stmts_;
	ExprList *exprs_;
public:
	ForStmt(Pos pos, Identifier *name, ExprAST *expr, StmtList *stmts, ExprList *exprs) :
			StmtAST(pos), name_(name), expr_(expr), stmts_(stmts), exprs_(exprs) {}
	// override
	void print(FILE *f) const override;
	void collectConstraints(TypeEnv *env) override;
	void initCheck(StringSet &in, std::vector<RUStruct*> &rus) override;
	void confCheck(StringSet &in) override;
	void tr_loc(IREnv *env) override;
	void tr_nbr(IREnv *env, RemoteData *rd) override;
	void tr_try(IREnv *env, RemoteData *rd) override;
	void calcStrategy(IREnv *env, Identifier *name) override;
};

/* let binding (e.g., let mut x = 5) */
class LetStmt : public StmtAST {
protected:
	Identifier *name_;
	ExprAST *expr_;
	Type *type_;
	bool mut_;
public:
	LetStmt(Pos pos, Identifier *name, ExprAST *expr,
			bool mut = false, Type *t = nullptr) : StmtAST(pos),
			name_(name), expr_(expr), mut_(mut), type_(t) {}
	// override
	void print(FILE *f) const override;
	void collectConstraints(TypeEnv *env) override;
	void initCheck(StringSet &in, std::vector<RUStruct*> &rus) override;
	void confCheck(StringSet &in) override;
	void tr_loc(IREnv *env) override;
	void tr_nbr(IREnv *env, RemoteData *rd) override;
	void tr_try(IREnv *env, RemoteData *rd) override;
	void calcStrategy(IREnv *env, Identifier *name) override;
};

/* procedure call (e.g., let _ = func(..) ) */
class ProcStmt : public StmtAST {
protected:
	FuncExpr *func_;
public:
	ProcStmt(Pos pos, FuncExpr *func) : StmtAST(pos), func_(func) {}
	// override
	void print(FILE *f) const override;
	void collectConstraints(TypeEnv *env) override;
	void initCheck(StringSet &in, std::vector<RUStruct*> &rus) override;
	void confCheck(StringSet &in) override;
	void tr_loc(IREnv *env) override;
	void tr_nbr(IREnv *env, RemoteData *rd) override;
	void tr_try(IREnv *env, RemoteData *rd) override;
	void calcStrategy(IREnv *env, Identifier *name) override;
};

/* local updating statement (e.g., local D[u] = true) */
class LocalUStmt : public StmtAST {
protected:
	FieldName *fn_;
	Operator *op_;
	ExprAST *expr_;
public:
	LocalUStmt(Pos pos, FieldName *fn, Operator *op, ExprAST *expr) :
			StmtAST(pos), fn_(fn), op_(op), expr_(expr) {}
	// override
	void print(FILE *f) const override;
	void collectConstraints(TypeEnv *env) override;
	void initCheck(StringSet &in, std::vector<RUStruct*> &rus) override;
	void confCheck(StringSet &in) override;
	void tr_loc(IREnv *env) override;
	void tr_nbr(IREnv *env, RemoteData *rd) override;
	void tr_try(IREnv *env, RemoteData *rd) override;
	void calcStrategy(IREnv *env, Identifier *name) override;
};

/* remote updating statement (e.g., remote D[D[u]] = x) */
class RemoteUStmt : public StmtAST {
protected:
	FieldName *fn_;
	ExprAST *dest_;
	Operator *op_;
	ExprAST *expr_;
public:
	RemoteUStmt(Pos pos, FieldName *fn, ExprAST *dest, Operator *op, ExprAST *expr) :
			StmtAST(pos), fn_(fn), dest_(dest), op_(op), expr_(expr) {}
	// override
	void print(FILE *f) const override;
	void collectConstraints(TypeEnv *env) override;
	void initCheck(StringSet &in, std::vector<RUStruct*> &rus) override;
	void confCheck(StringSet &in) override;
	void tr_loc(IREnv *env) override;
	void tr_nbr(IREnv *env, RemoteData *rd) override;
	void tr_try(IREnv *env, RemoteData *rd) override;
	void calcStrategy(IREnv *env, Identifier *name) override;
};

/* assignment (e.g. mut x += 1; ) */
class MutateStmt : public StmtAST {
protected:
	Identifier *name_;
	Operator *op_;
	ExprAST *expr_;
public:
	MutateStmt(Pos pos, Identifier *name, Operator *op, ExprAST *expr) :
		StmtAST(pos), name_(name), op_(op), expr_(expr) {}
	// override
	void print(FILE *f) const override;
	void collectConstraints(TypeEnv *env) override;
	void initCheck(StringSet &in, std::vector<RUStruct*> &rus) override;
	void confCheck(StringSet &in) override;
	void tr_loc(IREnv *env) override;
	void tr_nbr(IREnv *env, RemoteData *rd) override;
	void tr_try(IREnv *env, RemoteData *rd) override;
	void calcStrategy(IREnv *env, Identifier *name) override;
};

/* vertex-centric program */
class VCProg : public AST {
public:
	virtual ~VCProg() {}
	virtual void collectConstraints(TypeEnv *env) = 0;
	virtual void initCheck(StringSet &in) = 0;
	virtual void confCheck() {}
	virtual SeqSTM *transform(IREnv *env) = 0;
};

class BasicVC : public VCProg {
protected:
	Identifier *name_;
	StmtList *stmts_;
	ExprAST *cond_;
public:
	BasicVC(Identifier *name, StmtList *stmts, ExprAST *cond = nullptr) :
			name_(name), stmts_(stmts), cond_(cond) {}
	// override
	void print(FILE *f) const override;
	void collectConstraints(TypeEnv *env) override;
	void initCheck(StringSet &in) override;
	void confCheck() override;
	SeqSTM *transform(IREnv *env) override;
};

class StopVC : public VCProg {
protected:
	Identifier *name_;
	ExprAST *cond_; // might be nullptr
public:
	StopVC(Identifier *name, ExprAST *cond) :
			name_(name), cond_(cond) {}
	// override
	void print(FILE *f) const override;
	void collectConstraints(TypeEnv *env) override;
	void initCheck(StringSet &in) override;
	SeqSTM *transform(IREnv *env) override;
};

class ComposeVC : public VCProg {
protected:
	VCProg *head_, *tail_;
public:
	ComposeVC(VCProg *head, VCProg *tail) :
			head_(head), tail_(tail) {}
	// override
	void print(FILE *f) const override;
	void collectConstraints(TypeEnv *env) override;
	void initCheck(StringSet &in) override;
	void confCheck() override;
	SeqSTM *transform(IREnv *env) override;
};

class IterVC : public VCProg {
protected:
	VCProg *prog_;

public:
	IterVC(VCProg *prog) : prog_(prog) {}

	void print(FILE *f) const override = 0;
	void collectConstraints(TypeEnv *env) override;
	void initCheck(StringSet &in) override;
	void confCheck() override;
	SeqSTM *transform(IREnv *env) override = 0;
};

class RepeatVC : public IterVC {
protected:
	IntExpr *expr_;

public:
	RepeatVC(VCProg *prog, IntExpr *expr) :
			IterVC(prog), expr_(expr) {}
	// override
	void print(FILE *f) const override;
	SeqSTM *transform(IREnv *env) override;
};

class IterRangeVC : public IterVC {
protected:
	Identifier *name_;
	IntExpr *lo_, *hi_;

public:
	IterRangeVC(VCProg *prog, Identifier *name, IntExpr *lo, IntExpr *hi) :
		IterVC(prog), name_(name), lo_(lo), hi_(hi) {}
	// override
	void print(FILE *f) const override;
	void collectConstraints(TypeEnv *env) override;
	SeqSTM *transform(IREnv *env) override;
};

class FixPointVC : public IterVC {
protected:
	FieldList *list_;

public:
	FixPointVC(VCProg *prog, FieldList *list) :
			IterVC(prog), list_(list) {}
	// override
	void print(FILE *f) const override;
	void initCheck(StringSet &in) override;
	SeqSTM *transform(IREnv *env) override;
};

class ExistsVC : public IterVC {
public:
	ExistsVC(VCProg *prog) : IterVC(prog) {}
	// override
	void print(FILE *f) const override;
	SeqSTM *transform(IREnv *env) override;
};

class ForallVC : public IterVC {
public:
	ForallVC(VCProg *prog) : IterVC(prog) {}
	// override
	void print(FILE *f) const override;
	SeqSTM *transform(IREnv *env) override;
};

void debug(const AST *ast, FILE *f = stderr);
Operator *getOper(int tok);

#endif
