#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <algorithm>
#include <vector>

#include "tokenizer.h"
#include "errorinfo.h"

using namespace std;

static int line = 0;

static int ind_stack[256], ind_top;
static char buff[256];
static int bptr;
static int no_tok;

static char br_stack[256], br_top;

static FILE *fin = stdin;
void setInputStream(FILE *f) {
	if (f == nullptr) 
		prtError("input file does not exist");
	fin = f;
}

static struct { int type, cnt; } indent;
static void getLine() {
	line += 1;
	no_tok = (fgets(buff, 256, fin) == nullptr);
	setline(line, buff);
	if (!no_tok) {
		bptr = 0;
		indent.cnt = 0;
		int l = strlen(buff);
		while (buff[bptr] && isspace(buff[bptr]))
			bptr++;
		if (bptr < l && br_top == 0) {
			if (bptr > ind_stack[ind_top]) {
				indent.type = tok_lblock;
				indent.cnt++;
				ind_stack[++ind_top] = bptr;
			} else {
				while (bptr < ind_stack[ind_top]) {
					ind_top--;
					indent.type = tok_rblock;
					indent.cnt++;
				}
				if (ind_stack[ind_top] != bptr)
					prtError(Pos(line, 0), "indent error");
			}
		}
	} else {
		indent.type = tok_rblock;
		indent.cnt = ind_top;
	}
}

static Token createToken(int row, int col, int type, char *val = nullptr) {
	return Token(row, col + 1, type, val);
}

static void copyString(char *dst, char *src, int len) {
	memcpy(dst, src, len);
	dst[len] = 0;
}

typedef pair<const char *, int> TokRec;
static vector<TokRec> names;

static bool cmp(const TokRec &a, const TokRec &b) {
	return strcmp(a.first, b.first) < 0;
}

void initTokenizer() {
	names.clear();
	names.push_back(make_pair("for", tok_for));
	names.push_back(make_pair("in", tok_in));
	names.push_back(make_pair("do", tok_do));
	names.push_back(make_pair("until", tok_until));
	names.push_back(make_pair("fix", tok_fix));
	names.push_back(make_pair("repeat", tok_repeat));
	names.push_back(make_pair("exists", tok_exists));
	names.push_back(make_pair("forall", tok_forall));
	names.push_back(make_pair("end", tok_end));
	names.push_back(make_pair("iter", tok_iter));
	names.push_back(make_pair("range", tok_range));
	names.push_back(make_pair("if", tok_if));
	names.push_back(make_pair("else", tok_else));
	names.push_back(make_pair("let", tok_let));
	names.push_back(make_pair("mut", tok_mut));
	names.push_back(make_pair("agg", tok_agg));
	names.push_back(make_pair("true", tok_true));
	names.push_back(make_pair("false", tok_false));
	names.push_back(make_pair("fst", tok_fst));
	names.push_back(make_pair("snd", tok_snd));
	names.push_back(make_pair("to_int", tok_toi));
	names.push_back(make_pair("to_float", tok_tof));
	names.push_back(make_pair("V", tok_V));
	names.push_back(make_pair("Id", tok_id));
	names.push_back(make_pair("inf", tok_inf));
	names.push_back(make_pair("local", tok_local));
	names.push_back(make_pair("remote", tok_remote));
	names.push_back(make_pair("stop", tok_stop));
	names.push_back(make_pair("where", tok_where));
	names.push_back(make_pair("input", tok_inp));
	names.push_back(make_pair("output", tok_oup));
	names.push_back(make_pair("field", tok_field));
	names.push_back(make_pair("type", tok_type));
	names.push_back(make_pair("extern", tok_extern));
	names.push_back(make_pair("maximum", tok_buildin));
	names.push_back(make_pair("minimum", tok_buildin));
	names.push_back(make_pair("sum", tok_buildin));
	names.push_back(make_pair("and", tok_buildin));
	names.push_back(make_pair("or", tok_buildin));
	names.push_back(make_pair("xor", tok_buildin));
	names.push_back(make_pair("random", tok_buildin));
	names.push_back(make_pair("arbitrary", tok_buildin));
	sort(names.begin(), names.end(), cmp);
}

static Token lookupName(char *buf, int col) {
	int lo = -1, hi = names.size(), mid;
	while (lo + 1 < hi) {
		mid = (lo + hi) >> 1;
		int c = strcmp(buf, names[mid].first);
		if (c == 0)
			return createToken(line, col, 
				names[mid].second, strdup(buf));
		if (c < 0)
			hi = mid;
		else
			lo = mid;
	}
	if (isupper(buf[0]))
		return createToken(line, col, tok_fname, strdup(buf));
	else
		return createToken(line, col, tok_name, strdup(buf));
}

static Token gettok() {
	static char buf_[32];
	while (!no_tok && !indent.cnt && !buff[bptr])
		getLine();
	if (indent.cnt != 0) {
		indent.cnt -= 1;
		return createToken(line, 0, indent.type);
	}
	if (no_tok)
		return createToken(line, 0, tok_eof);

	while (true) {
		char ch = buff[bptr];
		int col = bptr++;
		if (ch == 0) {
			getLine();
			return getNextToken(); // reach EOL
		}
		if (isspace(ch))
			continue;
		if (isalpha(ch)) {
			while (isalnum(buff[bptr]) || buff[bptr] == '_') bptr++;
			copyString(buf_, buff + col, bptr - col);
			return lookupName(buf_, col);
		}
		if (isdigit(ch)) {
			while (isdigit(buff[bptr]))
				bptr++;
			if (buff[bptr] == '.') {
				bptr++;
				while (isdigit(buff[bptr])) bptr++;
				copyString(buf_, buff + col, bptr - col);
				return createToken(line, col, tok_float, strdup(buf_));
			} else {
				copyString(buf_, buff + col, bptr - col);
				return createToken(line, col, tok_int, strdup(buf_));
			}
		}
		switch (ch) {
		case '[':
			br_stack[++br_top] = '[';
			return createToken(line, col, tok_lbracket);
		case ']':
			if (br_stack[br_top] != '[')
				prtError(Pos(line, col), "mismatched brackets");
			br_top -= 1;
			return createToken(line, col, tok_rbracket);
		case '(':
			br_stack[++br_top] = '(';
			return createToken(line, col, tok_lparen);
		case ')':
			if (br_stack[br_top] != '(')
				prtError(Pos(line, col), "mismatched parenthesis");
			br_top -= 1;
			return createToken(line, col, tok_rparen);
		case '{':
			br_stack[++br_top] = '{';
			return createToken(line, col, tok_lbrace);
		case '}':
			if (br_stack[br_top] != '{')
				prtError(Pos(line, col), "mismatched braces");
			br_top -= 1;
			return createToken(line, col, tok_rbrace);
		case '<':
			if (!strncmp(buff + col, "<-", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_from);
			} else
			if (!strncmp(buff + col, "<?=", 3)) {
				bptr = col + 3;
				return createToken(line, col, tok_mmin);
			} else
			if (!strncmp(buff + col, "<=", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_le);
			} else
			if (!strncmp(buff + col, "<<", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_shl);
			} else
				return createToken(line, col, tok_lt);
		case '>':
			if (!strncmp(buff + col, ">?=", 3)) {
				bptr = col + 3;
				return createToken(line, col, tok_mmax);
			} else
			if (!strncmp(buff + col, ">=", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_ge);
			} else
			if (!strncmp(buff + col, ">>", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_shr);
			} else
				return createToken(line, col, tok_gt);
		case '+':
			if (!strncmp(buff + col, "+=", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_madd);
			} else
				return createToken(line, col, tok_add);
		case '-':
			if (!strncmp(buff + col, "-=", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_msub);
			} else
			if (!strncmp(buff + col, "->", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_to);
			} else
				return createToken(line, col, tok_sub);
		case '*':
			if (!strncmp(buff + col, "*=", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_mmul);
			} else
				return createToken(line, col, tok_mul);
		case '/':
			if (!strncmp(buff + col, "//", 2)) {
				getLine();
				return getNextToken();
			}
			if (!strncmp(buff + col, "/=", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_mdiv);
			} else
				return createToken(line, col, tok_div);
		case '=':
			if (!strncmp(buff + col, "==", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_eq);
			} else
				return createToken(line, col, tok_bind);
		case '&':
			if (!strncmp(buff + col, "&=", 2)) {
				bptr = col + 3;
				return createToken(line, col, tok_mand);
			} else
			if (!strncmp(buff + col, "&&", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_and);
			} else
				return createToken(line, col, tok_bita);
		case '|':
			if (!strncmp(buff + col, "|=", 2)) {
				bptr = col + 3;
				return createToken(line, col, tok_mor);
			} else
			if (!strncmp(buff + col, "||", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_or);
			} else
				return createToken(line, col, tok_bar);
		case '_':
			return createToken(line, col, tok_ul);
		case '~':
			return createToken(line, col, tok_bitn);
		case '^':
			if (!strncmp(buff + col, "^=", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_mxor);
			} else
				return createToken(line, col, tok_xor);
		case '!':
			if (!strncmp(buff + col, "!=", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_ne);
			} else
			return createToken(line, col, tok_not);
		case '?':
			return createToken(line, col, tok_ques);
		case ':':
			if (!strncmp(buff + col, ":=", 2)) {
				bptr = col + 2;
				return createToken(line, col, tok_mass);
			} else
			return createToken(line, col, tok_colon);
		case ',':
			return createToken(line, col, tok_comma);
		case '.':
			if (!strncmp(buff + col, ".append", 7)) {
				bptr = col + 7;
				return createToken(line, col, tok_mapp);
			} else
			if (!strncmp(buff + col, ".remove", 7)) {
				bptr = col + 7;
				return createToken(line, col, tok_mrem);
			} else
				return createToken(line, col, tok_dot);
		}
	}
	return createToken(line, 0, tok_eof);
}

Token CurTok;
Token getNextToken() {
	return CurTok = gettok();
}

static const char *token_strs[] = {
	"", "EOF", "INC", "DEC",
	"_I", "_F", "_B", // basic data types
	"name", "field",
	"[", "]", "(", ")", "{", "}", "<-", "->", "|", "_", "V",
	"Id", "inf", "local", "remote", "stop", "where",
	"input", "output", "field", "type", "extern",
	":=", ">?=", "<?=", "+=", "-=", "*=", "/=", "&=", "|=", "^=",
	".append", ".remove", ".random", ".arbitrary", // should not be printed
	"+", "-", "*", "/", "==", "!=", "<", "<=", ">", ">=",
	"&&", "||", "!", "&", "|", "~", "^", "<<", ">>",
	"?", ":", ",", ".", "=",
	"for", "in", "do", "until", "fix", "exist", "forall", "repeat",
	"end", "iter", "range", "if", "else", "let", "mut", "agg",
	"true", "false", "fst", "snd",
	"to_int", "to_float",
	"_BU", ""  // others
};

const char *getTokenStr(Token tok) {
	if ((tok.type >= tok_int && tok.type <= tok_fname) ||
			(tok.type == tok_buildin))
		return tok.val;
	return token_strs[tok.type];
}

const char *getTokenStr(int type) {
	return token_strs[type];
}
