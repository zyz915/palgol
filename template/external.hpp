#ifndef EXTERNAL_HPP
#define EXTERNAL_HPP

#include <cstdlib>
#include <algorithm>
#include <vector>

using namespace std;

// type [{id:vid, w:(int, (int, int))}] in DSL
typedef vector<pair<int, pair<int, pair<int, int> > > > EdgeType;

// this function is used in msf.pal
EdgeType merge(EdgeType &v) {
	EdgeType ret;
    sort(v.begin(), v.end());
    for (int i = 0; i < v.size(); i++)
        if (i == 0 || v[i].first != v[i-1].first)
            ret.push_back(v[i]);
    return ret;
}

// this function is used in gc.pal
int rand_int(int range) {
    return (range > 0 ? rand() % range : 0);
}

// type (int, (int, int)) in DSL
typedef pair<int, pair<int, int> > Edge;

bool cmpEdge(const Edge &a, const Edge &b) {
	if (a.first != b.first) return a.first < b.first;
	int ai = min(a.second.first, a.second.second);
	int ax = max(a.second.first, a.second.second);
	int bi = min(b.second.first, b.second.second);
	int bx = max(b.second.first, b.second.second);
	return (ai != bi ? ai < bi : ax < bx);
}

// this function is used in msf_SEAS.pal
Edge minEdge(const vector<Edge> &es) {
	static const int inf = 0x3fffffff;
	Edge ret = make_pair(inf, make_pair(inf, inf));
	for (const auto &e : es)
		if (cmpEdge(e, ret)) ret = e;
	return ret;
}

// this function is used in msf_SEAS.pal
bool findItem(const vector<int> &vec, int x) {
	return find(vec.begin(), vec.end(), x) != vec.end();
}

// es: an edge list of a vertex
// rm: a list of vertex id that need to be removed 
void removeEdges(vector<int> &es, vector<int> &rm) {
	if (es.empty() || es.size() == rm.size()) {
		es.clear();
		rm.clear();
		return;
	}
	sort(rm.begin(), rm.end());
	int ptr = 0;
	vector<int> ret;
	for (int i = 0; i < es.size(); i++)
		if (ptr >= rm.size() || es[i] != rm[ptr])
			ret.push_back(es[i]);
		else
			ptr++;
	es.swap(ret);
	rm.clear();
}

#endif
