#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "ast.h"
#include "codegen.h"
#include "config.h"
#include "intermediate.h"
#include "parser.h"
#include "types.h"

static const char *inpfile = "";
static const char *outfile = "run.cpp";

int usage(const char *prog) {
	fprintf(stderr, "usage: %s [options] <source file>\n", prog);
	fprintf(stderr, "  where options are:\n");
	fprintf(stderr, "    -o=<string>      output file (a C++ program), default: \"run.cpp\"\n");
	fprintf(stderr, "    -e=<string>      file for external functions, default: \"external.hpp\"\n");
	fprintf(stderr, "    -spaces=<int>    number of spaces for indentation (in C++ code), default: 4\n");
	fprintf(stderr, "    -use-tab         use tab instead of spaces for indentation (in C++ code)\n");
	fprintf(stderr, "    -type            print type signatures of external functions\n");
	fprintf(stderr, "    -fuse=on/off     whether to enable the fuse optimization, default: on\n");
	fprintf(stderr, "\n");
	return 0;
}

int main(int argc, char **argv)
{
	Config *conf = getConfig();
	int p = 1;
	while (p <= argc) {
		if (p == argc) {
			if (argc > 1)
				fprintf(stderr, "error: no input file\n");
			return usage(argv[0]);
		}
		const char *flag = argv[p++];
		if (flag[0] != '-') {
			inpfile = flag;
			break;
		}
		if (!strcmp(flag, "-h") || !strcmp(flag, "--help"))
			return usage(argv[0]);
		if (!strcmp(flag, "-use-tab")) {
			conf->spaces = 0;
			continue;
		}
		if (!strncmp(flag, "-spaces=", 8)) {
			conf->spaces = atoi(flag + 8);
			continue;
		}
		if (!strncmp(flag, "-target=", 8)) {
			conf->target = strdup(flag + 8);
			continue;
		}
		if (!strncmp(flag, "-mode=", 6)) {
			conf->mode = strdup(flag + 6);
			continue;
		}
		if (!strcmp(flag, "-type")) {
			conf->suggest = 1;
			continue;
		}
		if (!strncmp(flag, "-fuse=", 6)) {
			if (!strncmp(flag + 6, "on", 2))
				conf->fuse = 1;
			if (!strncmp(flag + 6, "off", 3))
				conf->fuse = 0;
			continue;
		}
		if (!strncmp(flag, "-o=", 3)) {
			outfile = strdup(flag + 3);
			continue;
		}
		if (!strncmp(flag, "-e=", 3)) {
			conf->external = strdup(flag + 3);
			continue;
		}
		if (p == argc) {
			fprintf(stderr, "require arguments after \"%s\" flag.\n", flag);
			return usage(argv[0]);
		}
		const char *argu = argv[p++];
		if (!strcmp(flag, "-o")) {
			outfile = argu;
		} else
		if (!strcmp(flag, "-e")) {
			conf->external = argu;
		} else {
			fprintf(stderr, "unrecognized option '%s'\n", flag);
			return usage(argv[0]);
		}
	}
	setInputStream(fopen(inpfile, "r"));
	/* compiling */
	VCProg *prog = parse();
	typeChecking(prog);
	char err[128];
	if (!strcmp(conf->target, "pregel+")) {
		bool reqresp = !strcmp(conf->mode, "reqresp");
		if (!reqresp && strcmp(conf->mode, "basic")) {
			sprintf(err, "unknown mode \"%s\"\n", conf->mode);
			prtError(err);
		}
		SeqSTM *ir = transform(prog, reqresp, false);
		genPregelPlus(ir, fopen(outfile, "w"));
	} else {
		sprintf(err, "unknown target system \"%s\"\n", conf->target);
		prtError(err);
	}
	return 0;
}

