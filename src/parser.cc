#include <vector>
#include <stack>

#include "ast.h"
#include "codegen.h"
#include "errorinfo.h"
#include "parser.h"
#include "tokenizer.h"
#include "types.h"

using namespace std;

extern Token CurTok;

const char *ref_ = "ref";
const char *val_ = "val";

static char err[256]; // buffer for error message
static void eatToken(int type) {
	if (CurTok.type != type) {
		if (CurTok.type == tok_rblock || CurTok.type == tok_lblock ||
			type == tok_rblock || type == tok_lblock)
			sprintf(err, "wrong indentation");
		else {
			Token tmp(1, 1, type);
			sprintf(err, "expect '%s' but '%s' is found",
					getTokenStr(tmp), getTokenStr(CurTok));
		}
		prtParseError(CurTok, err);
	}
	getNextToken();
}

static FieldName *eatFieldToken() {
	if (CurTok.type != tok_fname && CurTok.type != tok_id) {
		sprintf(err, "expect field name but '%s' is found",
				getTokenStr(CurTok));
		prtParseError(CurTok, err);
	}
	FieldName *ret = new FieldName(CurTok, CurTok.val);
	getTypeEnv()->addField(ret); // provide position
	getNextToken();
	return ret;
}

static Identifier *eatNameToken() {
	if (CurTok.type != tok_name) {
		sprintf(err, "expect identifier but '%s' is found",
				getTokenStr(CurTok));
		prtParseError(CurTok, err);
	}
	Identifier *ret = new Identifier(CurTok, CurTok.val);
	getNextToken();
	return ret;
}

static Operator *eatAssignToken() {
	Operator *ret = nullptr;
	int type = CurTok.type;
	if (type >= tok_mass && type <= tok_mrand) {
		ret = new Operator(CurTok, CurTok.type);
		getNextToken();
	} else {
		if (type == tok_bind)
			prtParseError(CurTok, "invalid assign operator (please use ':=')");
		prtParseError(CurTok, "invalid assign operator");
	}
	return ret;
}

static Operator *eatUpdateToken() {
	if (CurTok.type == tok_mass)
		prtWarning(CurTok, "risky to use ':=' to update non-local field");
	return eatAssignToken();
}

static int pre[128]; // precedence table
static void initPrecedence() {
	pre[tok_ques ] = 2;
	pre[tok_colon] = 3;
	pre[tok_or ] = 4;
	pre[tok_and] = 5;
	pre[tok_xor] = 6;
	pre[tok_bita]= 7;
	pre[tok_eq ] = 8;
	pre[tok_ne ] = 8;
	pre[tok_le ] = 9;
	pre[tok_lt ] = 9;
	pre[tok_ge ] = 9;
	pre[tok_gt ] = 9;
	pre[tok_shl] = 10;
	pre[tok_shr] = 10;
	pre[tok_add] = 11;
	pre[tok_sub] = 11;
	pre[tok_mul] = 12;
	pre[tok_div] = 12;
	pre[tok_not] = 13;
	pre[tok_bitn]= 13;
	// these are not binary operator
	pre[0] = 0; // empty
	pre[tok_comma] = 1;
	pre[tok_dot  ] = 14; // .size
	pre[tok_int  ] = 13; // (int) a == b
	pre[tok_float] = 13;
}

static bool isBinOp(int type) {
	return (type >= tok_add && type <= tok_colon)
		&& (type != tok_not && type != tok_bitn);
}

// pre-definitions
static StmtList *parseStmts();
static ExprAST *parseExpr();
static VCProg *parseVCProgram();
static Type *parseType();

// FieldExpr :: field [ expr ]
static FieldExpr *parseFieldExpr() {
	Pos pos = CurTok;
	FieldName *fn = eatFieldToken();
	eatToken(tok_lbracket);
	ExprAST *expr = parseExpr();
	eatToken(tok_rbracket);
	return new FieldExpr(pos, fn, expr);
}

// ListExpr :: [ exp | name <- exp { , exp }* ]
//           | [ exp { , exp }* ] 
//           | [ ] -- empty list!
//           | [ exp .. exp ]  -- not supported yet
static ListAST *parseListExpr() {
	Pos pos = CurTok;
	eatToken(tok_lbracket);
	if (CurTok.type == tok_rbracket) {
		eatToken(tok_rbracket);
		return new ListExpr(pos);
	}
	ExprAST *expr = parseExpr();
	if (CurTok.type == tok_bar) {
		ExprList *exprs = new ExprList();
		eatToken(tok_bar);
		Identifier *name = eatNameToken();
		eatToken(tok_from);
		ExprAST *gen = parseExpr();
		while (CurTok.type == tok_comma) {
			eatToken(tok_comma);
			exprs->add(parseExpr());
		}
		eatToken(tok_rbracket);
		return new ListComp(*expr, expr, name, gen, exprs);
	} else {
		ListExpr *listE = new ListExpr(pos);
		listE->add(expr);
		while (CurTok.type == tok_comma) {
			eatToken(tok_comma);
			listE->add(parseExpr());
		}
		eatToken(tok_rbracket);
		return listE;
	}
}

// record :: { id = exp, w = exp }
//         | { id = exp }       -- old syntax
// record :: { exp, exp }
//         | { exp }            -- new syntax
static RecordExpr *parseRecord() {
	Pos pos = CurTok;
	ExprAST *id = nullptr, *w = nullptr;
	eatToken(tok_lbrace);
	id = parseExpr();
	if (CurTok.type == tok_bind) {
		// { id = .. } is acceptable
		if (id->etype() != EType::e_name)
			prtParseError(*id, "require record field name 'id'");
		Identifier *fn = (Identifier *) id;
		if (fn->getName() != ref_)
			prtParseError(*id, "require record field name 'id'");
		eatToken(tok_bind);
		id = parseExpr();
		if (CurTok.type == tok_comma) {
			eatToken(tok_comma);
			fn = eatNameToken();
			if (fn->getName() != val_)
				prtParseError(*id, "require record field name 'w'");
			eatToken(tok_bind);
			w = parseExpr();
		}
	} else {
		// { exp .. } is acceptable
		if (CurTok.type == tok_comma) {
			eatToken(tok_comma);
			w = parseExpr();
		}
	}
	eatToken(tok_rbrace);
	return new RecordExpr(pos, id, w);
}

static Operator *parseBuildIns() {
	Pos pos = CurTok;
	int op = !strcmp(CurTok.val, "maximum") ? tok_mmax :
			(!strcmp(CurTok.val, "minimum") ? tok_mmin :
			(!strcmp(CurTok.val, "sum") ? tok_madd :
			(!strcmp(CurTok.val, "random") ? tok_mrand :
			(!strcmp(CurTok.val, "and") ? tok_mand :
			(!strcmp(CurTok.val, "or") ? tok_mor :
			(!strcmp(CurTok.val, "xor") ? tok_mxor :
			(!strcmp(CurTok.val, "arbitrary") ? tok_marbi :-1)))))));
	eatToken(tok_buildin);
	return new Operator(pos, op);
}

// BuildIns :: max/min/sum/random list_exp
static ExprAST *parseAccumExpr() {
	Pos pos = CurTok;
	Operator *op = parseBuildIns();
	ListAST *expr = parseListExpr();
	return new AccumExpr(pos, op, expr);
}

static FuncExpr *parseFuncCall(Identifier *name) {
	Pos pos = *name;
	if (!getTypeEnv()->existsFunc(name)) {
		sprintf(err, "undefined external function: %s",
				name->getName().c_str());
		prtError(pos, err);
	}	
	eatToken(tok_lparen);
	ExprList *exprs = new ExprList();
	if (CurTok.type != tok_rparen) {
		exprs->add(parseExpr());
		while (CurTok.type == tok_comma) {
			eatToken(tok_comma);
			exprs->add(parseExpr());
		}
	}
	eatToken(tok_rparen);
	return new FuncExpr(pos, name, exprs);
}

// Primary :: name | int | float | bool
//          | field | 'inf' | ifexp
//          | -exp | !exp | '(' exp ')'
//          | ...
static ExprAST *parsePrimary() {
	Pos pos = CurTok;
	ExprAST *ret = nullptr;
	FieldName *fn = nullptr;
	Identifier *name = nullptr, *rf = nullptr;
	Operator *op = nullptr;
	switch (CurTok.type) {
	case tok_int:
		ret = new IntExpr(CurTok);
		getNextToken();
		break;
	case tok_float:
		ret = new FloatExpr(CurTok);
		getNextToken();
		break;
	case tok_bool:
		ret = new BoolExpr(CurTok);
		getNextToken();
		break;
	case tok_name: // function or name or e.W
		name = eatNameToken();
		switch (CurTok.type) {
		// CallExp :: name ( { exp { , exp }* }? )
		case tok_lparen:
			ret = parseFuncCall(name);
			break;
		/* error informations */
		// func [ .. ] (wrong function name)
		case tok_lbracket:
			prtParseError(pos, "invalid function name");
			break;
		default:
			ret = name;
		}
		break;
	case tok_id:
		pos = CurTok;
		eatToken(tok_id);
		eatToken(tok_lbracket);
		ret = parseExpr();
		eatToken(tok_rbracket);
		ret = new VidExpr(pos, ret);
		break;
	case tok_fname:
		ret = parseFieldExpr();
		break;
	case tok_inf:
		eatToken(tok_inf);
		ret = new InfiniteExpr(pos);
		break;
	case tok_lparen: // (exp) or pair
		eatToken(tok_lparen);
		ret = parseExpr();
		if (CurTok.type == tok_comma) {
			eatToken(tok_comma);
			ret = new PairExpr(pos, ret, parseExpr());
		}
		eatToken(tok_rparen);
		break;
	case tok_lbrace:
		ret = parseRecord();
		break;
	case tok_sub:
		eatToken(tok_sub);
		ret = new NegativeExpr(pos, parsePrimary());
		break;
	case tok_not:
		eatToken(tok_not);
		ret = new NotExpr(pos, parsePrimary());
		break;
	case tok_fst:
	case tok_snd:
		op = new Operator(pos, CurTok.type);
		getNextToken();
		ret = new SelectExpr(pos, op, parsePrimary());
		break;
	case tok_toi:
		eatToken(tok_toi);
		ret = new ToiExpr(pos, parsePrimary());
		break;
	case tok_tof:
		eatToken(tok_tof);
		ret = new TofExpr(pos, parsePrimary());
		break;
	case tok_true:
	case tok_false:
		ret = new BoolExpr(CurTok);
		getNextToken();
		break;
	case tok_bar:
		eatToken(tok_bar);
		eatToken(tok_V);
		eatToken(tok_bar);
		ret = new GraphSize(pos);
		break;
	default:
		prtError(pos, "unrecognized expression");
	}
	if (CurTok.type == tok_dot) {
		eatToken(tok_dot);
		pos = CurTok;
		rf = eatNameToken();
		if (rf->equals(ref_))
			ret = new EdgeExpr(pos, new Operator(pos, tok_fst), name);
		else if (rf->equals(val_))
			ret = new EdgeExpr(pos, new Operator(pos, tok_snd), name);
		else if (rf->equals("size")) {
			eatToken(tok_lparen);
			eatToken(tok_rparen);
			ret = new SizeExpr(pos, ret);
		} else if (rf->equals("contains")) {
			eatToken(tok_lparen);
			ret = new ContainsExpr(pos, ret, parseExpr());
			eatToken(tok_rparen);
		} else
			prtError(pos, "unknown record field name");
	}
	return ret;
}

static void reduce(stack<ExprAST*> &exps, stack<Operator*> &ops) {
	Operator *op = ops.top();
	if (op->type == tok_colon) {
		if (ops.size() == 1)
			prtParseError(*op, "cannot match ':' with some '?'");
		ops.pop();
		if (ops.top()->type != tok_ques)
			prtParseError(*op, "cannot match ':' with some '?'");
		ops.pop();
		ExprAST *e2 = exps.top();
		exps.pop();
		ExprAST *e1 = exps.top();
		exps.pop();
		ExprAST *cond = exps.top();
		exps.pop();
		exps.push(new IfExpr(*cond, cond, e1, e2));
	} else {
		ops.pop();
		ExprAST *e2 = exps.top();
		exps.pop();
		ExprAST *e1 = exps.top();
		exps.pop();
		exps.push(new BinaryExpr(*op, op, e1, e2));
	}
}
// Expr :: primary { op primary }*
// operations are "+" "-" "*" "/" "%" etc.
static ExprAST *parseExpr() {
	stack<ExprAST*> exps;
	stack<Operator*> ops;
	exps.push(parsePrimary());
	int type = CurTok.type;
	while (isBinOp(type)) {
		while (!ops.empty() && pre[type] <= pre[ops.top()->type])
			reduce(exps, ops); 
		ops.push(new Operator(CurTok, type));
		getNextToken();
		exps.push(parsePrimary());
		type = CurTok.type;
	}
	while (!ops.empty())
		reduce(exps, ops);
	return exps.top();
}

// aggExpr :: func [ expr | var <- V, exprs ]
static LetStmt *parseAggregator(Pos pos) {
	eatToken(tok_agg);
	Identifier *fn = eatNameToken();
	eatToken(tok_bind);
	Operator *op = parseBuildIns();
	eatToken(tok_lbracket);
	ExprAST *expr = parseExpr();
	eatToken(tok_bar);
	Identifier *name = eatNameToken();
	eatToken(tok_from);
	eatToken(tok_V);
	ExprList *exprs = new ExprList();
	while (CurTok.type == tok_comma) {
		eatToken(tok_comma);
		exprs->add(parseExpr());
	}
	eatToken(tok_rbracket);
	AggExpr *agg = new AggExpr(*op, op, expr, name, exprs);
	return new LetStmt(pos, fn, agg);
}

// topExpr :: list | accum | expr
ExprAST *parseTopLevelExpr() {
	if (CurTok.type == tok_lbracket)
		return new AccumExpr(CurTok, getOper(tok_mass), parseListExpr());
	if (CurTok.type == tok_buildin)
		return parseAccumExpr();
	return parseExpr();
}

// IfStmt :: 'if' (exp) < stmts > { 'else' < stmts > }? end?
static IfStmt *parseIfStmt() {
	Pos pos = CurTok;
	eatToken(tok_if);
	ExprAST *cond = parseExpr();
	eatToken(tok_lblock);
	StmtList *s1 = parseStmts(), *s2 = nullptr;
	eatToken(tok_rblock);
	if (CurTok.type == tok_else) {
		eatToken(tok_else);
		eatToken(tok_lblock);
		s2 = parseStmts();
		eatToken(tok_rblock);
	}
	if (CurTok.type == tok_end)
		eatToken(tok_end);
	return new IfStmt(pos, cond, s1, s2);
}

// ForStmt :: for (name <- exp { 'with' exp { , exp }* }?) < stmts > end?
static int level = 0;
static ForStmt *parseForStmt() {
	Pos pos = CurTok;
	ExprList *exprs = new ExprList();
	eatToken(tok_for);
	eatToken(tok_lparen);
	Identifier *name = eatNameToken();
	eatToken(tok_from);
	ExprAST *expr = parseExpr();
	if (CurTok.type == tok_bar) {
		eatToken(tok_bar);
		exprs->add(parseExpr());
		while (CurTok.type == tok_comma) {
			eatToken(tok_comma);
			exprs->add(parseExpr());
		}
	}
	eatToken(tok_rparen);
	eatToken(tok_lblock);
	level += 1;
	StmtList *stmts = parseStmts();
	level -= 1;
	eatToken(tok_rblock);
	if (CurTok.type == tok_end)
		eatToken(tok_end);
	return new ForStmt(pos, name, expr, stmts, exprs);
}

// LetStmt :: let var = exp
static StmtAST *parseLetStmt() {
	Pos pos = CurTok;
	eatToken(tok_let);
	if (CurTok.type == tok_agg)
		return parseAggregator(pos);
	if (CurTok.type == tok_ul) {
		eatToken(tok_ul);
		eatToken(tok_bind);
		Identifier *name = eatNameToken();
		return new ProcStmt(pos, parseFuncCall(name));
	}
	bool mut = false;
	if (CurTok.type == tok_mut) {
		eatToken(tok_mut);
		mut = true;
	}
	Identifier *name = eatNameToken();
	Type *type = nullptr;
	if (CurTok.type == tok_colon) {
		eatToken(tok_colon);
		type = parseType();
	}
	eatToken(tok_bind);
	ExprAST *expr = parseTopLevelExpr();
	return new LetStmt(pos, name, expr, mut, type);
}

static bool isModify(int type) {
	return (type >= tok_mass && type <= tok_mor);
}

// Remote :: 'remote' field '[' exp ']' op exp
static RemoteUStmt *parseRemoteUpdateStmt() {
	Pos pos = CurTok;
	eatToken(tok_remote);
	if (CurTok.type == tok_id)
		prtError(CurTok, "field 'Id' is immutable");
	FieldName *fn = eatFieldToken();
	eatToken(tok_lbracket);
	ExprAST *dest = parseExpr();
	eatToken(tok_rbracket);
	Operator *op = eatUpdateToken();
	ExprAST *expr = parseTopLevelExpr();
	return new RemoteUStmt(pos, fn, dest, op, expr);
}

// Local :: 'local'? field '[' var ']' op exp
static LocalUStmt *parseLocalUpdateStmt() {
	Pos pos = CurTok;
	if (CurTok.type == tok_local)
		getNextToken();
	if (CurTok.type == tok_id)
		prtError(CurTok, "field 'Id' is immutable");
	FieldName *fn = eatFieldToken();
	eatToken(tok_lbracket);
	eatToken(tok_name);
	eatToken(tok_rbracket);
	Operator *op = eatAssignToken();
	ExprAST *expr = parseTopLevelExpr();
	return new LocalUStmt(pos, fn, op, expr);
}

// Mutate :: 'mut'? var op exp
static MutateStmt *parseMutateStmt() {
	Pos pos = CurTok;
	if (CurTok.type == tok_mut)
		getNextToken();
	Identifier *name = eatNameToken();
	if (CurTok.type == tok_lbracket)
		prtParseError(*name, "field name should start with a capital letter");
	Operator *op = eatAssignToken();
	ExprAST *expr = parseTopLevelExpr();
	return new MutateStmt(pos, name, op, expr);
}

// Stmts :: { if_stmt | for_stmt | let_stmt | for }*
static StmtList *parseStmts() {
	StmtList *ret = new StmtList();
	bool no_stmt = false;
	while (!no_stmt) {
		switch (CurTok.type) {
		case tok_if:
			ret->add(parseIfStmt());
			break;
		case tok_for:
			ret->add(parseForStmt());
			break;
		case tok_let:
			ret->add(parseLetStmt());
			break;
		case tok_local:
		case tok_fname:
		case tok_id:
			ret->add(parseLocalUpdateStmt());
			break;
		case tok_remote:
			ret->add(parseRemoteUpdateStmt());
			break;
		case tok_mut:
		case tok_name:
			ret->add(parseMutateStmt());
			break;
		default:
			no_stmt = true;
		}
	}
	return ret;
}

// ForVertex :: 'for' name 'in' 'V' < stmts > end?
//            | 'for' name 'where' exp < stmts > end?
static BasicVC *parseForVertex() {
	eatToken(tok_for);
	if (CurTok.type == tok_lparen)
		prtError(CurTok, "no longer support this syntax");
	Identifier *name = eatNameToken();
	ExprAST *cond = nullptr;
	if (CurTok.type == tok_where) {
		eatToken(tok_where);
		cond = parseExpr();
	} else {
		eatToken(tok_in);
		eatToken(tok_V);
	}
	eatToken(tok_lblock);
	StmtList *stmts = parseStmts();
	eatToken(tok_rblock);
	if (CurTok.type == tok_end)
		eatToken(tok_end);
	return new BasicVC(name, stmts, cond);
}

// Stop :: 'stop' name { 'where' expr }?
static StopVC *parseStop() {
	eatToken(tok_stop);
	Identifier *name = eatNameToken();
	if (CurTok.type == tok_where) {
		eatToken(tok_where);
		ExprAST *cond = parseExpr();
		return new StopVC(name, cond);
	} else
		return new StopVC(name, nullptr);
}

// FieldList :: field { ',' field }*
static FieldList *parseFieldList() {
	FieldList *ret = new FieldList();
	ret->add(eatFieldToken());
	while (CurTok.type == tok_comma) {
		eatToken(tok_comma);
		ret->add(eatFieldToken());
	}
	return ret;
}

// Repeat :: 'repeat' Int < Program > 'end'
static RepeatVC *parseRepeat() {
	eatToken(tok_repeat);
	Token tok = CurTok;
	eatToken(tok_int);
	eatToken(tok_lblock);
	VCProg *prog = parseVCProgram();
	eatToken(tok_rblock);
	eatToken(tok_end);
	return new RepeatVC(prog, new IntExpr(tok));
}

// Range :: 'iter' var 'in' 'range' ( Int, Int ) < Program > 'end'
static IterRangeVC *parseRangeIter() {
	eatToken(tok_iter);
	Identifier *name = eatNameToken();
	eatToken(tok_in);
	eatToken(tok_range);
	eatToken(tok_lparen);
	if (CurTok.type != tok_int)
		eatToken(tok_int);
	IntExpr *lo = new IntExpr(CurTok);
	getNextToken();
	eatToken(tok_comma);
	if (CurTok.type != tok_int)
		eatToken(tok_int);
	IntExpr *hi = new IntExpr(CurTok);
	getNextToken();
	eatToken(tok_rparen);
	eatToken(tok_lblock);
	VCProg *prog = parseVCProgram();
	eatToken(tok_rblock);
	if (CurTok.type == tok_end)
		eatToken(tok_end);
	return new IterRangeVC(prog, name, lo, hi);
}

// DoUntil :: 'do' < Program > 'until' 'fix' [ fields ]
//          | 'do' < Program > 'until' 'exists' var. exp
//          | 'do' < Program > 'until' 'forall' var. exp
static IterVC *parseDoUntil() {
	eatToken(tok_do);
	eatToken(tok_lblock);
	VCProg *prog = parseVCProgram();
	eatToken(tok_rblock);
	eatToken(tok_until);
	IterVC *ret = nullptr;
	int ty = CurTok.type;
	if (ty == tok_fix) {
		eatToken(tok_fix);
		eatToken(tok_lbracket);
		ret = new FixPointVC(prog, parseFieldList());
		eatToken(tok_rbracket);
		return ret;
	}
	if (ty == tok_exists || ty == tok_forall) {
		getNextToken();
		Identifier *var = eatNameToken();
		eatToken(tok_dot);
		ExprAST *expr = parseExpr();
		if (ty == tok_forall) {
			if (expr->etype() == EType::e_not) {
				NotExpr *nt = (NotExpr*) expr;
				expr = nt->getExpr();
			} else
				expr = new NotExpr(*expr, expr);
		}
		StmtList *stmt = new StmtList();
		FieldName *fn = getIREnv()->getAM()->getAggFieldName();
		getTypeEnv()->setType(fn, getBoolType());
		stmt->add(new LocalUStmt(Pos(), fn, getOper(tok_mass), expr));
		prog = new ComposeVC(prog, new BasicVC(var, stmt));
		if (ty == tok_exists)
			return new ExistsVC(prog);
		else
			return new ForallVC(prog);
	}
	prtParseError(CurTok, "invalid termination condition");
	return nullptr;
}

static VCProg *parseVCProgram() {
	vector <VCProg*> progs;
	while (CurTok.type != tok_eof 
			&& CurTok.type != tok_rblock) {
		switch (CurTok.type) {
		case tok_for:
			progs.push_back(parseForVertex());
			break;
		case tok_stop:
			progs.push_back(parseStop());
			break;
		case tok_do:
			progs.push_back(parseDoUntil());
			break;
		case tok_repeat:
			progs.push_back(parseRepeat());
			break;
		case tok_iter:
			progs.push_back(parseRangeIter());
			break;
		default:
			prtParseError(CurTok, "no palgol program");
		}
	}
	if (progs.size() == 0)
		prtParseError(CurTok, "no palgol program");
	// convert array to list
	VCProg *ret = progs.back();
	for (int i = (int)progs.size() - 2; i >= 0; i--)
		ret = new ComposeVC(progs[i], ret);
	return ret;
}

static Type *parseType() {
	Type *t = nullptr, *q = nullptr;
	Identifier *n;
	Pos pos = CurTok;
	switch (CurTok.type) {
	case tok_lparen:
		eatToken(tok_lparen);
		if (CurTok.type == tok_rparen) {
			eatToken(tok_rparen);
			return getUnitType();
		}
		t = parseType();
		eatToken(tok_comma);
		q = parseType();
		eatToken(tok_rparen);
		return new PairType(t, q);
	case tok_lbracket:
		eatToken(tok_lbracket);
		t = parseType();
		eatToken(tok_rbracket);
		return new ListType(t);
	case tok_lbrace:
		eatToken(tok_lbrace);
		if (CurTok.type == tok_name && !strcmp(CurTok.val, ref_)) {
			// old syntax
			n = eatNameToken();
			eatToken(tok_colon);
			t = parseType();
			if (!t->equals(getVidType()))
				prtError(*n, "field 'id' should have type 'vid'");
			if (CurTok.type == tok_comma) {
				eatToken(tok_comma);
				n = eatNameToken();
				if (n->getName() != val_) {
					sprintf(err, "require 'id' but found invalid field name '%s'", CurTok.val);
					prtParseError(*n, err);
				}
				eatToken(tok_colon);
				q = parseType();
			} else
				q = getUnitType();
		} else {
			// new syntax
			t = parseType();
			if (CurTok.type == tok_comma) {
				eatToken(tok_comma);
				q = parseType();
			} else
				q = getUnitType();
		}
		eatToken(tok_rbrace);
		return new EdgeType(t, q);
	case tok_ul:
		eatToken(tok_ul);
		return getTypeEnv()->createTypeVar();
	case tok_name:
		n = eatNameToken();
		return getTypeEnv()->getTypeAlias(n);
	default:
		prtParseError(pos, "expect a type");
	}
	return nullptr;
}

// 'field' field : type
// 'extern' name : type -> type
static void parseTypeDefs() {
	while (CurTok.type == tok_field || CurTok.type == tok_extern
			|| CurTok.type == tok_type) {
		if (CurTok.type == tok_field) {
			eatToken(tok_field);
			FieldName *fn = eatFieldToken();
			eatToken(tok_colon);
			getTypeEnv()->setType(fn, parseType());
		} else
		if (CurTok.type == tok_extern) {
			vector<Type*> tl;
			eatToken(tok_extern);
			Identifier *name = eatNameToken();
			eatToken(tok_lparen);
			if (CurTok.type != tok_rparen) {
				tl.push_back(parseType());
				while (CurTok.type == tok_comma) {
					eatToken(tok_comma);
					Pos pos = CurTok;
					tl.push_back(parseType());
					if (tl.back()->equals(getUnitType()))
						prtParseError(pos, "function parameter should not has unit type");
				}
			}
			eatToken(tok_rparen);
			eatToken(tok_to);
			tl.push_back(parseType());
			getTypeEnv()->addExternal(name, tl);
			getCodeGen()->useExternal();
		} else
		if (CurTok.type == tok_type) {
			eatToken(tok_type);
			Identifier *name = eatNameToken();
			eatToken(tok_bind);
			Type *t = parseType();
			getTypeEnv()->addTypeAlias(name, t);
		}
	}
}

static void parseInputOutput() {
	CodeGen *cg = getCodeGen();
	if (CurTok.type == tok_inp) {
		eatToken(tok_inp);
		eatToken(tok_lbracket);
		FieldName *fn = eatFieldToken();
		cg->addInputField(fn);
		while (CurTok.type == tok_comma) {
			eatToken(tok_comma);
			FieldName *fn = eatFieldToken();
			cg->addInputField(fn);
		}
		eatToken(tok_rbracket);
	}
	if (CurTok.type == tok_oup) {
		eatToken(tok_oup);
		eatToken(tok_lbracket);
		FieldName *fn = eatFieldToken();
		cg->addOutputField(fn);
		while (CurTok.type == tok_comma) {
			eatToken(tok_comma);
			fn = eatFieldToken();
			cg->addOutputField(fn);
		}
		eatToken(tok_rbracket);
	}
}

VCProg *parse() {
	initTokenizer();
	initPrecedence();
	getNextToken();
	parseInputOutput();
	parseTypeDefs();
	return parseVCProgram();
}

int getPrec(int op) {
	return pre[op];
}
