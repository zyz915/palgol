#include "ast.h"
#include "codegen.h"
#include "config.h"
#include "errorinfo.h"
#include "types.h"

using namespace std;

static char buf[128];

bool Identifier::equals(const char *str) const {
	return !strcmp(name_.c_str(), str);
}

const string &Identifier::getName() const {
	return name_;
}

const string &FieldName::getName() const {
	return field_;
}

bool FieldName::equals(const char *str) const {
	return !strcmp(field_.c_str(), str);
}

bool FieldName::equals(const FieldName *other) const {
	return field_ == other->field_;
}

FieldExpr *FieldExpr::copyThis(ExprAST *expr) {
	FieldExpr *fe = new FieldExpr(Pos(), name_, expr);
	fe->type_ = type_;
	return fe;
}

std::string FieldExpr::getName() const {
	return name_->getName();
}

FieldName *FieldExpr::getFieldName() {
	return name_;
}

ExprAST *FieldExpr::getExpr() {
	return expr_;
}

FieldExpr *FieldExpr::replace(ExprAST *expr, Identifier *name) {
	if (expr_->equals(expr))
		return copyThis(name);
	if (expr_->etype() != EType::e_field)
		prtError(*this, "could not find a sub-expression");
	return copyThis(((FieldExpr*) expr_)->replace(expr, name));
}

void FieldList::add(FieldName *fn) {
	list_.push_back(fn);
}

bool FieldList::equals(const FieldList *other) const {
	if (list_.size() != other->list_.size())
		return false;
	for (int i = 0; i < list_.size(); i++)
		if (!list_[i]->equals(other->list_[i]))
			return false;
	return true;
}

int FieldList::size() const {
	return list_.size();
}

FieldName *FieldList::get(int index) {
	return list_[index];
}

void ListExpr::add(ExprAST *expr) {
	list_.push_back(expr);
}

int ListExpr::size() const {
	return list_.size();
}

ExprAST *ListExpr::get(int index) {
	return list_[index];
}

void ExprList::add(ExprAST *expr) {
	list_.push_back(expr);
}

int ExprList::size() const {
	return list_.size();
}

ExprAST *ExprList::get(int index) {
	return list_[index];
}

ExprAST *ExprList::operator[](int index) {
	return list_[index];
}

void StmtList::add(StmtAST *stmt) {
	list_.push_back(stmt);
}

int StmtList::size() const {
	return list_.size();
}

StmtAST *StmtList::get(int index) {
	return list_[index];
}

StmtAST *StmtList::operator[](int index) {
	return list_[index];
}

/* implement the interface in class AST:
 * 	 void print(FILE *f);
 */
void Identifier::print(FILE *f) const {
	fprintf(f, "(Name %s)", name_.c_str());
}

void FieldName::print(FILE *f) const {
	fprintf(f, "(Field %s)", field_.c_str());
}

void FieldList::print(FILE *f) const {
	fprintf(f, "(FieldList");
	for (int i = 0; i < list_.size(); i++) {
		fprintf(f, " ");
		list_[i]->print(f);
	}
	fprintf(f, ")");
}

void IntExpr::print(FILE *f) const {
	fprintf(f, "(Int %d)", val_);
}

void FloatExpr::print(FILE *f) const {
	fprintf(f, "(Float %.2f)", val_);
}

void BoolExpr::print(FILE *f) const {
	fprintf(f, "(Bool %s)", (val_ ? "true" : "false"));
}

void InfiniteExpr::print(FILE *f) const {
	fprintf(f, "\"inf\"");
}

void GraphSize::print(FILE *f) const {
	fprintf(f, "(GraphSize)");
}

void ExprList::print(FILE *f) const {
	fprintf(f, "(ExprList");
	for (int i = 0; i < list_.size(); i++) {
		fprintf(f, " ");
		list_[i]->print(f);
	}
	fprintf(f, ")");
}

void FieldExpr::print(FILE *f) const {
	fprintf(f, "(FieldExpr ");
	name_->print(f);
	fprintf(f, " ");
	expr_->print(f);
	fprintf(f, ")");
}

void ListComp::print(FILE *f) const {
	fprintf(f, "(ListComp ");
	expr_->print(f);
	fprintf(f, " ");
	name_->print(f);
	fprintf(f, " ");
	gen_->print(f);
	fprintf(f, " ");
	exprs_->print(f);
	fprintf(f, ")");
}

void PairExpr::print(FILE *f) const {
	fprintf(f, "(Pair ");
	fst_->print(f);
	fprintf(f, " ");
	snd_->print(f);
	fprintf(f, ")");
}

void RecordExpr::print(FILE *f) const {
	fprintf(f, "(Record ");
	id_->print(f);
	if (w_ != nullptr) {
		fprintf(f, " ");
		w_->print(f);
	}
	fprintf(f, ")");
}

void ListExpr::print(FILE *f) const {
	fprintf(f, "(List");
	for (int i = 0; i < list_.size(); i++) {
		fprintf(f, " ");
		list_[i]->print(f);
	}
	fprintf(f, ")");
}

void IfExpr::print(FILE *f) const {
	fprintf(f, "(IfExpr ");
	cond_->print(f);
	fprintf(f, " ");
	e1_->print(f);
	fprintf(f, " ");
	e2_->print(f);
	fprintf(f, ")");
}

static const char *getAccumStr(int op) {
	if (op == tok_mmax) return "maximum";
	if (op == tok_mmin) return "minimum";
	if (op == tok_madd) return "sum";
	if (op == tok_mand) return "and";
	if (op == tok_mor)  return "or";
	if (op == tok_mxor) return "xor";
	if (op == tok_mrand) return "random";
	if (op == tok_marbi) return "arbitrary";
	return ""; // assignment
}

void AggExpr::print(FILE *f) const {
	fprintf(f, "(AggExpr \"%s\" ", getAccumStr(op_->type));
	expr_->print(f);
	fprintf(f, " ");
	name_->print(f);
	fprintf(f, " ");
	exprs_->print(f);
	fprintf(f, ")");
}

void AccumExpr::print(FILE *f) const {
	fprintf(f, "(AccumExpr \"%s\" ", getAccumStr(op_->type));
	expr_->print(f);
	fprintf(f, ")");
}

void BinaryExpr::print(FILE *f) const {
	fprintf(f, "(BinaryExpr \"%s\" ", getTokenStr(op_->type));
	e1_->print(f);
	fprintf(f, " ");
	e2_->print(f);
	fprintf(f, ")");
}

void NegativeExpr::print(FILE *f) const {
	fprintf(f, "(NegativeExpr ");
	expr_->print(f);
	fprintf(f, ")");
}

void NotExpr::print(FILE *f) const {
	fprintf(f, "(NotExpr ");
	expr_->print(f);
	fprintf(f, ")");
}

void VidExpr::print(FILE *f) const {
	fprintf(f, "(VidExpr ");
	expr_->print(f);
	fprintf(f, ")");
}

void SelectExpr::print(FILE *f) const {
	fprintf(f, "(SelectExpr \"%s\" ",
		(op_->type == tok_fst ? "fst" : "snd"));
	expr_->print(f);
	fprintf(f, ")");
}

void SizeExpr::print(FILE *f) const {
	fprintf(f, "(SizeExpr ");
	expr_->print(f);
	fprintf(f, ")");
}

void ToiExpr::print(FILE *f) const {
	fprintf(f, "(ToInt ");
	expr_->print(f);
	fprintf(f, ")");
}

void TofExpr::print(FILE *f) const {
	fprintf(f, "(ToFloat ");
	expr_->print(f);
	fprintf(f, ")");
}

void EdgeExpr::print(FILE *f) const {
	fprintf(f, "(EdgeExpr \"%s\" ",
		(op_->type == tok_fst ? "id" : "w"));
	expr_->print(f);
	fprintf(f, ")");
}

void FuncExpr::print(FILE *f) const {
	fprintf(f, "(FuncExpr \"%s\"", name_->getName().c_str());
	for (int i = 0; i < exprs_->size(); i++) {
		fprintf(f, " ");
		exprs_->get(i)->print(f);
	}
	fprintf(f, ")");
}

void ContainsExpr::print(FILE *f) const {
	fprintf(f, "(Contains ");
	e1_->print(f);
	fprintf(f, " ");
	e2_->print(f);
	fprintf(f, ")");
}

void StmtList::print(FILE *f) const {
	fprintf(f, "(StmtList");
	for (int i = 0; i < list_.size(); i++) {
		fprintf(f, " ");
		list_[i]->print(f);
	}
	fprintf(f, ")");
}

void IfStmt::print(FILE *f) const {
	fprintf(f, "(IfStmt ");
	cond_->print(f);
	fprintf(f, " ");
	s1_->print(f);
	if (s2_ != nullptr) {
		fprintf(f, " ");
		s2_->print(f);
	}
	fprintf(f, ")");
}

void ForStmt::print(FILE *f) const {
	fprintf(f, "(ForStmt ");
	name_->print(f);
	fprintf(f, " ");
	expr_->print(f);
	fprintf(f, " ");
	stmts_->print(f);
	fprintf(f, " ");
	exprs_->print(f);
	fprintf(f, ")");
}

void LetStmt::print(FILE *f) const {
	fprintf(f, "(Let %s", mut_ ? "mut " : "");
	name_->print(f);
	fprintf(f, " ");
	expr_->print(f);
	fprintf(f, ")");
}

void ProcStmt::print(FILE *f) const {
	func_->print(f);
}

void LocalUStmt::print(FILE *f) const {
	fprintf(f, "(Local ");
	fn_->print(f);
	fprintf(f, " \"%s\" ", getTokenStr(op_->type));
	expr_->print(f);
	fprintf(f, ")");
}

void RemoteUStmt::print(FILE *f) const {
	fprintf(f, "(Remote ");
	fn_->print(f);
	fprintf(f, " ");
	dest_->print(f);
	fprintf(f, " \"%s\" ", getTokenStr(op_->type));
	expr_->print(f);
	fprintf(f, ")");
}

void MutateStmt::print(FILE *f) const {
	fprintf(f, "(Mutate ");
	name_->print(f);
	fprintf(f, " ");
	expr_->print(f);
	fprintf(f, ")");
}

void BasicVC::print(FILE *f) const {
	fprintf(f, "(BasicVC ");
	name_->print(f);
	fprintf(f, " ");
	if (cond_ != nullptr) {
		cond_->print(f);
		fprintf(f, " ");
	}
	stmts_->print(f);
	fprintf(f, ")");
}

void StopVC::print(FILE *f) const {
	fprintf(f, "(StopVC ");
	name_->print(f);
	fprintf(f, " ");
	if (cond_ != nullptr)
		cond_->print(f);
	else
		fprintf(f, "(Bool true)");
	fprintf(f, ")");
}

void ComposeVC::print(FILE *f) const {
	fprintf(f, "(ComposeVC ");
	head_->print(f);
	fprintf(f, " ");
	tail_->print(f);
	fprintf(f, ")");
}

void RepeatVC::print(FILE *f) const {
	fprintf(f, "(RepeatVC %d ", expr_->getVal());
	prog_->print(f);
	fprintf(f, ")");
}

void IterRangeVC::print(FILE *f) const {
	fprintf(f, "(IterRangeVC ");
	name_->print(f);
	fprintf(f, " %d %d ", lo_->getVal(), hi_->getVal());
	prog_->print(f);
	fprintf(f, ")");
}

void FixPointVC::print(FILE *f) const {
	fprintf(f, "(FixPointVC ");
	prog_->print(f);
	fprintf(f, " ");
	list_->print(f);
	fprintf(f, ")");
}

void ExistsVC::print(FILE *f) const {
	fprintf(f, "(ExistsVC ");
	prog_->print(f);
	fprintf(f, ")");
}

void ForallVC::print(FILE *f) const {
	fprintf(f, "(ForallVC ");
	prog_->print(f);
	fprintf(f, ")");
}

/* implement the interface in class ExprAST:
 *   bool contains(const Identifier *name) const;
 */
bool FieldExpr::contains(const Identifier *name) const {
	return expr_->contains(name);
}

bool ListComp::contains(const Identifier *name) const {
	if (gen_->contains(name))
		return true;
	for (int i = 0; i < exprs_->size(); i++)
		if (exprs_->get(i)->contains(name))
			return true;
	return expr_->contains(name);
}

bool AggExpr::contains(const Identifier *name) const {
	for (int i = 0; i < exprs_->size(); i++)
		if (exprs_->get(i)->contains(name))
			return true;
	return expr_->contains(name);
}

bool PairExpr::contains(const Identifier *name) const {
	return fst_->contains(name) || snd_->contains(name);
}

bool RecordExpr::contains(const Identifier *name) const {
	return id_->contains(name) || 
			(w_ != nullptr && w_->contains(name));
}

bool ListExpr::contains(const Identifier *name) const {
	for (int i = 0; i < list_.size(); i++)
		if (list_[i]->contains(name))
			return true;
	return false;
}

bool IfExpr::contains(const Identifier *name) const {
	return cond_->contains(name) || e1_->contains(name) || e2_->contains(name);
}

bool UnaryExpr::contains(const Identifier *name) const {
	return expr_->contains(name);
}

bool BinaryExpr::contains(const Identifier *name) const {
	return e1_->contains(name) || e2_->contains(name);
}

bool ContainsExpr::contains(const Identifier *name) const {
	return e1_->contains(name) || e2_->contains(name);
}

/* implement the interface in class ExprAST:
 *   bool equals(const ExprAST *other) const;
 */
bool Identifier::equals(const ExprAST *other) const {
	if (etype_ != other->etype()) return false;
	Identifier *it = (Identifier*) other;
	return name_ == it->name_;
}

bool IntExpr::equals(const ExprAST *other) const {
	if (etype_ != other->etype()) return false;
	IntExpr *it = (IntExpr*) other;
	return val_ == it->val_;
}

bool FloatExpr::equals(const ExprAST *other) const {
	if (etype_ != other->etype()) return false;
	FloatExpr *it = (FloatExpr*) other;
	return val_ == it->val_;
}

bool BoolExpr::equals(const ExprAST *other) const {
	if (etype_ != other->etype()) return false;
	BoolExpr *it = (BoolExpr*) other;
	return val_ == it->val_;
}

bool InfiniteExpr::equals(const ExprAST *other) const {
	return (etype_ == other->etype());
}

bool GraphSize::equals(const ExprAST *other) const {
	return (etype_ == other->etype());
}

bool FieldExpr::equals(const ExprAST *other) const {
	if (etype_ != other->etype()) return false;
	FieldExpr *it = (FieldExpr*) other;
	return (name_->equals(it->name_) &&
			expr_->equals(it->expr_));
}

bool ListComp::equals(const ExprAST *other) const {
	return false; // special case!
}

bool AggExpr::equals(const ExprAST *other) const {
	return false; // special case!
}

bool PairExpr::equals(const ExprAST *other) const {
	if (etype_ != other->etype()) return false;
	PairExpr *it = (PairExpr*) other;
	return (fst_->equals(it->fst_) &&
			snd_->equals(it->snd_));
}

bool RecordExpr::equals(const ExprAST *other) const {
	if (etype_ != other->etype()) return false;
	RecordExpr *it = (RecordExpr*) other;
	return (id_->equals(it->id_) &&
			w_->equals(it->w_));
}

bool ListExpr::equals(const ExprAST *other) const {
	if (etype_ != other->etype()) return false;
	ListExpr *it = (ListExpr*) other;
	if (list_.size() != it->list_.size())
		return false;
	for (unsigned i = 0; i < list_.size(); i++)
		if (!list_[i]->equals(it->list_[i]))
			return false;
	return true;
}

bool IfExpr::equals(const ExprAST *other) const {
	if (etype_ != other->etype()) return false;
	IfExpr *it = (IfExpr*) other;
	return (cond_->equals(it->cond_) &&
			e1_->equals(it->e1_) &&
			e2_->equals(it->e2_));
}

bool AccumExpr::equals(const ExprAST *other) const {
	if (etype_ != other->etype()) return false;
	AccumExpr *it = (AccumExpr*) other;
	return (op_->type == it->op_->type &&
			expr_->equals(it->expr_));
}

bool BinaryExpr::equals(const ExprAST *other) const {
	if (etype_ != other->etype()) return false;
	BinaryExpr *it = (BinaryExpr*) other;
	return (op_->type == it->op_->type &&
			e1_->equals(it->e1_) &&
			e2_->equals(it->e2_));
}

bool NegativeExpr::equals(const ExprAST *other) const {
	if (etype_ != other->etype()) return false;
	NegativeExpr *it = (NegativeExpr*) other;
	return (expr_->equals(it->expr_));
}

bool NotExpr::equals(const ExprAST *other) const {
	if (etype_ != other->etype()) return false;
	NotExpr *it = (NotExpr*) other;
	return (expr_->equals(it->expr_));
}

bool VidExpr::equals(const ExprAST *other) const {
	if (etype_ != other->etype()) return false;
	VidExpr *it = (VidExpr*) other;
	return (expr_->equals(it->expr_));
}

bool SelectExpr::equals(const ExprAST *other) const {
	if (etype_ != other->etype()) return false;
	SelectExpr *it = (SelectExpr*) other;
	return (op_->type == it->op_->type &&
			expr_->equals(it->expr_));
}

bool SizeExpr::equals(const ExprAST *other) const {
	if (etype_ != other->etype()) return false;
	SizeExpr *it = (SizeExpr*) other;
	return (expr_->equals(it->expr_));
}

bool ToiExpr::equals(const ExprAST *other) const {
	if (etype_ != other->etype()) return false;
	ToiExpr *it = (ToiExpr*) other;
	return (expr_->equals(it->expr_));
}

bool TofExpr::equals(const ExprAST *other) const {
	if (etype_ != other->etype()) return false;
	TofExpr *it = (TofExpr*) other;
	return (expr_->equals(it->expr_));
}

bool EdgeExpr::equals(const ExprAST *other) const {
	if (etype_ != other->etype()) return false;
	EdgeExpr *it = (EdgeExpr*) other;
	return (op_->type == it->op_->type &&
			expr_->equals(it->expr_));
}

bool FuncExpr::equals(const ExprAST *other) const {
	if (etype_ != other->etype()) return false;
	FuncExpr *it = (FuncExpr*) other;
	if (exprs_->size() != it->exprs_->size())
		return false;
	for (int i = 0; i < exprs_->size(); i++)
		if (!exprs_->get(i)->equals(it->exprs_->get(i)))
			return false;
	return true;
}

bool ContainsExpr::equals(const ExprAST *other) const {
	if (etype_ != other->etype()) return false;
	ContainsExpr *it = (ContainsExpr*) other;
	return (e1_->equals(it->e1_) && e2_->equals(it->e2_));
}

/* implement the interface in class ExprAST:
 *   Type *produceType(TypeEnv *env);
 */
Type *Identifier::produceType(TypeEnv *env) {
	if (!env->exists(this))
		prtErrorUV(*this, this->name_.c_str());
	type_ = env->getType(this);
	env->trace(this);
	return type_;
}

Type *IntExpr::produceType(TypeEnv *env) {
	return (type_ = getIntType());
}

Type *FloatExpr::produceType(TypeEnv *env) {
	return (type_ = getFloatType());
}

Type *BoolExpr::produceType(TypeEnv *env) {
	return (type_ = getBoolType());
}

Type *InfiniteExpr::produceType(TypeEnv *env) {
	type_ = env->createTypeVar();
	env->trace(this);
	return type_;
}

Type *GraphSize::produceType(TypeEnv *env) {
	return (type_ = getIntType());
}

Type *FieldExpr::produceType(TypeEnv *env) {
	Type *et = expr_->produceType(env);
	env->addConstraint(et, getVidType(), *this);
	env->trace(this);
	type_ = env->getType(name_);
	return (type_);
}

Type *ListComp::produceType(TypeEnv *env) {
	if (env->exists(name_))
		prtErrorRV(*name_, name_->getName().c_str());
	Type *gt = gen_->produceType(env);
	Type *nt = env->createTypeVar();
	env->pushScope();
	env->addBinding(name_, nt);
	env->addConstraint(gt, new ListType(nt), *name_);
	for (int i = 0; i < exprs_->size(); i++) {
		Type *t = exprs_->get(i)->produceType(env);
		env->addConstraint(t, getBoolType(), *exprs_->get(i));
	}
	Type *et = expr_->produceType(env);
	env->popScope();
	env->trace(this);
	return (type_ = new ListType(et));
}

Type *AggExpr::produceType(TypeEnv *env) {
	if (env->exists(name_))
		prtErrorRV(*name_, name_->getName().c_str());
	env->pushScope(false);
	env->addBinding(name_, getVidType());
	for (int i = 0; i < exprs_->size(); i++) {
		Type *t = exprs_->get(i)->produceType(env);
		env->addConstraint(t, getBoolType(), *exprs_->get(i));
	}
	type_ = expr_->produceType(env);
	env->popScope();
	env->trace(this);
	return type_;
}

Type *PairExpr::produceType(TypeEnv *env) {
	Type *ft = fst_->produceType(env);
	Type *st = snd_->produceType(env);
	env->trace(this);
	return (type_ = new PairType(ft, st));
}

Type *RecordExpr::produceType(TypeEnv *env) {
	Type *ft = id_->produceType(env);
	Type *st = getUnitType();
	if (w_ != nullptr) {
		st = w_->produceType(env);
		env->addEdgeWeight(st, *w_);
	}
	env->trace(this);
	return (type_ = new EdgeType(ft, st));
}

Type *ListExpr::produceType(TypeEnv *env) {
	Type *vt = env->createTypeVar();
	for (int i = 0; i < list_.size(); i++) {
		Type *et = list_[i]->produceType(env);
		env->addConstraint(vt, et, *this);
	}
	env->trace(this);
	return (type_ = new ListType(vt));
}

Type *IfExpr::produceType(TypeEnv *env) {
	Type *ct = cond_->produceType(env);
	env->addConstraint(ct, getBoolType(), *this);
	type_ = env->createTypeVar();
	env->addConstraint(type_, e1_->produceType(env), *this);
	env->addConstraint(type_, e2_->produceType(env), *this);
	env->trace(this);
	return type_;
}

Type *AccumExpr::produceType(TypeEnv *env) {
	Type *et = expr_->produceType(env);
	type_ = env->createTypeVar();
	int ty = op_->type;
	if (ty == tok_mass)
		env->addConstraint(type_, et, *this);
	else
		env->addConstraint(et, new ListType(type_), *this);
	if (ty == tok_mmax || ty == tok_mmin)
		env->addComparable(type_, *expr_);
	if (ty == tok_madd)
		env->addNumeric(type_, *expr_);
	if (ty == tok_mxor)
		env->addConstraint(type_, getIntType(), *expr_);
	if (ty == tok_mand || ty == tok_mor)
		env->addConstraint(type_, getBoolType(), *expr_);
	if (ty == tok_mrand) {
		Type *ft = env->createTypeVar();
		Type *st = env->createTypeVar();
		env->addConstraint(type_, new PairType(ft, st), *expr_);
		env->addNumeric(st, *expr_);
	}
	env->trace(this);
	return type_;
}

Type *BinaryExpr::produceType(TypeEnv *env) {
	Type *t1 = e1_->produceType(env);
	Type *t2 = e2_->produceType(env);
	env->addConstraint(t1, t2, *this);
	int ty = op_->type;
	if (ty >= tok_add && ty <= tok_div) {
		env->addNumeric(t1, *this);
		env->trace(this);
		return (type_ = t1);
	}
	if (ty >= tok_eq && ty <= tok_ge) {
		env->addComparable(t1, *this);
		return (type_ = getBoolType());
	}
	if (ty == tok_and || ty == tok_or)
		return (type_ = getBoolType());
	if (ty >= tok_xor && ty <= tok_shr)
		return (type_ = getIntType());
	prtError(*op_, "invalid binary operator");
	return (type_ = getIntType());
}

Type *NegativeExpr::produceType(TypeEnv *env) {
	type_ = expr_->produceType(env);
	env->addNumeric(type_, *expr_);
	env->trace(this);
	return type_;
}

Type *NotExpr::produceType(TypeEnv *env) {
	Type *et = expr_->produceType(env);
	env->addConstraint(et, getBoolType(), *this);
	return (type_ = getBoolType());
}

Type *VidExpr::produceType(TypeEnv *env) {
	Type *et = expr_->produceType(env);
	env->addConstraint(et, getVidType(), *this);
	return (type_ = getIntType());
}

Type *SelectExpr::produceType(TypeEnv *env) {
	Type *et = expr_->produceType(env);
	Type *ft = env->createTypeVar();
	Type *st = env->createTypeVar();
	env->addConstraint(et, new PairType(ft, st), *this);
	int ty = op_->type;
	env->trace(this);
	return (type_ = (ty == tok_fst ? ft : st));
}

Type *SizeExpr::produceType(TypeEnv *env) {
	Type *et = expr_->produceType(env);
	env->addConstraint(et, new ListType(env->createTypeVar()), *this);
	return (type_ = getIntType());
}

Type *ToiExpr::produceType(TypeEnv *env) {
	Type *et = expr_->produceType(env);
	env->addConstraint(et, getFloatType(), *this);
	return (type_ = getIntType());
}

Type *TofExpr::produceType(TypeEnv *env) {
	Type *et = expr_->produceType(env);
	env->addConstraint(et, getIntType(), *this);
	return (type_ = getFloatType());
}

Type *EdgeExpr::produceType(TypeEnv *env) {
	Type *et = expr_->produceType(env);
	Type *ft = env->createTypeVar();
	Type *st = env->createTypeVar();
	env->addEdgeWeight(st, *expr_);
	env->addConstraint(et, new EdgeType(ft, st), *this);
	int ty = op_->type;
	env->trace(this);
	return (type_ = (ty == tok_fst ? ft : st));
}

Type *FuncExpr::produceType(TypeEnv *env) {
	vector<Type*> args = env->getFuncType(name_);
	if (args.size() - 1 != exprs_->size())
		prtError(*this, "wrong number of parameters");
	for (int i = 0; i < exprs_->size(); i++) {
		Type *et = exprs_->get(i)->produceType(env);
		env->addConstraint(args[i], et, *exprs_->get(i));
	}
	return (type_ = args.back());
}

Type *ContainsExpr::produceType(TypeEnv *env) {
	Type *t1 = e1_->produceType(env);
	Type *t2 = e2_->produceType(env);
	env->addConstraint(t1, new ListType(t2), *this);
	return getBoolType();
}

/* implement the interface in class VCProg, StmtAST:
 *   void collectConstraints(TypeEnv *env);
 */
void StmtList::collectConstraints(TypeEnv *env) {
	for (auto stmt : list_)
		stmt->collectConstraints(env);
}

void IfStmt::collectConstraints(TypeEnv *env) {
	Type *ct = cond_->produceType(env);
	env->addConstraint(ct, getBoolType(), *this);
	env->pushScope();
	s1_->collectConstraints(env);
	env->popScope();
	if (s2_ != nullptr) {
		env->pushScope();
		s2_->collectConstraints(env);
		env->popScope();
	}
}

void ForStmt::collectConstraints(TypeEnv *env) {
	Type *et = expr_->produceType(env);
	Type *nt = env->createTypeVar();
	env->pushScope();
	env->addBinding(name_, nt);
	env->addConstraint(et, new ListType(nt), *this);
	for (int i = 0; i < exprs_->size(); i++) {
		Type *t = exprs_->get(i)->produceType(env);
		env->addConstraint(t, getBoolType(), *this);
	}
	stmts_->collectConstraints(env);
	env->popScope();
}

void LetStmt::collectConstraints(TypeEnv *env) {
	if (env->exists(name_))
		prtErrorRV(*name_, name_->getName().c_str());
	Type *et = expr_->produceType(env);
	// check for type conversion?
	Type *nt = (type_ == nullptr ? env->createTypeVar() : type_);
	env->addBinding(name_, nt, mut_);
	env->addConstraint(nt, et, *name_);
}

void ProcStmt::collectConstraints(TypeEnv *env) {
	Type *et = func_->produceType(env);
	env->addConstraint(et, getUnitType(), *this);
}

void LocalUStmt::collectConstraints(TypeEnv *env) {
	Type *et = expr_->produceType(env);
	Type *ft = env->getType(fn_);
	int ty = op_->type;
	if (ty == tok_mrem || ty == tok_mapp) {
		env->addConstraint(ft, new ListType(et), *this);
	} else {
		env->addConstraint(ft, et, *this);
		if (ty == tok_mmax || ty == tok_mmin)
			env->addComparable(et, *expr_);
		if (ty >= tok_madd && ty <= tok_mdiv)
			env->addNumeric(et, *expr_);
		if (ty >= tok_mand && ty <= tok_mor)
			env->addConstraint(ft, getBoolType(), *this);
		if (ty == tok_xor)
			env->addConstraint(ft, getIntType(), *this);
	}
}

void RemoteUStmt::collectConstraints(TypeEnv *env) {
	Type *et = expr_->produceType(env);
	Type *ft = env->getType(fn_);
	Type *dt = dest_->produceType(env);
	int ty = op_->type;
	env->addConstraint(dt, getVidType(), *this);
	if (ty == tok_mrem || ty == tok_mapp) {
		env->addConstraint(ft, new ListType(et), *this);
	} else {
		env->addConstraint(ft, et, *this);
		if (ty == tok_mmax || ty == tok_mmin)
			env->addComparable(et, *expr_);
		if (ty >= tok_madd && ty <= tok_mdiv)
			env->addNumeric(et, *expr_);
		if (ty >= tok_mand && ty <= tok_mor)
			env->addConstraint(ft, getBoolType(), *this);
		if (ty == tok_xor)
			env->addConstraint(ft, getIntType(), *this);
	}
}

void MutateStmt::collectConstraints(TypeEnv *env) {
	if (!env->exists(name_))
		prtErrorUV(*name_, name_->getName().c_str());
	if (!env->isMutable(name_))
		prtErrorIM(*name_, name_->getName().c_str());
	Type *et = expr_->produceType(env);
	Type *vt = env->getType(name_);
	int ty = op_->type;
	if (ty == tok_mrem || ty == tok_mapp) {
		env->addConstraint(vt, new ListType(et), *this);
	} else {
		env->addConstraint(vt, et, *this);
		if (ty == tok_mmax || ty == tok_mmin)
			env->addComparable(et, *expr_);
		if (ty >= tok_madd && ty <= tok_mdiv)
			env->addNumeric(et, *expr_);
		if (ty >= tok_mand && ty <= tok_mor)
			env->addConstraint(vt, getBoolType(), *this);
		if (ty == tok_xor)
			env->addConstraint(vt, getIntType(), *this);
	}
}

void BasicVC::collectConstraints(TypeEnv *env) {
	env->pushScope();
	env->addBinding(name_, getVidType());
	if (cond_ != nullptr)
		env->addConstraint(cond_->produceType(env),
				getBoolType(), *cond_);
	stmts_->collectConstraints(env);
	env->popScope();
}

void StopVC::collectConstraints(TypeEnv *env) {
	env->pushScope();
	env->addBinding(name_, getVidType());
	if (cond_ != nullptr) {
		Type *ct = cond_->produceType(env);
		env->addConstraint(ct, getBoolType(), *cond_);
	}
	env->popScope();
}

void ComposeVC::collectConstraints(TypeEnv *env) {
	head_->collectConstraints(env);
	tail_->collectConstraints(env);
}

void IterVC::collectConstraints(TypeEnv *env) {
	env->pushScope();
	prog_->collectConstraints(env);
	env->popScope();
}

void IterRangeVC::collectConstraints(TypeEnv *env) {
	env->pushScope();
	env->addBinding(name_, getIntType());
	prog_->collectConstraints(env);
	env->popScope();
}

/* Implement the interface for class ExprAST
 *   void initCheck(const StringSet &in);
 */
void FieldExpr::initCheck(const StringSet &in) {
	expr_->initCheck(in);
	if (in.find(name_->getName()) == in.end()) {
		if (name_->getName() == "In" ||
			name_->getName() == "Out") {
			getConfig()->genSymEdge = true;
		} else {
			// other list do not need to initialize
			if (getTypeEnv()->getType(name_)->type() !=
					PalgolTypes::t_list)
				prtInitError(*this, name_->getName().c_str());
		}
	}
}

void FieldExpr::conflict(const StringSet &in) {
	if (in.find(name_->getName()) != in.end())
		prtConfError(*this, name_->getName().c_str());
	if (expr_->etype() == EType::e_field)
		((FieldExpr*) expr_)->conflict(in);
	else
		expr_->confCheck(in);
}

void FieldExpr::confCheck(const StringSet &in) {
	if (expr_->etype() != EType::e_name)
		conflict(in);
}

void ListComp::initCheck(const StringSet &in) {
	//gen_->initCheck(in);
	for (int i = 0; i < exprs_->size(); i++)
		exprs_->get(i)->initCheck(in);
	expr_->initCheck(in);
}

void ListComp::confCheck(const StringSet &in) {
	//gen_->confCheck(in);
	for (int i = 0; i < exprs_->size(); i++)
		exprs_->get(i)->confCheck(in);
	expr_->confCheck(in);
}

void AggExpr::initCheck(const StringSet &in) {
	for (int i = 0; i < exprs_->size(); i++)
		exprs_->get(i)->initCheck(in);
	expr_->initCheck(in);
}

void AggExpr::confCheck(const StringSet &in) {
	for (int i = 0; i < exprs_->size(); i++)
		exprs_->get(i)->confCheck(in);
	expr_->confCheck(in);
}

void PairExpr::initCheck(const StringSet &in) {
	fst_->initCheck(in);
	snd_->initCheck(in);
}

void PairExpr::confCheck(const StringSet &in) {
	fst_->confCheck(in);
	snd_->confCheck(in);
}

void RecordExpr::initCheck(const StringSet &in) {
	id_->initCheck(in);
	if (w_ != nullptr)
		w_->initCheck(in);
}

void RecordExpr::confCheck(const StringSet &in) {
	id_->confCheck(in);
	if (w_ != nullptr)
		w_->confCheck(in);
}

void ListExpr::initCheck(const StringSet &in) {
	for (int i = 0; i < list_.size(); i++)
		list_[i]->initCheck(in);
}

void ListExpr::confCheck(const StringSet &in) {
	for (int i = 0; i < list_.size(); i++)
		list_[i]->confCheck(in);
}

void IfExpr::initCheck(const StringSet &in) {
	cond_->initCheck(in);
	e1_->initCheck(in);
	e2_->initCheck(in);
}

void IfExpr::confCheck(const StringSet &in) {
	cond_->confCheck(in);
	e1_->confCheck(in);
	e2_->confCheck(in);
}

void UnaryExpr::initCheck(const StringSet &in) {
	expr_->initCheck(in);
}

void UnaryExpr::confCheck(const StringSet &in) {
	expr_->confCheck(in);
}

void BinaryExpr::initCheck(const StringSet &in) {
	e1_->initCheck(in);
	e2_->initCheck(in);
}

void BinaryExpr::confCheck(const StringSet &in) {
	e1_->confCheck(in);
	e2_->confCheck(in);
}

void VidExpr::initCheck(const StringSet &in) {
	if (in.find(string("Id")) == in.end())
		prtError(*this, "field Id is not initialized");
}

void FuncExpr::initCheck(const StringSet &in) {
	for (int i = 0; i < exprs_->size(); i++)
		exprs_->get(i)->initCheck(in);
}

void FuncExpr::confCheck(const StringSet &in) {
	for (int i = 0; i < exprs_->size(); i++)
		exprs_->get(i)->confCheck(in);
}

void ContainsExpr::initCheck(const StringSet &in) {
	e1_->initCheck(in);
	e2_->initCheck(in);
}

void ContainsExpr::confCheck(const StringSet &in) {
	e1_->confCheck(in);
	e2_->confCheck(in);
}

/* Implement the interface for class StmtAST
 *   void initCheck(StringSet &in, vector<RUStruct*> &rus);
 */
void StmtList::initCheck(StringSet &in, vector<RUStruct*> &rus) {
	for (auto stmt : list_)
		stmt->initCheck(in, rus);
}

void StmtList::confCheck(StringSet &in) {
	for (auto stmt : list_)
		stmt->confCheck(in);
}

void IfStmt::initCheck(StringSet &in, vector<RUStruct*> &rus) {
	cond_->initCheck(in);
	if (s2_ == nullptr)
		s1_->initCheck(in, rus);
	else {
		StringSet i1(in), i2(in);
		s1_->initCheck(i1, rus);
		s2_->initCheck(i2, rus);
		for (auto &item : i1)
			if (i2.find(item) != i2.end())
				in.insert(item);
	}
}

void IfStmt::confCheck(StringSet &in) {
	cond_->confCheck(in);
	if (s2_ == nullptr)
		s1_->confCheck(in);
	else {
		StringSet i1(in), i2(in);
		s1_->confCheck(i1);
		s2_->confCheck(i2);
		for (auto &item : i1)
			in.insert(item);
		for (auto &item : i2)
			in.insert(item);
	}
}

void ForStmt::initCheck(StringSet &in, vector<RUStruct*> &rus) {
	//expr_->initCheck(in);
	for (int i = 0; i < exprs_->size(); i++)
		exprs_->get(i)->initCheck(in);
	stmts_->initCheck(in, rus);
}

void ForStmt::confCheck(StringSet &in) {
	//expr_->confCheck(in);
	for (int i = 0; i < exprs_->size(); i++)
		exprs_->get(i)->confCheck(in);
	stmts_->confCheck(in);
}

void LetStmt::initCheck(StringSet &in, vector<RUStruct*> &rus) {
	expr_->initCheck(in);
}

void LetStmt::confCheck(StringSet &in) {
	expr_->confCheck(in);
}

void ProcStmt::initCheck(StringSet &in, vector<RUStruct*> &rus) {
	func_->initCheck(in);
}

void ProcStmt::confCheck(StringSet &in) {
	func_->confCheck(in);
}

void LocalUStmt::initCheck(StringSet &in, vector<RUStruct*> &rus) {
	expr_->initCheck(in);
	if (op_->type != tok_mass && op_->type != tok_mapp)
		if (in.find(fn_->getName()) == in.end())
			prtInitError(*fn_, fn_->getName().c_str());
	in.insert(fn_->getName());
}

void LocalUStmt::confCheck(StringSet &in) {
	expr_->confCheck(in);
	in.insert(fn_->getName());
}

void RemoteUStmt::initCheck(StringSet &in, vector<RUStruct*> &rus) {
	expr_->initCheck(in);
	dest_->initCheck(in);
	rus.push_back(new RUStruct(0, op_->type, fn_, expr_->type()));
}

void RemoteUStmt::confCheck(StringSet &in) {
	expr_->confCheck(in);
	dest_->confCheck(in);
}

void MutateStmt::initCheck(StringSet &in, vector<RUStruct*> &rus) {
	expr_->initCheck(in);
}

void MutateStmt::confCheck(StringSet &in) {
	expr_->confCheck(in);
}

/* Implement the interface for class VCProg
 *   void initCheck(StringSet &in);
 */
void BasicVC::initCheck(StringSet &in) {
	vector<RUStruct*> rus;
	if (cond_ != nullptr)
		cond_->initCheck(in);
	stmts_->initCheck(in, rus);
	for (auto ru : rus)
		if (ru->op != tok_mass && ru->op != tok_mapp &&
				ru->type->type() != PalgolTypes::t_list &&
				in.find(ru->fn->getName()) == in.end()) {
			prtInitError(*(ru->fn), ru->fn->getName().c_str());
		}
	for (auto ru : rus) {
		in.insert(ru->fn->getName());
		delete ru;
	}
}

void StopVC::initCheck(StringSet &in) {
	if (cond_ != nullptr)
		cond_->initCheck(in);
}

void ComposeVC::initCheck(StringSet &in) {
	head_->initCheck(in);
	tail_->initCheck(in);
}

void IterVC::initCheck(StringSet &in) {
	prog_->initCheck(in);
}

void FixPointVC::initCheck(StringSet &in) {
	for (int i = 0; i < list_->size(); i++) {
		FieldName *fn = list_->get(i);
		if (in.find(fn->getName()) == in.end())
			prtInitError(*fn, fn->getName().c_str());
	}
	prog_->initCheck(in);
}

/* Implement the interface for class VCProg
 *   void confCheck();
 */
void BasicVC::confCheck() {
	StringSet in;
	stmts_->confCheck(in);
}

void ComposeVC::confCheck() {
	head_->confCheck();
	tail_->confCheck();
}

void IterVC::confCheck() {
	prog_->confCheck();
}

/* Implement the interface for class EvalStrategy
 *   EvalStrategy combine(const EvalStrategy &a, const EvalStrategy &b);
 */
EvalStrategy EvalStrategy::combine(const EvalStrategy &a, const EvalStrategy &b) {
	return EvalStrategy(
			a.loc_ok && b.loc_ok, a.loc_lp || b.loc_lp,
			a.rmt_ok && b.rmt_ok, a.rmt_lp || b.rmt_lp);
}

/* Implement the interface for class ExprAST, StmtAST
 *   void calcStrategy(IREnv *env, Identifier *name);
 */
void Identifier::calcStrategy(IREnv *env, Identifier *name) {
	EvalStrategy *loc = env->getExpr(this);
	if (loc != nullptr) {
		// copyFrom(*loc); -- wrong implementation?
		loc_ok = true;
		rmt_ok = loc_lp = rmt_lp = false;
	} else {
		loc_ok = true; // loop variable or locally defined variable
		loc_lp = equals(name); // contains(e) => equals(e)
		rmt_ok = equals(name) || equals(env->getCurVertex()); // e or u
		rmt_lp = equals(env->getCurVertex()); // u => x.id
	}
}

void ConstAST::calcStrategy(IREnv *env, Identifier *name) {
	loc_ok = rmt_ok = true;
	loc_lp = rmt_lp = false;
}

void FieldExpr::calcStrategy(IREnv *env, Identifier *name) {
	expr_->calcStrategy(env, name);
	if (expr_->equals(new EdgeExpr(Pos(), getOper(tok_fst), name))) {
		// F[e.id] is evaluated on neighbor
		loc_ok = loc_lp = rmt_lp = 0;
		rmt_ok = 1;
	} else
	if (expr_->equals(new EdgeExpr(Pos(), getOper(tok_snd), name))) {
		// F[e.w] is considered invalid
		prtError(*this, "cannot translate this expression");
	} else
	if (expr_->equals(env->getCurVertex())) {
		// F[u] is evaluated locally
		loc_ok = 1;
		loc_lp = rmt_ok = rmt_lp = 0;
	} else
	if (expr_->etype() == EType::e_field) {
		// only chain access is allowed F[G[..]]
		copyFrom(*expr_);
	} else {
		prtError(*this, "unable to translate such remote access pattern");
	}
}

void PairExpr::calcStrategy(IREnv *env, Identifier *name) {
	fst_->calcStrategy(env, name);
	snd_->calcStrategy(env, name);
	copyFrom(combine(*fst_, *snd_));
}

void RecordExpr::calcStrategy(IREnv *env, Identifier *name) {
	id_->calcStrategy(env, name);
	if (w_ == nullptr) {
		copyFrom(*id_);
	} else {
		w_->calcStrategy(env, name);
		copyFrom(combine(*id_, *w_));
	}
}

void IfExpr::calcStrategy(IREnv *env, Identifier *name) {
	cond_->calcStrategy(env, name);
	e1_->calcStrategy(env, name);
	e2_->calcStrategy(env, name);
	copyFrom(combine(*cond_, combine(*e1_, *e2_)));
}

void ExprList::calcStrategy(IREnv *env, Identifier *name) {
	loc_ok = rmt_ok = true;
	loc_lp = rmt_lp = false;
	for (auto expr : list_) {
		expr->calcStrategy(env, name);
		copyFrom(combine(*this, *expr));
	}
}

void ListExpr::calcStrategy(IREnv *env, Identifier *name) {
	loc_ok = rmt_ok = true;
	loc_lp = rmt_lp = false;
	for (auto expr : list_) {
		expr->calcStrategy(env, name);
		copyFrom(combine(*this, *expr));
	}
	if (list_.size() == 0)
		rmt_ok = false; // empty list always evaluates locally
}

void ListComp::calcStrategy(IREnv *env, Identifier *name) {
	gen_->calcStrategy(env, name);
	env->pushScope();
	exprs_->calcStrategy(env, name);
	expr_->calcStrategy(env, name);
	env->popScope();
	copyFrom(combine(combine(*gen_, *exprs_), *expr_));
}

void AggExpr::calcStrategy(IREnv *env, Identifier *name) {
	loc_ok = rmt_ok = true;
	loc_lp = rmt_lp = false;
}

void UnaryExpr::calcStrategy(IREnv *env, Identifier *name) {
	expr_->calcStrategy(env, name);
	copyFrom(*expr_);
}

void BinaryExpr::calcStrategy(IREnv *env, Identifier *name) {
	e1_->calcStrategy(env, name);
	e2_->calcStrategy(env, name);
	copyFrom(combine(*e1_, *e2_));
}

void EdgeExpr::calcStrategy(IREnv *env, Identifier *name) {
	expr_->calcStrategy(env, name);
	copyFrom(*expr_);
	// only e.w is special
	if (expr_->equals(name) && op_->type == tok_snd)
		rmt_lp = true;
}

void FuncExpr::calcStrategy(IREnv *env, Identifier *name) {
	exprs_->calcStrategy(env, name);
	copyFrom(*exprs_);
}

void ContainsExpr::calcStrategy(IREnv *env, Identifier *name) {
	e1_->calcStrategy(env, name);
	e2_->calcStrategy(env, name);
	copyFrom(combine(*e1_, *e2_));
}

void IfStmt::calcStrategy(IREnv *env, Identifier *name) {
	cond_->calcStrategy(env, name);
	env->pushScope();
	s1_->calcStrategy(env, name);
	env->popScope();
	copyFrom(combine(*cond_, *s1_));
	if (s2_ != nullptr){
		env->pushScope();
		s2_->calcStrategy(env, name);
		env->popScope();
		copyFrom(combine(*this, *s2_));
	}
}

void ForStmt::calcStrategy(IREnv *env, Identifier *name) {
	expr_->calcStrategy(env, name);
	env->pushScope();
	exprs_->calcStrategy(env, name);
	stmts_->calcStrategy(env, name);
	env->popScope();
	copyFrom(combine(combine(*expr_, *exprs_), *stmts_));
}

void LetStmt::calcStrategy(IREnv *env, Identifier *name) {
	expr_->calcStrategy(env, name);
	copyFrom(*expr_);
	env->addBinding(name_, expr_);
}

void ProcStmt::calcStrategy(IREnv *env, Identifier *name) {
	func_->calcStrategy(env, name);
	copyFrom(*func_);
}

void LocalUStmt::calcStrategy(IREnv *env, Identifier *name) {
	expr_->calcStrategy(env, name);
	copyFrom(*expr_);
}

void RemoteUStmt::calcStrategy(IREnv *env, Identifier *name) {
	dest_->calcStrategy(env, name);
	expr_->calcStrategy(env, name);
	copyFrom(combine(*dest_, *expr_));
}

void MutateStmt::calcStrategy(IREnv *env, Identifier *name) {
	expr_->calcStrategy(env, name);
	copyFrom(*expr_);
}

void StmtList::calcStrategy(IREnv *env, Identifier *name) {
	loc_ok = rmt_ok = true;
	loc_lp = rmt_lp = false;
	for (auto stmt : list_) {
		stmt->calcStrategy(env, name);
		copyFrom(combine(*this, *stmt));
	}
}

static bool isNbrList(FieldName *name) {
	const char *s = name->getName().c_str();
	return (!strcmp(s, "Nbr") || !strcmp(s, "In") || !strcmp(s, "Out"));
}

static FieldName *reverseEdge(FieldName *name) {
	if (name->getName() == "Nbr")
		return name;
	if (name->getName() == "In")
		return new FieldName("Out");
	if (name->getName() == "Out")
		return new FieldName("In");
	prtError(*name, "edge list should be 'In', 'Out' or 'Nbr'");
	return nullptr;
}

// get infinite value (spl: 1 -> maximum, -1 -> minimum)
// get default value for Bool (spl: 0 -> false, _ -> true)
static ExprIR *getNumIR(Type *type, int spl) {
	if (type->type() == PalgolTypes::t_int || 
		type->type() == PalgolTypes::t_vid)
		return new IntIR(0, spl);
	if (type->type() == PalgolTypes::t_float)
		return new FloatIR(0, spl);
	if (type->type() == PalgolTypes::t_bool)
		return new BoolIR(spl != 0);
	if (type->type() == PalgolTypes::t_pair ||
		type->type() == PalgolTypes::t_edge) {
		PairType *pt = (PairType *) type;
		if (pt->getSecond()->type() == PalgolTypes::t_unit)
			return getNumIR(pt->getFirst(), spl);
		return new PairIR(type, getNumIR(pt->getFirst(), spl),
				getNumIR(pt->getSecond(), spl));
	}
	fail("getNumIR() -- type error");
	return nullptr;
}

/* Implement the interface for class VCProg, ExprAST, StmtAST
 *   ExprIR *tr_loc(IREnv *env);
 *   ExprIR *tr_loc(IREnv *env, RemoteData *rd);
 */
ExprIR *Identifier::tr_loc(IREnv *env) {
	if (equals(env->getCurVertex()))
		return new IDIR();
	else
		return new VarIR(type_, rename(name_, env->getPID()));
}

ExprIR *Identifier::tr_nbr(IREnv *env, RemoteData *rd) {
	if (!equals(env->getCurVertex())) {
		if (rename(name_, env->getPID()) == rd->getName()) {
			// e -> u or (u, e.w)
			Type *t = getTypeEnv()->getEdgeWeight();
			if (t->type() == PalgolTypes::t_unit)
				return new IDIR();
			else
				return new PairIR(type_, new IDIR(), rd->getSnd());
		}
		fail(*this, "Identifier::tr_nbr()");
	}
	return rd->getFst();
}

ExprIR *Identifier::tr_try(IREnv *env, RemoteData *rd) {
	if (rmt_ok) return rd->add(tr_nbr(env, rd));
	else return tr_loc(env);
}

ExprIR *IntExpr::tr_loc(IREnv *env) {
	return new IntIR(val_);
}

ExprIR *FloatExpr::tr_loc(IREnv *env) {
	return new FloatIR(val_);
}

ExprIR *BoolExpr::tr_loc(IREnv *env) {
	return new BoolIR(val_);
}

ExprIR *InfiniteExpr::tr_loc(IREnv *env) {
	if (type_->type() == PalgolTypes::t_int ||
		type_->type() == PalgolTypes::t_vid)
		return new IntIR(0, 1);
	if (type_->type() == PalgolTypes::t_float)
		return new FloatIR(0, 1);
	fail("InfiniteExpr::tr_loc() -- type error");
	return nullptr;
}

ExprIR *GraphSize::tr_loc(IREnv *env) {
	return new GraphSizeIR();
}

ExprIR *ConstAST::tr_nbr(IREnv *env, RemoteData *rd) {
	return tr_loc(env);
}

ExprIR *ConstAST::tr_try(IREnv *env, RemoteData *rd) {
	return tr_loc(env); // no difference!
}

ExprIR *FieldExpr::tr_loc(IREnv *env) {
	// special treatment in translating aggregator
	if (env->isAggMode()) {
		if (!expr_->equals(env->getAggCurVertex()))
			prtError(*this, "cannot support such pattern in aggregator");
		return new AggFieldIR(type_, name_);
	}
	// local field (e.g. F[u])
	if (expr_->equals(env->getCurVertex())) {
 		FieldIR *ret = new FieldIR(type_, name_);
		env->useField(ret);
		return ret;
	}
	// request-respond pattern (e.g., F[D[u]])
	if (env->isReqResp(this) && getConfig()->hasReqResp()) {
		// TODO
	}
	// read cache from env
	VarIR *cache = env->find(this);
	if (cache != nullptr) return cache;
	// calculate
	ExprSet *subs = new ExprSet();
	env->getSubExprs(this, subs);
	int minDepth = 1000;
	ExprIR *result = nullptr;
	for (int i = 0; i < subs->size(); i++) {
		if (subs->get(i)->equals(env->getCurVertex())) continue;
		FieldExpr *cont = replace(subs->get(i), env->getCurVertex());
		ExprAST *dest = subs->get(i);
		ExprIR *c = cont->tr_loc(env);
		ExprIR *d = env->transformRemote(dest);
		int curDepth = max(c->depth(), d->depth()) + 1;
		if (curDepth < minDepth) {
			minDepth = curDepth;
			SendPatt *send = new SendPatt(c, d);
			result = env->bind(this, send);
		}
	}
	return result;
}

ExprIR *FieldExpr::tr_nbr(IREnv *env, RemoteData *rd) {
	// replace the innermost variable (e.g., F[e.ref] -> F[u])
	FieldExpr *fe = replace(new EdgeExpr(Pos(), getOper(tok_fst),
			rd->getCurLoopVar()), env->getCurVertex());
	return fe->tr_loc(env);
}

ExprIR *FieldExpr::tr_try(IREnv *env, RemoteData *rd) {
	if (rmt_ok) return rd->add(tr_nbr(env, rd));
	else return tr_loc(env);
}

ExprIR *ListComp::tr_loc(IREnv *env) {
	env->pushScope();
	env->incLoopLv();
	if (env->getLoopLv() == 1) {
		// gen_ is local for toplevel loop
		exprs_->calcStrategy(env, name_);
		expr_->calcStrategy(env, name_);
		copyFrom(combine(*expr_, *exprs_));
	}
	if (loc_ok) {
		LValue *lval = env->getLValue(type_);
		Operator *op = env->getOperator();
		ExprIR *expr = expr_->tr_loc(env);
		Conjunction *conj = new Conjunction();
		for (int i = 0; i < exprs_->size(); i++)
			conj->add(exprs_->get(i)->tr_loc(env));
		StmtIR *body = conj->produce(new AssignIR(lval, op, expr));
		env->emit(new ForStmtIR(rename(name_->getName(), env->getPID()),
				gen_->tr_loc(env), body));
		delete conj;
	} else {
		// TODO too loose
		if (gen_->etype() != EType::e_field)
			prtError(*expr_, "should be a field expression");
		FieldName *fn = ((FieldExpr*) gen_)->getFieldName();
		if (isNbrList(fn)) {
			// translate to neighborhood fetching
			FieldName *rev = reverseEdge(fn);
			env->useField(new FieldIR(gen_->type(), rev));
			NbrGetData *rd = new NbrGetData(env->newChannel(), name_, rev);
			LValue *lval = env->getLValue(type_);
			Operator *op = env->getOperator();
			ExprIR *expr = expr_->tr_try(env, rd);
			Conjunction *conj = new Conjunction();
			for (int i = 0; i < exprs_->size(); i++) {
				ExprAST *e = exprs_->get(i);
				if (e->rmt_ok)
					rd->addPredicate(e->tr_nbr(env, rd), e->rmt_lp);
				else if (e->loc_ok && !e->loc_lp)
					conj->add(e->tr_loc(env));
				else
					conj->add(e->tr_try(env, rd));
			}
			if (!expr_->rmt_ok) // remove combiner
				env->setCombiner(CombinerT::c_null);
			AssignIR *ass = new AssignIR(lval, op, expr);
			env->emit(new ScanStmtIR(rd->channel(), rd->getName(),
					rd->produce(env), conj->produce(ass)));
			delete conj;
		} else {
			// translate to request-response
			ReqResData *rd = new ReqResData(env->newChannel(), name_, fn);
			LValue *lval = env->getLValue(type_);
			Operator *op = env->getOperator();
			ExprIR *expr = expr_->tr_try(env, rd);
			Conjunction *conj = new Conjunction();
			for (int i = 0; i < exprs_->size(); i++) {
				ExprAST *e = exprs_->get(i);
				if (e->loc_ok)
					rd->addReqPredicate(e->tr_loc(env));
				else if (e->rmt_ok)
					rd->addResPredicate(e->tr_nbr(env, rd), e->rmt_lp);
				else
					conj->add(e->tr_try(env, rd));
			}
			AssignIR *ass = new AssignIR(lval, op, expr);
			env->emit(new ScanStmtIR(rd->channel(), rd->getName(),
					rd->produce(env), conj->produce(ass)));
			delete conj;
		}
	}
	env->decLoopLv();
	env->popScope();
	return nullptr;
}

ExprIR *ListComp::tr_nbr(IREnv *env, RemoteData *rd) {
	LValue *lval = env->getLValue(type_);
	Operator *op = env->getOperator();
	ExprIR *expr = expr_->tr_nbr(env, rd);
	Conjunction *conj = new Conjunction();
	for (int i = 0; i < exprs_->size(); i++)
		conj->add(exprs_->get(i)->tr_nbr(env, rd));
	StmtIR *body = conj->produce(new AssignIR(lval, op, expr));
	env->emit(new ForStmtIR(rename(name_->getName(), env->getPID()),
			gen_->tr_nbr(env, rd), body));
	return nullptr;
}

ExprIR *ListComp::tr_try(IREnv *env, RemoteData *rd) {
	if (rmt_ok) tr_nbr(env, rd);
	else if (loc_ok && !loc_lp) tr_loc(env);
	else {
		LValue *lval = env->getLValue(type_);
		Operator *op = env->getOperator();
		ExprIR *expr = expr_->tr_try(env, rd);
		Conjunction *conj = new Conjunction();
		for (int i = 0; i < exprs_->size(); i++)
			conj->add(exprs_->get(i)->tr_try(env, rd));
		StmtIR *body = conj->produce(new AssignIR(lval, op, expr));
		env->emit(new ForStmtIR(rename(name_->getName(), env->getPID()),
				gen_->tr_try(env, rd), body));
		delete conj;
	}
	return nullptr;
}

ExprIR *AggExpr::tr_loc(IREnv *env) {
	env->setAggCurVertex(name_);
	ExprIR *expr = expr_->tr_loc(env);
	Conjunction *conj = new Conjunction();
	for (int i = 0; i < exprs_->size(); i++)
		conj->add(exprs_->get(i)->tr_loc(env));
	env->exitAggMode();
	return env->registerAgg(type_, op_, conj, expr);
}

ExprIR *AggExpr::tr_nbr(IREnv *env, RemoteData *rd) {
	return tr_loc(env);
}

ExprIR *AggExpr::tr_try(IREnv *env, RemoteData *rd) {
	return tr_loc(env);
}

ExprIR *ListExpr::tr_loc(IREnv *env) {
	if (list_.size() == 0)
		return new EmptyListIR(type_);
	LValue *lval = env->getLValue(type_);
	for (auto expr : list_)
		env->emit(new AssignIR(lval, getOper(tok_mapp), expr->tr_loc(env)));
	return lval;
}

ExprIR *ListExpr::tr_nbr(IREnv *env, RemoteData *rd) {
	LValue *lval = env->getLValue(type_);
	for (auto expr : list_)
		env->emit(new AssignIR(lval, getOper(tok_mapp), expr->tr_nbr(env, rd)));
	return lval;
}

ExprIR *ListExpr::tr_try(IREnv *env, RemoteData *rd) {
	if (rmt_ok) return rd->add(tr_nbr(env, rd));
	if (loc_ok && !loc_lp) return tr_loc(env);
	LValue *lval = env->getLValue(type_);
	for (auto expr : list_)
		env->emit(new AssignIR(lval, getOper(tok_mapp), expr->tr_try(env, rd)));
	return lval;
}

ExprIR *PairExpr::tr_loc(IREnv *env) {
	ExprIR *fst = fst_->tr_loc(env);
	ExprIR *snd = snd_->tr_loc(env);
	return new PairIR(type_, fst, snd);
}

ExprIR *PairExpr::tr_nbr(IREnv *env, RemoteData *rd) {
	ExprIR *fst = fst_->tr_nbr(env, rd);
	ExprIR *snd = snd_->tr_nbr(env, rd);
	return new PairIR(type_, fst, snd);
}

ExprIR *PairExpr::tr_try(IREnv *env, RemoteData *rd) {
	if (loc_ok && !loc_lp) return tr_loc(env);
	ExprIR *fst = fst_->tr_try(env, rd);
	ExprIR *snd = snd_->tr_try(env, rd);
	return new PairIR(type_, fst, snd);
}

ExprIR *RecordExpr::tr_loc(IREnv *env) {
	ExprIR *id = id_->tr_loc(env);
	if (w_ == nullptr) return id;
	ExprIR *w = w_->tr_loc(env);
	return new PairIR(new PairType(
		id->type(), w->type()), id, w);
}

ExprIR *RecordExpr::tr_nbr(IREnv *env, RemoteData *rd) {
	ExprIR *id = id_->tr_nbr(env, rd);
	if (w_ == nullptr) return id;
	ExprIR *w = w_->tr_nbr(env, rd);
	return new PairIR(new PairType(
		id->type(), w->type()), id, w);
}

ExprIR *RecordExpr::tr_try(IREnv *env, RemoteData *rd) {
	if (loc_ok && !loc_lp) return tr_loc(env);
	ExprIR *id = id_->tr_try(env, rd);
	if (w_ == nullptr) return id;
	ExprIR *w = w_->tr_try(env, rd);
	return new PairIR(new PairType(
		id->type(), w->type()), id, w);
}

ExprIR *IfExpr::tr_loc(IREnv *env) {
	ExprIR *cond = cond_->tr_loc(env);
	ExprIR *e1 = e1_->tr_loc(env);
	ExprIR *e2 = e2_->tr_loc(env);
	return new IfExprIR(type_, cond, e1, e2);
}

ExprIR *IfExpr::tr_nbr(IREnv *env, RemoteData *rd) {
	ExprIR *cond = cond_->tr_nbr(env, rd);
	ExprIR *e1 = e1_->tr_nbr(env, rd);
	ExprIR *e2 = e2_->tr_nbr(env, rd);
	return new IfExprIR(type_, cond, e1, e2);
}

ExprIR *IfExpr::tr_try(IREnv *env, RemoteData *rd) {
	if (rmt_ok) return rd->add(tr_nbr(env, rd));
	if (loc_ok && !loc_lp) return tr_loc(env);
	ExprIR *cond = cond_->tr_try(env, rd);
	ExprIR *e1 = e1_->tr_try(env, rd);
	ExprIR *e2 = e2_->tr_try(env, rd);
	return new IfExprIR(type_, cond, e1, e2);
}

ExprIR *BinaryExpr::tr_loc(IREnv *env) {
	ExprIR *e1 = e1_->tr_loc(env);
	ExprIR *e2 = e2_->tr_loc(env);
	return new BinaryIR(type_, op_, e1, e2);
}

ExprIR *BinaryExpr::tr_nbr(IREnv *env, RemoteData *rd) {
	ExprIR *e1 = e1_->tr_nbr(env, rd);
	ExprIR *e2 = e2_->tr_nbr(env, rd);
	return new BinaryIR(type_, op_, e1, e2);
}

ExprIR *BinaryExpr::tr_try(IREnv *env, RemoteData *rd) {
	if (rmt_ok) return rd->add(tr_nbr(env, rd));
	if (loc_ok && !loc_lp) return tr_loc(env);
	ExprIR *e1 = e1_->tr_try(env, rd);
	ExprIR *e2 = e2_->tr_try(env, rd);
	return new BinaryIR(type_, op_, e1, e2);
}

ExprIR *AccumExpr::tr_loc(IREnv *env) {
	LValue *lval = env->getLValue(type_);
	Operator *op = env->getOperator();
	env->setCombiner(op2combiner(op_->type));
	env->pushStmts();
	if (op->type == tok_mass) {
		ExprIR *init = nullptr;
		if (op_->type == tok_mmax)
			init = getNumIR(type_, -1);
		if (op_->type == tok_mmin)
			init = getNumIR(type_, 1);
		if (op_->type == tok_madd)
			init = getNumIR(type_, 0);
		if (op_->type == tok_mand)
			init = new BoolIR(true);
		if (op_->type == tok_mor)
			init = new BoolIR(false);
		if (op_->type == tok_mxor)
			init = new IntIR(0, 0);
		if (op_->type == tok_mrand)
			init = getNumIR(type_, 0);
		if (op_->type == tok_mass)
			init = new EmptyListIR(type_);
		env->emit(new AssignIR(lval, getOper(tok_mass), init));
	}
	env->setLValue(lval);
	env->setOperator(op_->type == tok_mass ? getOper(tok_mapp) :
		(op->type == tok_mass ? op_ : op));
	expr_->tr_loc(env);
	env->setCombiner(CombinerT::c_null); // reset
	StmtListIR *stmts = env->popStmts();
	return new ExprStmtIR(lval, stmts);
}

ExprIR *AccumExpr::tr_nbr(IREnv *env, RemoteData *rd) {
	prtError(*this, "cannot translate this expression");
	return nullptr;
}

ExprIR *AccumExpr::tr_try(IREnv *env, RemoteData *rd) {
	prtError(*this, "cannot translate this expression");
	return nullptr;
}

ExprIR *NegativeExpr::tr_loc(IREnv *env) {
	return new NegativeIR(type_, expr_->tr_loc(env));
}

ExprIR *NegativeExpr::tr_nbr(IREnv *env, RemoteData *rd) {
	return new NegativeIR(type_, expr_->tr_nbr(env, rd));
}

ExprIR *NegativeExpr::tr_try(IREnv *env, RemoteData *rd) {
	if (rmt_ok) return rd->add(tr_nbr(env, rd));
	if (loc_ok && !loc_lp) return tr_loc(env);
	return new NegativeIR(type_, expr_->tr_try(env, rd));
}

ExprIR *NotExpr::tr_loc(IREnv *env) {
	return new NotIR(type_, expr_->tr_loc(env));
}

ExprIR *NotExpr::tr_nbr(IREnv *env, RemoteData *rd) {
	return new NotIR(type_, expr_->tr_nbr(env, rd));
}

ExprIR *NotExpr::tr_try(IREnv *env, RemoteData *rd) {
	if (rmt_ok) return rd->add(tr_nbr(env, rd));
	if (loc_ok && !loc_lp) return tr_loc(env);
	return new NotIR(type_, expr_->tr_try(env, rd));
}

ExprIR *VidExpr::tr_loc(IREnv *env) {
	return expr_->tr_loc(env);
}

ExprIR *VidExpr::tr_nbr(IREnv *env, RemoteData *rd) {
	return expr_->tr_nbr(env, rd);
}

ExprIR *VidExpr::tr_try(IREnv *env, RemoteData *rd) {
	if (rmt_ok) return rd->add(tr_nbr(env, rd));
	if (loc_ok && !loc_lp) return tr_loc(env);
	return expr_->tr_try(env, rd);
}

ExprIR *SelectExpr::tr_loc(IREnv *env) {
	return new SelectIR(type_, op_, expr_->tr_loc(env));
}

ExprIR *SelectExpr::tr_nbr(IREnv *env, RemoteData *rd) {
	return new SelectIR(type_, op_, expr_->tr_nbr(env, rd));
}

ExprIR *SelectExpr::tr_try(IREnv *env, RemoteData *rd) {
	if (rmt_ok) return rd->add(tr_nbr(env, rd));
	if (loc_ok && !loc_lp) return tr_loc(env);
	return new SelectIR(type_, op_, expr_->tr_try(env, rd));
}

ExprIR *SizeExpr::tr_loc(IREnv *env) {
	return new SizeIR(type_, expr_->tr_loc(env));
}

ExprIR *SizeExpr::tr_nbr(IREnv *env, RemoteData *rd) {
	return new SizeIR(type_, expr_->tr_nbr(env, rd));
}

ExprIR *SizeExpr::tr_try(IREnv *env, RemoteData *rd) {
	if (rmt_ok) return rd->add(tr_nbr(env, rd));
	if (loc_ok && !loc_lp) return tr_loc(env);
	return new SizeIR(type_, expr_->tr_try(env, rd));
}

ExprIR *ToiExpr::tr_loc(IREnv *env) {
	return new ToiIR(type_, expr_->tr_loc(env));
}

ExprIR *ToiExpr::tr_nbr(IREnv *env, RemoteData *rd) {
	return new ToiIR(type_, expr_->tr_nbr(env, rd));
}

ExprIR *ToiExpr::tr_try(IREnv *env, RemoteData *rd) {
	if (rmt_ok) return rd->add(tr_nbr(env, rd));
	if (loc_ok && !loc_lp) return tr_loc(env);
	return new ToiIR(type_, expr_->tr_try(env, rd));
}

ExprIR *TofExpr::tr_loc(IREnv *env) {
	return new TofIR(type_, expr_->tr_loc(env));
}

ExprIR *TofExpr::tr_nbr(IREnv *env, RemoteData *rd) {
	return new TofIR(type_, expr_->tr_nbr(env, rd));
}

ExprIR *TofExpr::tr_try(IREnv *env, RemoteData *rd) {
	if (rmt_ok) return rd->add(tr_nbr(env, rd));
	if (loc_ok && !loc_lp) return tr_loc(env);
	return new TofIR(type_, expr_->tr_try(env, rd));
}

ExprIR *EdgeExpr::tr_loc(IREnv *env) {
	return new SelectIR(type_, op_, expr_->tr_loc(env));
}

ExprIR *EdgeExpr::tr_nbr(IREnv *env, RemoteData *rd) {
	if (expr_->equals(rd->getCurLoopVar())) {
		if (op_->type == tok_fst) // e.id -> u
			return new IDIR();
		if (op_->type == tok_snd) // e.w -> x.w
			return rd->getSnd();
	}
	prtError(*this, "cannot translate this pattern");
	return new SelectIR(type_, op_, expr_->tr_nbr(env, rd));
}

ExprIR *EdgeExpr::tr_try(IREnv *env, RemoteData *rd) {
	if (!rmt_ok)
		prtError(*this, "cannot translate this pattern");
	return rd->add(tr_nbr(env, rd));
}

FunctionIR *FuncExpr::tr_loc(IREnv *env) {
	FunctionIR *ret = new FunctionIR(type_, name_->getName());
	for (int i = 0; i < exprs_->size(); i++)
		ret->add(exprs_->get(i)->tr_loc(env));
	return ret;
}

FunctionIR *FuncExpr::tr_nbr(IREnv *env, RemoteData *rd) {
	FunctionIR *ret = new FunctionIR(type_, name_->getName());
	for (int i = 0; i < exprs_->size(); i++)
		ret->add(exprs_->get(i)->tr_nbr(env, rd));
	return ret;
}

ExprIR *FuncExpr::tr_try(IREnv *env, RemoteData *rd) {
	if (rmt_ok) return rd->add(tr_nbr(env, rd));
	if (loc_ok && !loc_lp) return tr_loc(env);
	FunctionIR *ret = new FunctionIR(type_, name_->getName());
	for (int i = 0; i < exprs_->size(); i++)
		ret->add(exprs_->get(i)->tr_try(env, rd));
	return ret;
}

ExprIR *ContainsExpr::tr_loc(IREnv *env) {
	FunctionIR *ret = new FunctionIR(type_, string("contains"));
	ret->add(e1_->tr_loc(env));
	ret->add(e2_->tr_loc(env));
	return ret;
}

ExprIR *ContainsExpr::tr_nbr(IREnv *env, RemoteData *rd) {
	FunctionIR *ret = new FunctionIR(type_, string("contains"));
	ret->add(e1_->tr_nbr(env, rd));
	ret->add(e2_->tr_nbr(env, rd));
	return ret;
}

ExprIR *ContainsExpr::tr_try(IREnv *env, RemoteData *rd) {
	if (rmt_ok) return rd->add(tr_nbr(env, rd));
	if (loc_ok && !loc_lp) return tr_loc(env);
	FunctionIR *ret = new FunctionIR(type_, string("contains"));
	ret->add(e1_->tr_try(env, rd));
	ret->add(e2_->tr_try(env, rd));
	return ret;
}

void IfStmt::tr_loc(IREnv *env) {
	ExprIR *c = cond_->tr_loc(env);
	env->pushStmts();
	s1_->tr_loc(env);
	StmtListIR *s1, *s2 = nullptr;
	s1 = env->popStmts();
	if (s2_ != nullptr){
		env->pushStmts();
		s2_->tr_loc(env);
		s2 = env->popStmts();
	}
	env->emit(new IfStmtIR(c, s1, s2));
}

void IfStmt::tr_nbr(IREnv *env, RemoteData *rd) {
	ExprIR *c = rd->add(cond_->tr_nbr(env, rd));
	env->pushStmts();
	s1_->tr_nbr(env, rd);
	StmtListIR *s1, *s2 = nullptr;
	s1 = env->popStmts();
	if (s2_ != nullptr){
		env->pushStmts();
		s2_->tr_nbr(env, rd);
		s2 = env->popStmts();
	}
	env->emit(new IfStmtIR(c, s1, s2));
}

void IfStmt::tr_try(IREnv *env, RemoteData *rd) {
	if (rmt_ok) { tr_nbr(env, rd); return; }
	if (loc_ok && !loc_lp) { tr_loc(env); return; }
	ExprIR *c = cond_->tr_try(env, rd);
	env->pushStmts();
	s1_->tr_try(env, rd);
	StmtListIR *s1, *s2 = nullptr;
	s1 = env->popStmts();
	if (s2_ != nullptr){
		env->pushStmts();
		s2_->tr_try(env, rd);
		s2 = env->popStmts();
	}
	env->emit(new IfStmtIR(c, s1, s2));
}

void ForStmt::tr_loc(IREnv *env) {
	env->pushScope();
	env->incLoopLv();
	if (env->getLoopLv() == 1) {
		stmts_->calcStrategy(env, name_);
		exprs_->calcStrategy(env, name_);
		copyFrom(combine(*stmts_, *exprs_));
	}
	if (loc_ok) {
		Conjunction *conj = new Conjunction();
		for (int i = 0; i < exprs_->size(); i++)
			conj->add(exprs_->get(i)->tr_loc(env));
		env->pushStmts();
		stmts_->tr_loc(env);
		StmtListIR *stmts = env->popStmts();
		StmtIR *body = conj->produce(stmts);
		env->emit(new ForStmtIR(rename(name_->getName(),
				env->getPID()), expr_->tr_loc(env), body));
		delete conj;
	} else {
		// currently, "expr_" should be a list of neighbors
		if (expr_->etype() != EType::e_field)
			prtError(*expr_, "should be a neighboring edge list");
		FieldName *fn = ((FieldExpr*) expr_)->getFieldName();
		if (isNbrList(fn)) {
			// translate to neighborhood fetching
			FieldName *rev = reverseEdge(fn);
			env->useField(new FieldIR(expr_->type(), rev));
			NbrGetData *rd = new NbrGetData(env->newChannel(), name_, rev);
			Conjunction *conj = new Conjunction();
			for (int i = 0; i < exprs_->size(); i++) {
				ExprAST *e = exprs_->get(i);
				if (e->rmt_ok)
					rd->addPredicate(e->tr_nbr(env, rd), e->rmt_lp);
				else if (e->loc_ok && !e->loc_lp)
					conj->add(e->tr_loc(env));
				else
					conj->add(e->tr_try(env, rd));
			}
			env->pushStmts();
			stmts_->tr_try(env, rd);
			StmtListIR *stmts = env->popStmts();
			StmtIR *body = conj->produce(stmts);
			env->emit(new ScanStmtIR(rd->channel(), rd->getName(),
					rd->produce(env), body));
			delete conj;
		} else {
			// translate to request-response
			ReqResData *rd = new ReqResData(env->newChannel(), name_, fn);
			ExprIR *expr = expr_->tr_try(env, rd);
			Conjunction *conj = new Conjunction();
			for (int i = 0; i < exprs_->size(); i++) {
				ExprAST *e = exprs_->get(i);
				if (e->loc_ok)
					rd->addReqPredicate(e->tr_loc(env));
				else if (e->rmt_ok)
					rd->addResPredicate(e->tr_nbr(env, rd), e->rmt_lp);
				else
					conj->add(e->tr_try(env, rd));
			}
			env->pushStmts();
			stmts_->tr_try(env, rd);
			StmtListIR *stmts = env->popStmts();
			env->emit(new ScanStmtIR(rd->channel(), rd->getName(),
					rd->produce(env), conj->produce(stmts)));
			delete conj;
		}
	}
	env->decLoopLv();
	env->popScope();
}

void ForStmt::tr_nbr(IREnv *env, RemoteData *rd) {
	FieldName *fn = ((FieldExpr*) expr_)->getFieldName();
	ListType *ty = (ListType*) getTypeEnv()->getType(fn);
	VarIR *var = new VarIR(ty->getContent(), env->createVar());
	Conjunction *conj = new Conjunction();
	for (int i = 0; i < exprs_->size(); i++)
		conj->add(exprs_->get(i)->tr_nbr(env, rd));
	env->pushStmts();
	stmts_->tr_nbr(env, rd);
	StmtListIR *stmts = env->popStmts();
	StmtIR *body = conj->produce(stmts);
	env->emit(new ForStmtIR(rename(name_->getName(), env->getPID()),
			expr_->tr_nbr(env, rd), body));
	delete conj;
}

void ForStmt::tr_try(IREnv *env, RemoteData *rd) {
	if (rmt_ok) tr_nbr(env, rd);
	else if (loc_ok && !loc_lp) tr_loc(env);
	else {
		FieldName *fn = ((FieldExpr*) expr_)->getFieldName();
		ListType *ty = (ListType*) getTypeEnv()->getType(fn);
		VarIR *var = new VarIR(ty->getContent(), env->createVar());
		Conjunction *conj = new Conjunction();
		for (int i = 0; i < exprs_->size(); i++)
			conj->add(exprs_->get(i)->tr_try(env, rd));
		env->pushStmts();
		stmts_->tr_try(env, rd);
		StmtListIR *stmts = env->popStmts();
		StmtIR *body = conj->produce(stmts);
		env->emit(new ForStmtIR(rename(name_->getName(), env->getPID()),
				expr_->tr_try(env, rd), body));
		delete conj;
	}
}

void LetStmt::tr_loc(IREnv *env) {
	VarIR *lval = new VarIR(expr_->type(),
			rename(name_->getName(), env->getPID()));
	env->setLValue(lval);
	env->setOperator(getOper(tok_mass));
	env->emit(new AssignIR(lval, getOper(tok_mass), expr_->tr_loc(env)));
}

void LetStmt::tr_nbr(IREnv *env, RemoteData *rd) {
	VarIR *lval = new VarIR(expr_->type(),
			rename(name_->getName(), env->getPID()));
	env->setLValue(lval);
	env->setOperator(getOper(tok_mass));
	env->emit(new AssignIR(lval, getOper(tok_mass), expr_->tr_nbr(env, rd)));
}

void LetStmt::tr_try(IREnv *env, RemoteData *rd) {
	if (rmt_ok) tr_nbr(env, rd);
	else if (loc_ok && !loc_lp) tr_loc(env);
	VarIR *lval = new VarIR(expr_->type(), 
			rename(name_->getName(), env->getPID()));
	env->setLValue(lval);
	env->setOperator(getOper(tok_mass));
	env->emit(new AssignIR(lval, getOper(tok_mass), expr_->tr_try(env, rd)));
}

void ProcStmt::tr_loc(IREnv *env) {
	env->emit(new ProcedureIR(func_->tr_loc(env)));
}

void ProcStmt::tr_nbr(IREnv *env, RemoteData *rd) {
	env->emit(new ProcedureIR(func_->tr_nbr(env, rd)));
}

void ProcStmt::tr_try(IREnv *env, RemoteData *rd) {
	if (rmt_ok) tr_nbr(env, rd);
	else env->emit(new ProcedureIR((FunctionIR*) func_->tr_try(env, rd)));
}

void LocalUStmt::tr_loc(IREnv *env) {
	// TODO need more information to decide the efficient update
	// Now, suppose there's no write-read conflict
	Type *ty = getTypeEnv()->getType(fn_);
	FieldIR *fn = new FieldIR(ty, fn_);
	env->useField(fn);
	env->setLValue(fn);
	env->setOperator(op_);
	env->emit(new AssignIR(fn, op_, expr_->tr_loc(env)));
}

void LocalUStmt::tr_nbr(IREnv *env, RemoteData *rd) {
	Type *ty = getTypeEnv()->getType(fn_);
	FieldIR *fn = new FieldIR(ty, fn_);
	env->useField(fn);
	env->setLValue(fn);
	env->setOperator(op_);
	env->emit(new AssignIR(fn, op_, rd->add(expr_->tr_nbr(env, rd))));
}

void LocalUStmt::tr_try(IREnv *env, RemoteData *rd) {
	Type *ty = getTypeEnv()->getType(fn_);
	FieldIR *fn = new FieldIR(ty, fn_);
	env->useField(fn);
	env->setLValue(fn);
	env->setOperator(op_);
	env->emit(new AssignIR(fn, op_, expr_->tr_try(env, rd)));
}

void RemoteUStmt::tr_loc(IREnv *env) {
	Type *ty = getTypeEnv()->getType(fn_);
	FieldIR *fn = new FieldIR(ty, fn_);
	env->useField(fn);
	env->emit(new RemoteIR(fn_, dest_->tr_loc(env),
			op_, expr_->tr_loc(env)));
}

void RemoteUStmt::tr_nbr(IREnv *env, RemoteData *rd) {
	Type *ty = getTypeEnv()->getType(fn_);
	FieldIR *fn = new FieldIR(ty, fn_);
	env->useField(fn);
	ExprIR *dest = rd->add(dest_->tr_nbr(env, rd));
	ExprIR *expr = rd->add(expr_->tr_nbr(env, rd));
	env->emit(new RemoteIR(fn_, dest, op_, expr));
}

void RemoteUStmt::tr_try(IREnv *env, RemoteData *rd) {
	Type *ty = getTypeEnv()->getType(fn_);
	FieldIR *fn = new FieldIR(ty, fn_);
	env->useField(fn);
	ExprIR *dest = dest_->tr_try(env, rd);
	ExprIR *expr = expr_->tr_try(env, rd);
	env->emit(new RemoteIR(fn_, dest, op_, expr));
}

void MutateStmt::tr_loc(IREnv *env) {
	VarIR *lval = new VarIR(expr_->type(),
			rename(name_->getName(), env->getPID()));
	env->setLValue(lval);
	env->setOperator(getOper(tok_mass));
	env->emit(new AssignIR(lval, getOper(tok_mass), expr_->tr_loc(env)));
}

void MutateStmt::tr_nbr(IREnv *env, RemoteData *rd) {
	VarIR *lval = new VarIR(expr_->type(),
			rename(name_->getName(), env->getPID()));
	env->setLValue(lval);
	env->setOperator(getOper(tok_mass));
	env->emit(new AssignIR(lval, getOper(tok_mass), expr_->tr_nbr(env, rd)));
}

void MutateStmt::tr_try(IREnv *env, RemoteData *rd) {
	if (rmt_ok) tr_nbr(env, rd);
	else if (loc_ok && !loc_lp) tr_loc(env);
	VarIR *lval = new VarIR(expr_->type(),
			rename(name_->getName(), env->getPID()));
	env->setLValue(lval);
	env->setOperator(getOper(tok_mass));
	env->emit(new AssignIR(lval, getOper(tok_mass), expr_->tr_try(env, rd)));
}

void StmtList::tr_loc(IREnv *env) {
	for (auto stmt : list_)
		stmt->tr_loc(env);
}

void StmtList::tr_nbr(IREnv *env, RemoteData *rd) {
	for (auto stmt : list_)
		stmt->tr_nbr(env, rd);
}

void StmtList::tr_try(IREnv *env, RemoteData *rd) {
	for (auto stmt : list_)
		stmt->tr_try(env, rd);
}

SeqSTM *BasicVC::transform(IREnv *env) {
	env->pushScope();
	env->pushStmts();
	env->setCurVertex(name_);
	env->incPID();
	StmtAST *prog = stmts_;
	if (cond_ != nullptr)
		prog = new IfStmt(Pos(), cond_, stmts_, nullptr);
	prog->tr_loc(env);
	StmtListIR *stmts = env->popStmts();
	env->popScope();
	stmts->calcDepth(env);
	env->initSteps(stmts->depth());
	stmts->genSteps(env, stmts->depth());
	SeqSTM *ret = env->produceSteps();
	env->clear();
	return ret;
}

SeqSTM *StopVC::transform(IREnv *env) {
	env->pushScope();
	env->pushStmts();
	env->setCurVertex(name_);
	StmtListIR *vth = new StmtListIR();
	vth->add(new VoteToHalt());
	vth->add(new ReturnIR());
	StmtIR *stmt = vth; // when (cond_ == nullptr)
	int depth = 0;
	if (cond_ != nullptr) {
		ExprIR *cond = cond_->tr_loc(env);
		stmt = new IfStmtIR(cond, vth, nullptr);
		depth = cond->depth();
	}
	env->popScope();
	env->initSteps(depth);
	stmt->genSteps(env, depth);
	SeqSTM *ret = env->produceSteps(true);
	env->clear();
	return ret;
}

SeqSTM *ComposeVC::transform(IREnv *env) {
	Config *conf = getConfig();
	SeqSTM *p1 = head_->transform(env);
	SeqSTM *p2 = tail_->transform(env);
	p1->append(p2);
	return p1;
}

/* fuse optimization (program, initialization, jumping, termination) */
static SeqSTM *fuse(SeqSTM *p, Superstep *I, Superstep *J, ExprIR *tc) {
	if (p->isReading() && getConfig()->fuse) {
		SeqSTM *ret = new SeqSTM(false, p->dupStep(), nullptr);
		SeqSTM *app = new SeqSTM(false, p->dupStep(), nullptr);
		CondSTM *c = new CondSTM(tc, (SeqSTM*) p->getNext(),
				new SeqSTM(false, new Superstep(), nullptr));
		SeqSTM *j = new SeqSTM(false, J, nullptr);
		j->setJump(c);
		app->append(j);
		p->getNext()->append(app);
		SeqSTM *i = new SeqSTM(false, I, nullptr);
		ret->append(i);
		ret->add(c);
		return ret;
	} else {
		CondSTM *c = new CondSTM(tc, p, new SeqSTM(false, new Superstep(), nullptr));
		SeqSTM *j = new SeqSTM(false, J, nullptr);
		j->setJump(c);
		p->append(j);
		return new SeqSTM(false, I, c);
	}
}

SeqSTM *RepeatVC::transform(IREnv *env) {
	env->incIterCount();
	SeqSTM *p = prog_->transform(env);
	Superstep *I = new Superstep();
	Superstep *J = new Superstep();
	FieldIR *cnt = new FieldIR(getIntType(), new FieldName(Pos(),
			env->genCountField().c_str()));
	env->useField(cnt);
	I->add(new AssignIR(cnt, getOper(tok_mass), expr_->tr_loc(env)));
	ExprIR *tc = new BinaryIR(getBoolType(), getOper(tok_gt), cnt, new IntIR(0, 0));
	J->add(new AssignIR(cnt, getOper(tok_msub), new IntIR(1, 0)));
	env->decIterCount();
	return fuse(p, I, J, tc);
}

SeqSTM *IterRangeVC::transform(IREnv *env) {
	env->turnToField(name_->getName());
	SeqSTM *p = prog_->transform(env);
	Superstep *I = new Superstep();
	Superstep *J = new Superstep();
	FieldIR *cnt = new FieldIR(getIntType(), new FieldName(*name_,
			name_->getName().c_str()));
	env->useField(cnt);
	I->add(new AssignIR(cnt, getOper(tok_mass), lo_->tr_loc(env)));
	ExprIR *tc = new BinaryIR(getBoolType(), getOper(tok_lt), cnt, hi_->tr_loc(env));
	J->add(new AssignIR(cnt, getOper(tok_madd), new IntIR(1, 0)));
	env->clear();
	return fuse(p, I, J, tc);
}

SeqSTM *FixPointVC::transform(IREnv *env) {
	for (int i = 0; i < list_->size(); i++) {
		FieldName *fn = list_->get(i);
		env->pushIterField(fn);
	}
	SeqSTM *p = prog_->transform(env);
	Superstep *I = new Superstep();
	Superstep *J = new Superstep();
	FieldIR *m = env->getAggFieldIR();
	ExprIR *cond = nullptr;
	for (int i = 0; i < list_->size(); i++) {
		FieldName *fn = list_->get(i);
		Type *ty = getTypeEnv()->getType(fn);
		FieldIR *curr = new FieldIR(ty, fn);
		FieldIR *orig = new FieldIR(ty, new FieldName(Pos(),
				env->genIterField(fn).c_str()));
		env->useField(curr);
		env->useField(orig);
		I->add(new AssignIR(orig, getOper(tok_mass), curr));
		ExprIR *eq = new BinaryIR(getBoolType(),
				getOper(tok_eq), orig, curr);
		cond = (cond == nullptr ? eq : new BinaryIR(getBoolType(),
				getOper(tok_or), cond, eq));
	}
	I->add(new AssignIR(m, getOper(tok_mass), new BoolIR(true)));
	string name = env->getAM()->getDefault();
	ExprIR *tc = new AggIR(getBoolType(), name);
	J->add(new AssignIR(m, getOper(tok_mass), 
			new NotIR(getBoolType(), cond)));
	for (int i = 0; i < list_->size(); i++) {
		FieldName *fn = list_->get(i);
		Type *ty = getTypeEnv()->getType(fn);
		FieldIR *curr = new FieldIR(ty, fn);
		FieldIR *orig = new FieldIR(ty, new FieldName(Pos(),
				env->genIterField(fn).c_str()));
		J->add(new AssignIR(orig, getOper(tok_mass), curr));
		env->popIterField(fn);
	}
	return fuse(p, I, J, tc);
}

SeqSTM *ExistsVC::transform(IREnv *env) {
	SeqSTM *p = prog_->transform(env);
	Superstep *I = new Superstep();
	Superstep *J = new Superstep();
	FieldIR *m = env->getAggFieldIR();
	I->add(new AssignIR(m, getOper(tok_mass), new BoolIR(false)));
	string name = env->getAM()->getDefault();
	ExprIR *tc = new NotIR(getBoolType(),
			new AggIR(getBoolType(), name));
	return fuse(p, I, J, tc);
}

SeqSTM *ForallVC::transform(IREnv *env) {
	SeqSTM *p = prog_->transform(env);
	Superstep *I = new Superstep();
	Superstep *J = new Superstep();
	FieldIR *m = env->getAggFieldIR();
	I->add(new AssignIR(m, getOper(tok_mass), new BoolIR(true)));
	string name = env->getAM()->getDefault();
	ExprIR *tc = new AggIR(getBoolType(), name);
	return fuse(p, I, J, tc);
}

/* other functions */
void debug(const AST *ast, FILE *f) {
	ast->print(f);
	fprintf(f, "\n");
	fflush(f);
}

Operator *getOper(int tok) {
	return new Operator(Pos(), tok);
}
