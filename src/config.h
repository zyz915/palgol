#ifndef CONFIG_H
#define CONFIG_H

struct Config {
	/* generate incoming edges from outgoing edge (or vise versa) */
	int genSymEdge = 0;
	/* check uninitialized field */
	int noInitCheck = 0;
	/* number of spaces for indent (0 -> tab) */
	int spaces = 4;
	/* name of external file */
	const char *external = "external.hpp";
	/* target language*/
	const char *language = "c++";
	/* target system */
	const char *target = "pregel+";
	/* mode (basic, reqresp) */
	const char *mode = "basic";
	/* suggest type sbyignature for external functions */
	int suggest = 0;
	/* whether to use fuse optimization */
	int fuse = 1;

	Config() {}

	bool hasReqResp() const;
};

Config* getConfig();

#endif
