#ifndef INTERMEDIATE_H
#define INTERMEDIATE_H

#include <map>
#include <vector>

#include "tokenizer.h"
#include "types.h"

// from ast.h
struct Operator;
struct Aggregator;
struct FieldName;
class ExprIR;
class IfStmtIR;
class FieldExpr;
class FieldList;
class LetStmt;
// from codegen.h
class CodeGen;
class AggregatorManager;
class MessageManager;
class VertexManager;
class Text;
class Formatter;
// from this file
class IREnv;

class IR {
protected:
	int depth_;
public:
	IR(int depth=0) : depth_(depth) {}
	virtual ~IR() {}

	virtual void print(FILE *f) const = 0;
	virtual void calcDepth(IREnv *env) {}
	virtual int depth() { return depth_; }
};

enum class IRType {
	ir_int, ir_float, ir_bool, ir_pair,
	ir_remote, ir_field, ir_var, ir_vid,
	ir_ifexp, ir_select, ir_binop,
	ir_not, ir_neg, ir_size, ir_agg,
	ir_toi, ir_tof, ir_elist, ir_gsize,
	ir_snbr, ir_msg, ir_exst,
	ir_func, ir_aggf, ir_reqr
};

class ExprIR : public IR {
protected:
	Type *type_;
	IRType itype_;
public:
	ExprIR(Type *type, IRType itype, int depth=0) :
			IR(depth), type_(type), itype_(itype) {}
	virtual ~ExprIR() {}

	virtual bool equals(const ExprIR* other) const;
	virtual Type *type() const final { return type_; }
	virtual IRType itype() const final { return itype_; }
	virtual ExprIR *genSteps(IREnv *env, int step) { return this; }
	virtual ExprIR *tr_readmsg() { return this; }
	virtual void pregelPlus(Text *text, int op) const = 0;
};

class ConstIR : public ExprIR {
public:
	ConstIR(Type *type, IRType itype) : ExprIR(type, itype) {}
};

class IntIR : public ConstIR {
protected:
	int val_;
	int spl_; // -1 = -INF, 0 = val_, 1 = INF
public:
	IntIR(int val, int spl=0) : ConstIR(getIntType(),
			IRType::ir_int), val_(val), spl_(spl) {}

	void print(FILE *f) const;
	bool equals(const ExprIR* other) const;
	void pregelPlus(Text *text, int op) const;
};

class FloatIR : public ConstIR {
protected:
	double val_;
	int spl_; // -1 = -INF, 0 = val_, 1 = INF
public:
	FloatIR(double val, int spl=0) : ConstIR(getFloatType(), 
			IRType::ir_float), val_(val), spl_(spl) {}

	void print(FILE *f) const;
	bool equals(const ExprIR* other) const;
	void pregelPlus(Text *text, int op) const;
};

class BoolIR : public ConstIR {
protected:
	bool val_;
public:
	BoolIR(bool val) : ConstIR(getBoolType(),
			IRType::ir_bool), val_(val) {}

	void print(FILE *f) const;
	bool equals(const ExprIR* other) const;
	void pregelPlus(Text *text, int op) const;
};

class EmptyListIR : public ConstIR {
public:
	EmptyListIR(Type *type) : ConstIR(type, IRType::ir_elist) {}

	void print(FILE *f) const;
	void pregelPlus(Text *text, int op) const;
};

class GraphSizeIR : public ConstIR {
public:
	GraphSizeIR() : ConstIR(getIntType(), IRType::ir_gsize) {}

	void print(FILE *f) const;
	void pregelPlus(Text *text, int op) const;
};

class PairIR : public ExprIR {
protected:
	ExprIR *fst_, *snd_;
public:
	PairIR(Type *type, ExprIR *fst, ExprIR *snd):
			ExprIR(type, IRType::ir_pair), fst_(fst), snd_(snd) {}

	ExprIR *getFirst()  { return fst_; }
	ExprIR *getSecond() { return snd_; }

	void print(FILE *f) const;
	bool equals(const ExprIR* other) const;
	void calcDepth(IREnv *env);
	ExprIR *genSteps(IREnv *env, int step);
	void pregelPlus(Text *text, int op) const;
};

class IfExprIR : public ExprIR {
protected:
	ExprIR *cond_, *e1_, *e2_;
public:
	IfExprIR(Type *type, ExprIR *cond, ExprIR *e1, ExprIR *e2) :
			ExprIR(type, IRType::ir_ifexp), cond_(cond), e1_(e1), e2_(e2) {}

	void print(FILE *f) const;
	bool equals(const ExprIR* other) const;
	void calcDepth(IREnv *env);
	ExprIR *genSteps(IREnv *env, int step);
	void pregelPlus(Text *text, int op) const;
};

class IDIR : public ConstIR {
public:
	IDIR() : ConstIR(getVidType(), IRType::ir_vid) {}

	void print(FILE *f) const;
	void pregelPlus(Text *text, int op) const;
};

class LValue : public ExprIR {
public:
	LValue(Type *type, IRType itype, int depth=0) :
			ExprIR(type, itype, depth) {}
};

class FieldIR : public LValue {
protected:
	FieldName *field_;
public:
	FieldIR(Type *type, FieldName *field) :
			LValue(type, IRType::ir_field), field_(field) {}

	std::string getName() const;
	void print(FILE *f) const;
	bool equals(const ExprIR* other) const;
	void pregelPlus(Text *text, int op) const;
};

// field expression inside an aggregator
// generated conde is different from FieldIR
class AggFieldIR : public ExprIR{
protected:
	FieldName *field_;
public:
	AggFieldIR(Type *type, FieldName *field) :
			ExprIR(type, IRType::ir_aggf), field_(field) {}

	std::string getName() const;
	void print(FILE *f) const;
	bool equals(const ExprIR* other) const;
	void pregelPlus(Text *text, int op) const;
};

class VarIR : public LValue {
protected:
	std::string name_;
public:
	VarIR(Type *type, std::string name, int depth=-1) :
			LValue(type, IRType::ir_var, depth), name_(name) {}

	std::string getName() const { return name_; }
	void print(FILE *f) const;
	bool equals(const ExprIR* other) const;
	void calcDepth(IREnv *env);
	ExprIR *genSteps(IREnv *env, int step);
	void pregelPlus(Text *text, int op) const;
};

class BinaryIR : public ExprIR {
protected:
	Operator *op_;
	ExprIR *e1_, *e2_;
public:
	BinaryIR(Type *type, Operator *op, ExprIR *e1, ExprIR *e2) :
			ExprIR(type, IRType::ir_binop), op_(op), e1_(e1), e2_(e2) {}

	void print(FILE *f) const;
	bool equals(const ExprIR* other) const;
	void calcDepth(IREnv *env);
	ExprIR *genSteps(IREnv *env, int step);
	void pregelPlus(Text *text, int op) const;
};

class FunctionIR : public ExprIR {
protected:
	std::string name_;
	std::vector<ExprIR*> args_;
public:
	FunctionIR(Type *type, std::string name) :
			ExprIR(type, IRType::ir_func), name_(name) {}

	void add(ExprIR *expr) { args_.push_back(expr); }
	int size() const { return args_.size(); }

	void print(FILE *f) const;
	void pregelPlus(Text *text, int op) const;
};

class UnaryIR : public ExprIR {
protected:
	ExprIR *expr_;
public:
	UnaryIR(Type *type, ExprIR *expr, IRType itype) :
			ExprIR(type, itype), expr_(expr) {}

	bool equals(const ExprIR* other) const;
	void calcDepth(IREnv *env);
	ExprIR *genSteps(IREnv *env, int step);
};

class NotIR : public UnaryIR {
public:
	NotIR(Type *type, ExprIR *expr) :
			UnaryIR(type, expr, IRType::ir_not) {}

	void print(FILE *f) const;
	void pregelPlus(Text *text, int op) const;
};

class NegativeIR : public UnaryIR {
public:
	NegativeIR(Type *type, ExprIR *expr) :
			UnaryIR(type, expr, IRType::ir_neg) {}

	void print(FILE *f) const;
	void pregelPlus(Text *text, int op) const;
};

class SelectIR : public UnaryIR {
protected:
	Operator *op_; // either FST or SND
public:
	SelectIR(Type *type, Operator *op, ExprIR *expr) :
			UnaryIR(type, expr, IRType::ir_select), op_(op) {}

	void print(FILE *f) const;
	ExprIR *tr_readmsg();
	void pregelPlus(Text *text, int op) const;
};

class SizeIR : public UnaryIR {
public:
	SizeIR(Type *type, ExprIR *expr) :
			UnaryIR(type, expr, IRType::ir_size) {}

	void print(FILE *f) const;
	void pregelPlus(Text *text, int op) const;
};

class ToiIR : public UnaryIR {
public:
	ToiIR(Type *type, ExprIR *expr) :
			UnaryIR(type, expr, IRType::ir_toi) {}

	void print(FILE *f) const;
	void pregelPlus(Text *text, int op) const;
};

class TofIR : public UnaryIR {
public:
	TofIR(Type *type, ExprIR *expr) :
			UnaryIR(type, expr, IRType::ir_tof) {}

	void print(FILE *f) const;
	void pregelPlus(Text *text, int op) const;
};

class ReadMsgIR : public ExprIR {
protected:
	int ch_, index_;
	std::string name_;
public:
	ReadMsgIR(Type *type, std::string name, int ch, int index) :
			ExprIR(type, IRType::ir_msg), ch_(ch),
			index_(index), name_(name) {}

	void print(FILE *f) const;
	ExprIR *tr_readmsg();
	void pregelPlus(Text *text, int op) const;
};

class AggIR : public ExprIR {
protected:
	std::string name_;
public:
	AggIR(Type *type, std::string name) :
			ExprIR(type, IRType::ir_agg), name_(name) {}

	bool equals(const ExprIR *other) const;
	void print(FILE *f) const;
	void pregelPlus(Text *text, int op) const;
};

class ReqResp : public ExprIR {
protected:
	FieldName *val_, *dst_;
public:
	ReqResp(Type *type, FieldName *val, FieldName *dst) :
			ExprIR(type, IRType::ir_reqr) {}

	bool equals(const ExprIR* other) const override;
	void print(FILE *f) const override;
	ExprIR *genSteps(IREnv *env, int step) const
		{ return nullptr; /* TODO */ };
	void pregelPlus(Text *text, int op) const override
		{ /* TODO */ }
	int depth() override { return 2; }
};

class StmtIR : public IR {
public:
	virtual ~StmtIR() {}

	virtual void print(FILE *f) const = 0;
	virtual bool single() const { return true; }
	virtual void calcDepth(IREnv *env) {}
	virtual void genSteps(IREnv *env, int step);
	virtual void pregelPlus(CodeGen *cg) const = 0;
};

class StmtListIR : public StmtIR {
protected:
	std::vector<StmtIR*> list_;
public:
	StmtListIR() {}
	StmtListIR(StmtIR *stmt) { list_.push_back(stmt); }

	void add(StmtIR *stmt);
	StmtIR *get(int index);
	int size() const;

	void print(FILE *f) const override;
	bool single() const override { return size() == 1; }
	void calcDepth(IREnv *env) override;
	void genSteps(IREnv *env, int step) override;
	void pregelPlus(CodeGen *cg) const override;
};

class ExprStmtIR : public ExprIR {
protected:
	ExprIR *expr_;
	StmtListIR *stmts_;
public:
	ExprStmtIR(ExprIR *expr, StmtListIR *stmts) :
			ExprIR(expr->type(), IRType::ir_exst), expr_(expr), stmts_(stmts) {}

	StmtListIR *getStmts() { return stmts_; }
	void print(FILE *f) const;
	void calcDepth(IREnv *env);
	ExprIR *genSteps(IREnv *env, int step);
	void pregelPlus(Text *text, int op) const;
};

class AssignIR : public StmtIR {
protected:
	LValue *lval_;
	Operator *op_;
	ExprIR *expr_;
public:
	AssignIR(LValue *lval, Operator *op, ExprIR *expr) :
			lval_(lval), op_(op), expr_(expr) {}

	void print(FILE *f) const;
	void calcDepth(IREnv *env);
	void genSteps(IREnv *env, int step);
	void pregelPlus(CodeGen *cg) const;
};

class RemoteIR : public StmtIR {
protected:
	FieldName *fn_;
	ExprIR *dest_;
	Operator *op_;
	ExprIR *expr_;
public:
	RemoteIR(FieldName *fn, ExprIR *dest, Operator *op, ExprIR *expr) :
			fn_(fn), dest_(dest), op_(op), expr_(expr) {}

	void print(FILE *f) const;
	void calcDepth(IREnv *env);
	void genSteps(IREnv *env, int step);
	void pregelPlus(CodeGen *cg) const;
};

class ForStmtIR : public StmtIR {
protected:
	std::string name_;
	ExprIR *expr_;
	StmtIR *stmt_;	
public:
	ForStmtIR(std::string name, ExprIR *expr, StmtIR *stmt) :
			name_(name), expr_(expr), stmt_(stmt) {}

	void print(FILE *f) const;
	void calcDepth(IREnv *env);
	void genSteps(IREnv *env, int step);
	void pregelPlus(CodeGen *cg) const;
};

class ProcedureIR : public StmtIR {
protected:
	FunctionIR *func_;
public:
	ProcedureIR(FunctionIR *func) : func_(func) {}

	void print(FILE *f) const;
	void pregelPlus(CodeGen *cg) const;
};

enum class CombinerT {
	c_null, c_min, c_max, c_sum,
	c_and,  c_or,  c_xor, c_rand,
	c_arbi
};

class SendMsgIR : public StmtIR {
protected:
	int ch_; // channel number
	std::vector<ExprIR*> list_;
	ExprIR *dest_;
	/* special tag indicating how the message is consumed
	 *  1 -> minimum,  2 -> maximum,  3 -> sum,
	 *  4 -> random,   5 -> and,      6 -> or
	 *  0 -> others
	 */
	CombinerT comb_;

protected:
	/* this will be used in generating message data structure
	 * this array stores the information of how data are encoded
	 */
	std::vector<Type*> msgTypes_;

public:
	SendMsgIR(int ch, std::vector<ExprIR*> list, ExprIR *dest,
			CombinerT comb = CombinerT::c_null) :
			ch_(ch), list_(list), dest_(dest), comb_(comb) {}

	void dumpTypes(FILE *f = stderr) const;
	std::vector<BasicType*> flatten();
	Type *getType(int index) const { return msgTypes_[index]; }
	int getChannel() const { return ch_; }
	CombinerT getCombiner() const { return comb_; }
	ExprIR *get(int index) const { return list_[index]; }
	int size() const { return list_.size(); }

	void print(FILE *f) const;
	void calcDepth(IREnv *env);
	void genSteps(IREnv *env, int step);
	void pregelPlus(CodeGen *cg) const;
};

class GetMsgIR : public StmtIR {
protected:
	int ch_;
	VarIR *var_;
public:
	GetMsgIR(int ch, VarIR *var) : ch_(ch), var_(var) {}

	void print(FILE *f) const;
	void calcDepth(IREnv *env);
	void pregelPlus(CodeGen *cg) const;
};

class ScanStmtIR : public StmtIR {
protected:
	int ch_;
	std::string name_; // loop variable
	StmtIR *sender_; // optional
	StmtIR *stmt_;
public:
	ScanStmtIR(int ch, std::string name, StmtIR *sender, StmtIR *stmt) :
			ch_(ch), name_(name), sender_(sender), stmt_(stmt) {}

	void print(FILE *f) const;
	void calcDepth(IREnv *env);
	void genSteps(IREnv *env, int step);
	void pregelPlus(CodeGen *cg) const;
};

class IfStmtIR : public StmtIR {
protected:
	ExprIR *cond_;
	StmtIR *s1_, *s2_;
public:
	IfStmtIR(ExprIR *cond, StmtIR *s1, StmtIR *s2) :
			cond_(cond), s1_(s1), s2_(s2) {}

	void print(FILE *f) const;
	void calcDepth(IREnv *env);
	void genSteps(IREnv *env, int step);
	void pregelPlus(CodeGen *cg) const;
};

class VoteToHalt : public StmtIR {
public:
	VoteToHalt() {}

	void print(FILE *f) const;
	void pregelPlus(CodeGen *cg) const;
};

class ReturnIR : public StmtIR {
public:
	ReturnIR() {}
	
	void print(FILE *f) const;
	void pregelPlus(CodeGen *cg) const;
};

class Conjunction {
protected:
	std::vector<ExprIR*> preds_;
public:
	Conjunction() {}

	void add(ExprIR *expr);
	StmtIR *produce(StmtIR *stmt);
};

class ExprSum {
private:
	std::vector<ExprIR*> list_;
public:
	ExprSum() {}

	void add(ExprIR *expr);
	ExprIR *produce();
};

class SendPatt : public IR {
protected:
	ExprIR *cont_;
	ExprIR *dest_;
public:
	SendPatt(ExprIR *cont, ExprIR *dest) :
			cont_(cont), dest_(dest) {}

	ExprIR *cont() { return cont_; }
	ExprIR *dest() { return dest_; }
	Type *type() { return cont_->type(); }

	void print(FILE *f) const override;
	int depth() override;
};

class RemoteData {
protected:
	Identifier *cur_;
	std::string name_; // u's loop variable (renamed)
	EdgeType *et_;
	std::string lvar_; // remote vertex's loop variable
	std::vector<ExprIR*> list_;  // list of msgs to send
	int ch_;

public:
	RemoteData(int ch, Identifier *cur, std::string lvar);
	RemoteData() = delete;

	virtual int channel() const { return ch_; }
	virtual int size() const { return list_.size(); }
	virtual Identifier *getCurLoopVar() { return cur_; }
	virtual std::string getName() const { return name_; }
	virtual ReadMsgIR *add(ExprIR *expr);
	virtual ExprIR *getFst() = 0;
	virtual ExprIR *getSnd() = 0;
	virtual StmtIR *produce(IREnv *env) = 0;
};

class NbrGetData : public RemoteData {
protected:
	FieldName *gen_;
	Conjunction in_, out_;
public:
	NbrGetData(int ch, Identifier *cur, FieldName *gen);
	NbrGetData() = delete;

	void addPredicate(ExprIR *expr, bool rmt_lp = 1);
	ExprIR *getFst() override;
	ExprIR *getSnd() override;
	StmtIR *produce(IREnv *env) override;
};

class ReqResData : public RemoteData {
protected:
	FieldName *fn_;
	int ew_, req_ch_;
	Conjunction req_, res_in_, res_out_;
public:
	ReqResData(int ch, Identifier *cur, FieldName *fn);
	ReqResData() = delete;

	void addReqPredicate(ExprIR *expr);
	void addResPredicate(ExprIR *expr, bool rmt_lp = 1);
	ExprIR *getFst() override;
	ExprIR *getSnd() override;
	StmtIR *produce(IREnv *env) override;
};

class Superstep {
private:
	std::vector<int> channels_;
	std::vector<StmtListIR*> stack_;
public:
	Superstep() {
		stack_.push_back(new StmtListIR());
	}

	void dump(MessageManager *mm, FILE *f = stderr);
	void add(StmtIR *stmt);
	void push();
	StmtListIR *get() const;
	StmtListIR *pop();

	void append(Superstep *s);
	void addChannel(int channel);
	std::vector<int> getChannels() { return channels_; }
};

class SeqSTM;
class STM {
protected:
	int step_;
public:
	STM() : step_(0) {}

	virtual bool isReading() = 0;
	virtual void dump(MessageManager *mm, FILE *f = stderr) = 0;

	virtual void append(SeqSTM *stm) = 0;
	virtual void add(STM *stm) = 0;
	virtual void setStep(CodeGen *cg, int step) = 0;
	virtual void pregelPlus(CodeGen *cg) const = 0;
};

class SeqSTM : public STM {
protected:
	bool reading_;
	Superstep *head_;
	STM *next_, *jump_;

public:
	SeqSTM(bool reading, Superstep *head, STM *next) :
			reading_(reading), head_(head), next_(next), jump_(nullptr) {}

	Superstep *getStep() { return head_; }
	Superstep *dupStep();
	STM *getNext() { return next_; }
	STM *getJump() { return jump_; }
	void setNext(STM *stm) { next_ = stm; }
	void setJump(STM *jmp) { jump_ = jmp; }
	STM *jump() const { return jump_ == nullptr ? next_ : jump_; }

	bool isReading() { return reading_; }
	void dump(MessageManager *mm, FILE *f = stderr);

	void append(SeqSTM *stm);
	void add(STM *stm);
	void setStep(CodeGen *cg, int step);
	void pregelPlus(CodeGen *cg) const;
};

class CondSTM : public STM {
protected:
	ExprIR *cond_;
	SeqSTM *loop_;
	SeqSTM *exit_;

public:
	CondSTM(ExprIR *cond, SeqSTM *loop, SeqSTM *exit) :
			cond_(cond), loop_(loop), exit_(exit) {}

	bool isReading() { return false; }
	void dump(MessageManager *mm, FILE *f = stderr);

	void append(SeqSTM *stm);
	void add(STM *stm);
	void setStep(CodeGen *cg, int step);
	void pregelPlus(CodeGen *cg) const;
};

class ExprSet {
protected:
	std::vector<ExprAST*> exprs_;
public:
	ExprSet() {}

	void add(ExprAST* expr);
	ExprAST *get(int index);
	int size();
};

struct RUStruct {
	int ch, op;
	FieldName *fn;
	Type *type;

	RUStruct(int ch, int op, FieldName *fn, Type *type) :
			ch(ch), op(op), fn(fn), type(type) {}
};

class IREnv {
private:
	Identifier *curVertex_;
	std::vector<std::map<std::string, ExprAST*> > stack_;
	std::vector<StmtListIR*> stmts_;
	int varid_, chid_, looplv_, pid_, repeat_;
	bool hasRU_; // has remote update phase
	CombinerT comb_; // for combiner
	Operator *op_;
	LValue *lval_;
	Identifier *curAgg_;

	AggregatorManager *am_;
	MessageManager *mm_;
	VertexManager *vm_;

	// for FieldExpr
	std::vector<std::pair<FieldExpr*, std::pair<VarIR*, SendPatt*> > > binds_;
	std::vector<std::pair<ExprAST*, std::pair<VarIR*, SendPatt*> > > rbinds_;
	std::map<std::string, ExprIR*> vbinds_;
	std::vector<Superstep*> steps_;
	std::set<std::string> used_; // used VarIR in bindings
	std::set<std::string> isf_;
	std::map<std::string, int> calc_; // channel number for pattern two
	std::map<std::string, RUStruct*> rus_; // remote udpates
	std::map<std::string, Type*> ftypes_;
	std::map<std::string, std::pair<Type*, 
			std::pair<int, int> > > varStep_;
	std::map<std::string, int> itcnt_;
public:
	IREnv() : curVertex_(nullptr), varid_(0), chid_(0), looplv_(0), pid_(0), steps_(0),
			am_(nullptr), mm_(nullptr), vm_(nullptr), lval_(nullptr), op_(nullptr),
			repeat_(0), curAgg_(nullptr), comb_(CombinerT::c_null) {}

	void dump(FILE *f = stderr);
	void setCurVertex(Identifier *name);
	Identifier *getCurVertex();
	void incPID() { pid_ += 1; }
	int  getPID() { return pid_; }
	// for variable binding
	void pushScope() { stack_.push_back(std::map<std::string, ExprAST*>()); }
	void popScope() { stack_.pop_back(); }
	void addBinding(Identifier *name, ExprAST *expr);
	ExprAST *getExpr(Identifier *name);
	// for translating loops
	void incLoopLv() { looplv_++; }
	void decLoopLv() { looplv_--; }
	int  getLoopLv() { return looplv_; }
	// return 0=no, 1=In, 2=Nbr, 3=Out
	int  isEdgeList(ExprAST *expr);
	// tentative interface
	void getSubExprs(FieldExpr *fe, ExprSet *subs);
	bool isReqResp(FieldExpr *fe);
	ExprIR *transformRemote(ExprAST *vertex);
	// cache for field expressions (FieldExpr -> (VarIR -> exprIR))
	VarIR *bind(FieldExpr *fe, SendPatt *expr);
	VarIR *find(FieldExpr *fe);
	// cache for [u]_vertex (ExprAST -> (VarIR -> ExprIR))
	VarIR *rbind(ExprAST *vertex, SendPatt *expr);
	VarIR *rfind(ExprAST *vertex);
	// cache for normal variable definitions
	void vbind(VarIR *vertex, ExprIR *expr);
	// some interfaces used in generating code
	SendPatt *getSendPatt(VarIR *var);
	void producePattOne(VarIR *var);
	int producePattTwo(VarIR *var);
	void produceVariable(VarIR *var);
	// create variable, channel
	std::string createVar();
	int newChannel() { return chid_++; }
	// translation commands
	void pushStmts();
	StmtListIR *popStmts();
	void emit(StmtIR *stmt);
	// for translating list comprehension
	void setCombiner(CombinerT c) { comb_ = c; }
	CombinerT getCombiner() { return comb_; }
	void setLValue(LValue *lval) { lval_ = lval; }
	LValue *getLValue(Type *type);
	void setOperator(Operator *op) { op_ = op; }
	Operator *getOperator();
	// for generating supersteps and STM
	void useVarIR(VarIR *var); // to remove redundant let
	void initSteps(int steps);
	void addToStep(int step, StmtIR *stmt);
	void addChannel(int step, int ch);
	void pushStmts(int step) { steps_[step]->push(); }
	StmtListIR *popStmts(int step) { return steps_[step]->pop(); }
	int remoteUpdate(FieldName *fn, Type *type, Operator *op);
	SeqSTM *produceSteps(bool stop = false);
	// lifetime of variable
	void useVarInStep(const VarIR *var, int step);
	bool isField(const VarIR *var);
	void turnToField(const std::string &name);
	void checkVarsSpans();
	void useField(FieldIR *field);
	// generate field in iteration
	void pushIterField(FieldName *fn);
	void popIterField(FieldName *fn);
	std::string genIterField(FieldName *fn);
	void incIterCount() { ++repeat_; }
	void decIterCount() { --repeat_; }
	std::string genCountField();
	// aggregators
	ExprIR *registerAgg(Type *type, Operator *op,
			Conjunction *conj, ExprIR *expr);
	FieldIR *getAggFieldIR();
	void setAggCurVertex(Identifier *name) { curAgg_ = name; }
	Identifier *getAggCurVertex() { return curAgg_; }
	void exitAggMode() { curAgg_ = nullptr; }
	bool isAggMode() { return curAgg_ != nullptr; }
	// clear bindings
	void clear();
	// aggregator & message & vertex manager
	AggregatorManager *getAM();
	MessageManager *getMM();
	VertexManager *getVM();
};

SeqSTM *transform(VCProg *prog, bool reqresp, bool dump);

CombinerT op2combiner(int op);
IREnv *getIREnv();
void debug(const IR *ir, FILE *f = stderr);

#endif
