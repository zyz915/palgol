#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>

#include "tokenizer.h"

using namespace std;

enum ast_tok { t_lpa, t_rpa, t_str, t_eof };

struct Tree {
	Token cons;
	vector<Tree*> list;

	Tree(Token tok) : cons(tok) {}
};

static Token createToken(int type, char *val = nullptr) {
	return Token(0, 0, type, val);
}

static char buff[280];
static int ch;
static Token gettok() {
	int bptr = 0;
	while (ch != EOF && isspace(ch))
		ch = getchar();
	if (ch == EOF)
		return createToken(t_eof);
	if (ch == '(') {
		ch = getchar();
		return createToken(t_lpa);
	}
	if (ch == ')') {
		ch = getchar();
		return createToken(t_rpa);
	}
	if (ch == '"') {
		do {
			buff[bptr++] = ch;
			ch = getchar();
		} while (ch != '"' && ch != EOF);
		if (ch == EOF)
			perror("parse error");
		buff[bptr++] = ch;
		buff[bptr] = 0;
		ch = getchar();
		return createToken(t_str, strdup(buff));
	}
	if (isalnum(ch) || ch == '-') {
		do {
			buff[bptr++] = ch;
			ch = getchar();
		} while (isalnum(ch) || ch == '_' || ch == '.');
		buff[bptr] = 0;
		return createToken(t_str, strdup(buff));
	}
	return createToken(t_eof);
}

static void pretty(Tree *t, int indent, FILE *f);
static Token tok;
static Tree *parseTree() {
	vector<Tree*> st;
	while (tok.type != t_eof) {
		if (tok.type != t_rpa) {
			st.push_back(new Tree(tok));
			tok = gettok();
		} else {
			int pr = st.size() - 1;
			while (st[pr]->cons.type != t_lpa) pr--;
			Tree *cur = new Tree(st[pr + 1]->cons);
			for (int i = pr + 2; i < st.size(); i++)
				cur->list.push_back(st[i]);
			st.resize(pr);
			st.push_back(cur);
			tok = gettok();
		}
	}
	return st[0];
}

static bool isLeaf(const Tree *t) {
	return t->list.size() == 0;
}
static bool isInline(const Tree *t) {
	for (int i = 0; i < t->list.size(); i++)
		if (!isLeaf(t->list[i]))
			return false;
	return true;
}

static char line[256];
static char *getSpace(int len) {
	line[len] = ' ';
	line[len + 2] = 0;
	return line + 2;
}

static void pretty(Tree *t, int indent = 0, FILE *f = stdout) {
	if (isLeaf(t))
		fprintf(f, "%s%s\n", getSpace(indent), t->cons.val);
	else if (isInline(t)) {
		fprintf(f, "%s(%s", getSpace(indent), t->cons.val);
		for (int i = 0; i < t->list.size(); i++)
			fprintf(f, " %s", t->list[i]->cons.val);
		fprintf(f, ")\n");
	} else {
		fprintf(f, "%s(%s\n", getSpace(indent), t->cons.val);
		for (int i = 0; i < t->list.size(); i++)
			pretty(t->list[i], indent + 2, f);
		fprintf(f, "%s)\n", getSpace(indent));
	}
}

int main() {
	ch = getchar();
	tok = gettok();
	Tree *tr = parseTree();
	memset(line, ' ', 256);
	pretty(tr);
	return 0;
}
