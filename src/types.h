#ifndef TYPES_H
#define TYPES_H

#include <map>
#include <set>
#include <vector>

class TypeVar;
class TypeEnv;
class ExprIR;
class ExprSum;
class StmtListIR;
class Text;
struct MsgEnc;

enum class PalgolTypes {
	t_vid,  t_int,  t_float, t_bool,
	t_pair, t_edge, t_list,  t_set,
	t_var,  t_unit
};

class BasicType;

class Type {
protected:
	PalgolTypes type_;
public:
	Type(PalgolTypes type) : type_(type) {}
	virtual ~Type() {}
	virtual PalgolTypes type() const final { return type_; }
	virtual Type *subst(TypeEnv *env) { return this; }
	virtual bool equals(Type *t) const { return type_ == t->type_; }
	virtual bool contains(TypeVar *tv) const { return false; }
	virtual bool contains(PalgolTypes type) const { return type_ == type; }
	virtual bool comparable() const { return true; }
	virtual bool numeric() const { return false; }
	virtual bool basic() const { return false; }
	virtual void replace(TypeVar *vt, Type *t) {}
	virtual void print(FILE *f) const = 0;
	virtual void toCPP(Text *text) const = 0;
	virtual Type *flatten(std::vector<BasicType*> &v) { return this; }
	virtual ExprIR *produceRead(const char *name, bool sp) { return nullptr; }
	virtual void produceParam(ExprSum *sums[], ExprIR *floats[], ExprIR *e) {}
};

class UnitType : public Type {
private:
	UnitType() : Type(PalgolTypes::t_unit) {}

public:
	static UnitType *utype;

	UnitType(const UnitType &) = delete;
	UnitType &operator=(const UnitType&) = delete;

	bool comparable() const override;
	void print(FILE *f) const override;
	void toCPP(Text *text) const override;
};

class TypeVar : public Type {
private:
	int id_;
public:
	TypeVar(int id) : Type(PalgolTypes::t_var), id_(id) {}

	int getId() const;
	Type *subst(TypeEnv *env) override;
	bool equals(Type *t) const override;
	bool contains(TypeVar *t) const override;
	bool comparable() const override;
	void print(FILE *f) const override;
	void toCPP(Text *text) const override;
	Type *flatten(std::vector<BasicType*> &v) override;
};

class PairType : public Type {
protected:
	Type *fst_, *snd_;
public:
	PairType(Type *fst, Type *snd, PalgolTypes type = PalgolTypes::t_pair) :
			Type(type), fst_(fst), snd_(snd) {}

	Type *subst(TypeEnv *env) override;
	bool equals(Type *t) const override;
	bool contains(TypeVar *t) const override;
	bool contains(PalgolTypes type) const override;
	bool comparable() const override;
	bool basic() const override { return snd_->equals(UnitType::utype); }
	void replace(TypeVar *vt, Type *t) override;
	void print(FILE *f) const override;
	void toCPP(Text *text) const override;
	Type *flatten(std::vector<BasicType*> &v) override;
	ExprIR *produceRead(const char *name, bool sp) override;
	void produceParam(ExprSum *sums[], ExprIR *floats[], ExprIR *e) override;

	virtual Type *getFirst();
	virtual Type *getSecond();
};

class EdgeType : public PairType {
public:
	EdgeType(Type *fst, Type *snd) :
			PairType(fst, snd, PalgolTypes::t_edge) {}

	Type *subst(TypeEnv *env) override;
	bool basic() const override { return snd_->equals(UnitType::utype); }
	void print(FILE *f) const override;
	void toCPP(Text *text) const override;
	Type *flatten(std::vector<BasicType*> &v) override;
};

class ListType : public Type {
protected:
	Type *cont_;
public:
	ListType(Type *cont) :
			Type(PalgolTypes::t_list), cont_(cont) {}

	Type *subst(TypeEnv *env) override;
	bool equals(Type *t) const override;
	bool contains(TypeVar *t) const override;
	bool contains(PalgolTypes type) const override;
	bool comparable() const override;
	void replace(TypeVar *vt, Type *t) override;
	void print(FILE *f) const override;
	void toCPP(Text *text) const override;
	Type *flatten(std::vector<BasicType*> &v) override;

	Type *getContent();
};

class SetType : public Type {
protected:
	Type *cont_;
public:
	SetType(Type *cont) :
			Type(PalgolTypes::t_set), cont_(cont) {}
	
	Type *subst(TypeEnv *env) override;
	bool equals(Type *t) const override;
	bool contains(TypeVar *t) const override;
	bool contains(PalgolTypes type) const override;
	bool comparable() const override;
	void replace(TypeVar *vt, Type *t) override;
	void print(FILE *f) const override;
	void toCPP(Text *text) const override;
	Type *flatten(std::vector<BasicType*> &v) override;

	Type *getContent();
};

class BasicType : public Type {
protected:
	MsgEnc *enc_; /* will be accessed in generating message */
public:
	BasicType(PalgolTypes type) : enc_(nullptr), Type(type) {}

	bool basic() const override { return true; }
	void setMsgEnc(MsgEnc *enc) { enc_ = enc; }
	MsgEnc *getMsgEnc() { return enc_; }
};

class VidType : public BasicType {
private:
	VidType() : BasicType(PalgolTypes::t_vid) {}

public:
	static VidType *vtype;

	VidType(const VidType &) = delete;
	VidType &operator=(const VidType &) = delete;

	bool numeric() const override;
	void print(FILE *f) const override;
	void toCPP(Text *text) const override;
	Type *flatten(std::vector<BasicType*> &v) override;
	ExprIR *produceRead(const char *name, bool sp) override;
	void produceParam(ExprSum *sums[], ExprIR *floats[], ExprIR *e) override;
};

class IntType : public BasicType {
private:
	IntType() : BasicType(PalgolTypes::t_int) {}

public:
	static IntType *itype;

	IntType(const IntType &) = delete;
	IntType &operator=(const IntType &) = delete;

	bool numeric() const override;
	void print(FILE *f) const override;
	void toCPP(Text *text) const override;
	Type *flatten(std::vector<BasicType*> &v) override;
	ExprIR *produceRead(const char *name, bool sp) override;
	void produceParam(ExprSum *sums[], ExprIR *floats[], ExprIR *e) override;
};

class FloatType : public BasicType {
private:
	FloatType() : BasicType(PalgolTypes::t_float) {}

public:
	static FloatType *ftype;

	FloatType(const FloatType &) = delete;
	FloatType &operator=(const FloatType &) = delete;

	bool numeric() const override;
	void print(FILE *f) const override;
	void toCPP(Text *text) const override;
	Type *flatten(std::vector<BasicType*> &v) override;
	ExprIR *produceRead(const char *name, bool sp) override;
	void produceParam(ExprSum *sums[], ExprIR *floats[], ExprIR *e) override;
};

class BoolType : public BasicType {
private:
	BoolType() : BasicType(PalgolTypes::t_bool) {}

public:
	static BoolType *btype;

	BoolType(PalgolTypes type) : BasicType(type) {}
	~BoolType() {}

	void print(FILE *f) const override;
	void toCPP(Text *text) const override;
	Type *flatten(std::vector<BasicType*> &v) override;
	ExprIR *produceRead(const char *name, bool sp) override;
	void produceParam(ExprSum *sums[], ExprIR *floats[], ExprIR *e) override;
};

// declaration
class ExprAST;
class Identifier;
class FieldName;
class VCProg;

class TypeEnv {
private:
	class Scope {
	protected:
		std::map<std::string, Type*> types_;
		std::set<std::string> mut_;
	public:
		bool recur_;

		Scope(bool rec) : recur_(rec) {}
		~Scope() {}

		void add(Identifier *name, Type *t, bool mut);
		Type *get(Identifier *name);
		bool exists(Identifier *name);
	};

private:
	int inc_, ver_;
	std::map<std::string, Type*> globals_, alias_;
	std::map<std::string, Pos> fpos_;
	std::vector<std::pair<Pos, Type*> > numeric_, compa_;
	std::map<int, TypeVar*> weight_;
	std::map<int, Type*> table_;
	std::vector<ExprAST*> trace_;
	std::vector<Scope*> stack_;
	std::map<std::string, std::vector<Type*> > exts_;
	TypeVar *w_;

	void extend(TypeVar *vt, Type *t, Pos pos);
public:
	TypeEnv();

	TypeVar *createTypeVar();
	Type *lookup(TypeVar *vt);
	// for external functions
	void addTypeAlias(Identifier *str, Type *t);
	Type* getTypeAlias(Identifier *str);
	void addExternal(Identifier *name, std::vector<Type*> types);
	bool existsFunc(Identifier *name);
	void suggestCPP(FILE *f);
	std::vector<Type*> getFuncType(Identifier *name);
	// add/remove bindings
	void addBinding(Identifier *name, Type *t, bool mut = false);
	void removeBinding(Identifier *name);
	void pushScope(bool r = true) { stack_.push_back(new Scope(r)); }
	void popScope() { stack_.pop_back(); }
	bool exists(Identifier *name);
	void clearLocal();
	Type *getEdgeWeight() { return lookup(w_); }
	Type *getType(Identifier *name);
	bool isMutable(Identifier *name);
	// for global fields
	Type *getType(FieldName *field);
	void setType(FieldName *field, Type *t);
	void addField(FieldName *field);
	// add constraint
	void addNumeric(Type *t, Pos pos);
	void addComparable(Type *t, Pos pos);
	void addEdgeWeight(Type *t, Pos pos);
	void addConstraint(Type *t1, Type *t2, Pos pos);
	// check the other constraints
	void trace(ExprAST *expr);
	void check();
	// debug information
	void dump(FILE *f = stderr);
};

IntType *getIntType();
VidType *getVidType();
BoolType *getBoolType();
UnitType *getUnitType();
FloatType *getFloatType();

void typeChecking(VCProg *prog, bool dump = false);
TypeEnv *getTypeEnv();
int cmp_type(const Type *t1, const Type *t2);
void debug(const Type *t, FILE *f = stderr);

#endif
